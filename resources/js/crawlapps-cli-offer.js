const host = process.env.MIX_APP_URL;
const apiEndPoint = host + '/api';

var shopifyDomain = Shopify.shop;
var frontendDomain = window.location.origin;
var shopifyTheme = convertToSlug(Shopify.theme.name);
var jqueryLoaded = 0;
if(!window.jQuery)
{

    var script = document.createElement('script');
    script.type = "text/javascript";
    script.src = "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js";
    document.getElementsByTagName('head')[0].appendChild(script);
    jqueryLoaded = 1;
}


Window.crawlapps = {
    shop_data: JSON.parse(document.getElementById('crawlapps_offer_shop_data').innerHTML),
    init:function(){
        this.initScript();
    },
    addShortcode() {

        if(allThemes())
            return true;

        let themes = ['crawlapps_body_narrative', 'crawlapps_body_boundless','crawlapps_body_prestige','crawlapps_body_ella'];

        let theme2parentlvl = ['crawlapps_body_shoptimized', 'crawlapps_body_masonry', 'crawlapps_body_district'];

        let noAddBtnName =['crawlapps_body_prestige'];
        let themeMotion = ['crawlapps_body_motion'];
        let gridAtLastAppend = ['crawlapps_body_narrative', 'crawlapps_body_boundless','crawlapps_body_prestige', 'crawlapps_body_district'];

        let addToCart = $('[name="add"]:first, #AddToCart:first, .product-form--atc-button:first, .add_to_cart_product_page:first').parent();

        if(theme2parentlvl.indexOf(addBodyClass) >= 0) {
            addToCart = $('[name="add"]:first, #AddToCart:first, .product-form--atc-button:first, .add_to_cart_product_page:first').parent().parent();
        }


        if(themeMotion.indexOf(addBodyClass) >= 0)
            addToCart = $('[name="add"]:first, #AddToCart:first, .product-form--atc-button:first, .add_to_cart_product_page:first');


        if($('.crawlapps_offers_dropdown').length <= 0 ) {

            if(themes.indexOf(noAddBtnName) >= 0) {
                addToCart = $('form[action="/cart/add"]:first .ProductForm__AddToCart:first, #AddToCart:first, .product-form--atc-button:first, .add_to_cart_product_page:first');
            }
            else if(addBodyClass == "crawlapps_body_minimart"){
                addToCart = $('form[action="/cart/add"]:first #product-variants');
                addToCart.after('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_dropdown"></div>');
            }else if(addBodyClass == "crawlapps_body_hustler"){
                addToCart = $('form[action="/cart/add"]:first .bar_adjustment');
                addToCart.before('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_dropdown"></div>');
            }
            else if(addBodyClass == "crawlapps_body_debut"){
                if($('form[action="/cart/add"]:first .single-option-selector').length > 0){
                    addToCart = $('form[action="/cart/add"]:first .selector-wrapper:last');
                    addToCart.after('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_dropdown"></div>');
                }else{
                    addToCart = $('form[action="/cart/add"]:first .product-form__controls-group--submit:first');
                    addToCart.prepend('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_dropdown"></div>');
                }
            }else if(addBodyClass == "crawlapps_body_venture"){
                addToCart = $('form[action="/cart/add"]:first .product-form__item--quantity:first');
                addToCart.before('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_dropdown"></div>');
            }else if(addBodyClass == "crawlapps_body_simple"){
                if($('form[action="/cart/add"]:first .single-option-selector').length > 1){
                    addToCart.before('<div data-code="crawlapps_offers" class="selector-wrapper crawlapps_offers crawlapps_offers_dropdown"></div>');
                }else{
                    addToCart.before('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_dropdown"></div>');
                }
            }else if(addBodyClass == "crawlapps_body_ella"){
                addToCart = $('[name="add"]:first');
                addToCart.before('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_dropdown"></div>');
            }
            else if(addBodyClass == "crawlapps_body_narrative"){
                addToCart = $('[name="add"]:first');
                addToCart.before('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_dropdown"></div>');
            }else if(addBodyClass == "crawlapps_body_boundless"){
                addToCart = $('[name="add"]:first');
                addToCart.before('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_dropdown"></div>');
            }
            else {
                addToCart.before('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_dropdown"></div>');
            }
        }


        if($('.crawlapps_offers_swatch').length <= 0 ) {

            if(themes.indexOf(addBodyClass) >= 0){

                if(themes.indexOf(noAddBtnName) >= 0 ){

                    addToCart = $('form[action="/cart/add"]:first .ProductForm__AddToCart:first, #AddToCart:first, .product-form--atc-button:first, .add_to_cart_product_page:first');
                }
                else{
                    addToCart = $('[name="add"]:first, #AddToCart:first, .product-form--atc-button:first, .add_to_cart_product_page:first');
                }
                addToCart.before('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_swatch"></div>');
            }
            else if(addBodyClass == "crawlapps_body_minimart"){
                addToCart = $('form[action="/cart/add"]:first #product-variants');
                addToCart.after('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_swatch"></div>');
            }else if(addBodyClass == "crawlapps_body_hustler"){
                addToCart = $('form[action="/cart/add"]:first .bar_adjustment');
                addToCart.before('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_swatch"></div>');
            }else if(addBodyClass == "crawlapps_body_debut"){
                addToCart = $('form[action="/cart/add"]:first .product-form__controls-group--submit');
                addToCart.prepend('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_swatch"></div>');
            }else{
                addToCart.before('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_swatch"></div>');
            }

        }


        addToCart = $('[name="add"]:first, #AddToCart:first, .product-form--atc-button:first, .add_to_cart_product_page:first').parent();
        if($('.crawlapps_offers_gridview').length <= 0 ) {
            if(gridAtLastAppend.indexOf(addBodyClass) >= 0){
                $('form[action="/cart/add"]:first, #AddToCart:first, .product-form--atc-button:first, .add_to_cart_product_page:first').append('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_gridview" ></div>');
            }else{
                addToCart.after('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_gridview" ></div>');
            }
        }

    },
    getOffers() {
        var self = Window.crawlapps;
        let aPIEndPoint = `${apiEndPoint}/${shopifyDomain}/offers?body_class=${addBodyClass}&product_id=`+Window.crawlapps.shop_data.product.id;
        $.ajax({
                method: "GET",
                url: aPIEndPoint,
                contentType: 'application/json;',
                success:function (response,success,header) {
                let type = header.getResponseHeader('x_discount_type');
                let body_class = header.getResponseHeader('body_class');
                console.log(body_class);
                    document.body.classList.add(body_class);
                if (typeof type == "string") {
                    self.addShortcode();
                }
                if(type == "swatch") {
                    $(".crawlapps_offers_swatch").html(response);
                }
                else if(type == "dropdown") {
                    $(".crawlapps_offers_dropdown").html(response);
                    var value = $("select.crawlapps_css_dropdown").children("option:selected").val();
                    $("#offerChange_dropdown").trigger('change');
                }
                else {
                    $(".crawlapps_offers_gridview").html(response);
                }
            },
        });
    },
    initScript(){

        if(Window.crawlapps.shop_data.template == 'product'){
            Window.crawlapps.getOffers();
            Window.crawlapps.addToCartButtonClick();
        }

        if( Window.crawlapps.shop_data.template == 'cart' ) {
            Window.crawlapps.cartPage();
        } else {
            if ( localStorage.getItem("crawlapps_cart_discount") != null ) {
                $("input[name='checkout'], input[value='Checkout'], button[name='checkout'], [href$='checkout'], button[value='Checkout'], input[name='goto_pp'], button[name='goto_pp'], input[name='goto_gc'], button[name='goto_gc'], .action button.btn-2").removeAttr('name').attr('name','checkout_crawlapps').addClass('checkout_crawlapps');
            }
        }
        setTimeout(function () {
            console.log("initScript");
            Window.crawlapps.checkoutButtonClick();
        },3000);

    },
    async cartPage(){
        $('body').on('click', '.crawlapps_discount_button', function(e){
            e.preventDefault();
            var code = $('.crawlapps_discount_code').val();
            if(code == ''){
                $('.crawlapps_discount_code').addClass('discount_error');
            }else{
                localStorage.setItem('discount_code', code);
                $('.crawlapps_discount_code').removeClass('discount_error');
            }
        });
        var self = this;
        let aPIEndPoint = `${frontendDomain}/cart.json`;
        $.ajax({
            method: "GET",
            url: aPIEndPoint,
            contentType: 'application/json;',
            success:function (response,success,header) {
                console.log(response);
                let formdata = {
                    discount_code : localStorage.getItem('discount_code'),
                    data: response
                };
                let url = `${apiEndPoint}/${shopifyDomain}/offers/cart`;

                $.post(url, formdata)
                    .done(function(response){
                        var data = response.data;
                        if (typeof data.discounts == "object" && typeof data.discounts.items == "object") {
                            self.crawlappsShowCartDiscounts(data.discounts);
                        }
                    })
                    .fail(function(xhr, status, error) {
                        console.log(xhr, status, error);
                    });
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                console.log(errorThrown);
            },
        });
    },
    async addToCartButtonClick(){
        let base = Window.crawlapps;
        $(document).on('click', "input[name='add'], input[value='Add To Cart'], button[name='add'], [href$='add'], button[value='Add To Cart']", function(e){
            setTimeout(function () {
                base.cartPage();
            },1000);
        });
    },
    crawlappsShowCartDiscounts(discounts) {
        var flag=0;
        $(".crawlapps_discount_hide").show();
        console.log('sssssss_2---in');
        discounts.cart.items.forEach(function(item) {
            if (item.cart_discount_away_msg_html){
                $(".crawlapps-reminder[data-key='" + item.key + "']").html(item.cart_discount_away_msg_html);
            }

            if (item.discounted_price < item.original_price) {
                $(".cart__qty-input[data-key='" + item.key + "']").val( item.__quantity );
                //$(".cart__qty-input[data-key='" + item.key + "']").attr( 'readonly', 'readonly' );
                flag=1;

                if(item.__code == "DISCOUNTSINGLE"){
                    $(".crawlapps-cart-item-price[data-key='" + item.key + "']").html("<span class='original_price'>" + item.original_price_format + "</span>");

                }else{
                    //console.log('---------------1----------');
                    $(".crawlapps-cart-item-price[data-key='" + item.key + "']").html("<span class='original_price' style='text-decoration:line-through;'>" + item.original_price_format + "</span><br/>" + "<span class='discounted_price'>" + item.discounted_price_format + "</span>");
                }
                //console.log('---------------2----------');
                $(".crawlapps-cart-item-line-price[data-key='" + item.key + "']").html("<span class='original_price' style='text-decoration:line-through;'>" + item.original_line_price_format + "</span><br/>" + "<span class='discounted_price'>" + item.discounted_line_price_format + "</span>")

            }else{
                $(".crawlapps-cart-item-price[data-key='" + item.key + "']").html("<span class='original_price'>" + item.original_price_format + "</span>");
                $(".crawlapps-cart-item-line-price[data-key='" + item.key + "']").html("<span class='original_price'>" + item.original_line_price_format + "</span>")
            }

        });
        if(flag==1){
            $(".crawlapps-cart-original-total").html(discounts.original_total_price).css("text-decoration", "line-through");
            if(discounts.final_with_discounted_price == null){
                $("<span class='crawlapps-cart-total'>" + discounts.discounted_price_total + "</span>").insertAfter('.crawlapps-cart-original-total');
            }else{
                $("<span class='crawlapps-cart-total'>" + discounts.final_with_discounted_price + "</span>").insertAfter('.crawlapps-cart-original-total');
            }
            if($(".crawlapps-discount-bar").length > 0){
                $(".crawlapps-discount-bar").html(discounts.cart_discount_msg_html)
            }else {
                $('form[action="/cart"]').prepend(discounts.cart_discount_msg_html)
            }
        }
        if (discounts.discount_code && discounts.discount_error == 1) {
            $(".crawlapps-cart-original-total").html(discounts.original_price_total);
            $(".crawlapps_discount_hide").after("<span class='crawlapps_summary'>Discount code does not match</span>");
            localStorage.removeItem('discount_code');
        } else if((discounts.discount_code && $('.discount_code_box').is(":visible"))){
            $(".crawlapps_discount_hide").after("<span class='crawlapps-summary-line-discount-code'><span class='discount-tag'>"+ discounts.discount_code +"<span class='close-tag'></span></span><span class='crawlapps_with_discount'>"+" -" + discounts.with_discount + "</span></span><span class='after_discount_price'><span class='final-total'>Total</span>"+discounts.final_with_discounted_price +"</span>");
            if(flag ==1){
                $(".crawlapps-cart-original-total").html(discounts.discounted_price_total).css("text-decoration", "line-through");
            }else{
                $(".crawlapps-cart-original-total").html(discounts.original_price_total).css("text-decoration", "line-through");
            }
            $(".crawlapps-cart-total").remove();
        }else{
            $(".crawlapps-cart-original-total").html(discounts.original_price_total);
        }
        console.log("-------end");
        Window.crawlapps.cart = discounts;
        localStorage.removeItem("crawlapps_cart_discount");
        localStorage.setItem('crawlapps_cart_discount',JSON.stringify(discounts))
    },
    checkoutButtonClick() {
        console.log("in");
        $('a.checkout_crawlapps, button[name="checkout"], button.add_to_cart').click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            checkoutIN();
        });

        $(document).on('click', ".checkout_crawlapps, input[name='checkout'], [name='checkout_crawlapps'], input[name='checkout_crawlapps'] , input[value='CHECKOUT'], input[value='Checkout'], button[name='checkout'], [href$='checkout'], [href$='/checkout'], [href='/checkout'], [href='checkout'], button[value='Checkout'], input[name='goto_pp'], button[name='goto_pp'], input[name='goto_gc'], button[name='goto_gc'], #dropdown-cart .actions .btn, .checkout-button, .ajax-cart__button.button--add-to-cart", function(e){
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            checkoutIN();

        });

        function checkoutIN(){
            console.log("checkoutButtonClick");

            let formdata = {
                discount_code : localStorage.getItem('discount_code'),
                data: JSON.parse(localStorage.getItem('crawlapps_cart_discount')),
            };
            let url = `${apiEndPoint}/${shopifyDomain}/offers/create-draft-order`;
            $.post(url, formdata)
                .done(function(response){
                    let data = response.data;
                    if ( typeof data.url == "string" ) {
                        window.open(data.url,"_self", "", true);
                    }
                    return false;
                })
                .fail(function(xhr, status, error) {
                    console.log(xhr, status, error);
                });

            return false;
        };
    },
    reloadCartPage:function(){
        document.location.reload();
    }
};
var addBodyClass = "";

var body = document.body;
console.log("Class Name: crawlapps_body_"+shopifyTheme);
addBodyClass = "crawlapps_body_"+shopifyTheme;
body.classList.add(addBodyClass);

if(jqueryLoaded){
    setTimeout(function(){
        $(document).ready(function () {
            Window.crawlapps.init();
        });
    },2000);
}else{
    $(document).ready(function () {
        Window.crawlapps.init();
    });
}



function convertToSlug(Text)
{
    return Text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-');
}


function allThemes() {

    let addedCode = false;
    let dropdown = '<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_dropdown"></div>';

    let swatch = '<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_swatch"></div>';

    let grid = '<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_gridview" ></div>';


    // Dropdown
    if($('.crawlapps_offers_dropdown').length <= 0 ) {
        let atc = "";
        switch (addBodyClass) {
            case 'crawlapps_body_colors':
                 atc = $('form[action="/cart/add"]:first > .expanded > #addToCart-product-template').parent();
                atc.before(dropdown);
                addedCode = true;
                break;
            case 'crawlapps_body_editions':
            case 'crawlapps_body_editionsv960':
                atc = $('form[action="/cart/add"]:first > .product-options');
                atc.append(dropdown);
                addedCode = true;
                break;
            case 'crawlapps_body_launch':
            case 'crawlapps_body_launchv630':
                atc = $('form[action="/cart/add"]:first > .add-to-cart');
                atc.before(dropdown);
                addedCode = true;
                break;
            case 'crawlapps_body_startup':
            case 'crawlapps_body_startupv921':
                atc = $('form[action="/cart/add"]:first > .smart-payments');
                atc.before(dropdown);
                addedCode = true;
                break;
            case 'crawlapps_body_reach':
            case 'crawlapps_body_reachv440':
                atc = $('form[action="/cart/add"]:first > .product-form-scrollable .product-form-atc');
                atc.before(dropdown);
                addedCode = true;
                break;
            case 'crawlapps_body_atlantic':
            case 'crawlapps_body_atlanticv1421':
                atc = $('form[action="/cart/add"]:first > .addtocart-button-active');
                atc.before(dropdown);
                addedCode = true;
                break;
            case 'crawlapps_body_loft':
            case 'crawlapps_body_loft-14101':
                atc = $('form[action="/cart/add"]:first > #ProductSection-product-template .product-single .product-mobile .product-details .product-smart-wrapper');
                atc.before(dropdown);
                addedCode = true;
                break;
            case 'crawlapps_body_grid':
            case 'crawlapps_body_gridv460':
                atc = $('form[action="/cart/add"]:first > .product-add-to-cart');
                atc.before(dropdown);
                addedCode = true;
                break;
            case 'crawlapps_body_pacific':
            case 'crawlapps_body_pacificv430':
                atc = $('form[action="/cart/add"]:first > .product-submit');
                atc.before(dropdown);
                addedCode = true;
                break;
            case 'crawlapps_body_vogue':
            case 'crawlapps_body_voguev450':
                atc = $('form[action="/cart/add"]:first > div.product-add');
                atc.before(dropdown);
                addedCode = true;
                break;
            case 'crawlapps_body_kagami':
                atc = $('form[action="/cart/add"]:first > div.product__buy');
                atc.before(dropdown);
                addedCode = true;
                break;
            case 'crawlapps_body_triss-shopify-theme':
                atc = $('form[action="/cart/add"]:first > #AddToCart');
                atc.before(dropdown);
                addedCode = true;
                break;
            case 'crawlapps_body_symetry':
            case 'crawlapps_body_symetry-301':
                atc = $('form[action="/cart/add"]:first > .quantity-submit-row');
                atc.before(dropdown);

                addedCode = true;
                break;
            case 'crawlapps_body_foodly':
            case 'crawlapps_body_foodlythemev18':
                atc = $('form[action="/cart/add"]:first > div .js-add-to-card').parent();
                atc.before(dropdown);
                addedCode = true;
                break;
            case 'crawlapps_body_basel-premium-theme':
                atc = $('form[action="/cart/add"]:first #shopify_quantity');
                atc.before(dropdown);
                addedCode = true;
                break;
            case 'crawlapps_body_testament':
                atc = $('form[action="/cart/add"]:first > .product-add');
                atc.before(dropdown);
                addedCode = true;
                break;
            case 'crawlapps_body_adeline-offical':
                atc = $('form[action="/cart/add"]:first > div #add-to-cart').parent();
                atc.before(dropdown);
                addedCode = true;
                break;
            case 'crawlapps_body_flow':
            case 'crawlapps_body_flow1':
                atc = $('form[action="/cart/add"]:first > #AddToCart');
                atc.before(dropdown);
                addedCode = true;
                break;
            case 'crawlapps_body_minimal':
                atc = $('form[action="/cart/add"]:first > #AddToCart');
                atc.before(dropdown);
                addedCode = true;
                break;
            case 'crawlapps_body_prestige':
                atc = $('form[action="/cart/add"]:first > .ProductForm__AddToCart');
                atc.before(dropdown);
                addedCode = true;
                break;
            case 'crawlapps_body_showtime':
                atc = $('form[action="/cart/add"]:first > .desc_blk > .variations');
                atc.after(dropdown);
                addedCode = true;
                break;
        }
    }

    // Swatch
    if($('.crawlapps_offers_swatch').length <= 0 ) {
        let atc = "";
        switch (addBodyClass) {
            case 'crawlapps_body_colors':
                atc = $('form[action="/cart/add"]:first > .expanded > #addToCart-product-template').parent();
                atc.before(swatch);
                addedCode = true;
                break;
            case 'crawlapps_body_editions':
            case 'crawlapps_body_editionsv960':
                    atc = $('form[action="/cart/add"]:first > .product-options');
                    atc.append(swatch);
                    addedCode = true;
                break;
            case 'crawlapps_body_launch':
            case 'crawlapps_body_launchv630':
                atc = $('form[action="/cart/add"]:first > .add-to-cart');
                atc.before(swatch);
                addedCode = true;
                break;
            case 'crawlapps_body_startup':
            case 'crawlapps_body_startupv921':
                atc = $('form[action="/cart/add"]:first > .smart-payments');
                atc.before(swatch);
                addedCode = true;
                break;
            case 'crawlapps_body_reach':
            case 'crawlapps_body_reachv440':
                atc = $('form[action="/cart/add"]:first > .product-form-scrollable .product-form-atc');
                atc.before(swatch);
                addedCode = true;
                break;
            case 'crawlapps_body_atlantic':
            case 'crawlapps_body_atlanticv1421':
                atc = $('form[action="/cart/add"]:first > .addtocart-button-active');
                atc.before(swatch);
                addedCode = true;
                break;
            case 'crawlapps_body_loft':
            case 'crawlapps_body_loft-14101':
                atc = $('form[action="/cart/add"]:first > #ProductSection-product-template .product-single .product-mobile .product-details .product-smart-wrapper');
                atc.before(swatch);
                addedCode = true;
                break;
            case 'crawlapps_body_grid':
            case 'crawlapps_body_gridv460':
                atc = $('form[action="/cart/add"]:first > .product-add-to-cart');
                atc.before(swatch);
                addedCode = true;
                break;
            case 'crawlapps_body_pacific':
            case 'crawlapps_body_pacificv430':
                atc = $('form[action="/cart/add"]:first > .product-submit');
                atc.before(swatch);
                addedCode = true;
                break;
            case 'crawlapps_body_vogue':
            case 'crawlapps_body_voguev450':
                atc = $('form[action="/cart/add"]:first > div.product-add');
                atc.before(swatch);
                addedCode = true;
                break;
            case 'crawlapps_body_kagami':
                atc = $('form[action="/cart/add"]:first > div.product__buy');
                atc.before(swatch);
                addedCode = true;
                break;
            case 'crawlapps_body_triss-shopify-theme':
                atc = $('form[action="/cart/add"]:first > #AddToCart');
                atc.before(swatch);
                addedCode = true;
                break;
            case 'crawlapps_body_symetry':
            case 'crawlapps_body_symetry-301':
                atc = $('form[action="/cart/add"]:first > .quantity-submit-row');
                atc.before(swatch);
                addedCode = true;
                break;
            case 'crawlapps_body_foodly':
            case 'crawlapps_body_foodlythemev18':
                atc = $('form[action="/cart/add"]:first > div .js-add-to-card').parent();
                atc.before(swatch);
                addedCode = true;
                break;
            case 'crawlapps_body_basel-premium-theme':
                atc = $('form[action="/cart/add"]:first #shopify_quantity');
                atc.before(swatch);
                addedCode = true;
                break;
            case 'crawlapps_body_testament':
                atc = $('form[action="/cart/add"]:first > .product-add');
                atc.before(swatch);
                addedCode = true;
                break;
            case 'crawlapps_body_adeline-offical':
                atc = $('form[action="/cart/add"]:first > div #add-to-cart').parent();
                atc.before(swatch);
                addedCode = true;
                break;
            case 'crawlapps_body_flow':
            case 'crawlapps_body_flow1':
                atc = $('form[action="/cart/add"]:first > #AddToCart');
                atc.before(swatch);
                addedCode = true;
                break;
            case 'crawlapps_body_minimal':
                atc = $('form[action="/cart/add"]:first > #AddToCart');
                atc.before(swatch);
                addedCode = true;
                break;
            case 'crawlapps_body_prestige':
                atc = $('form[action="/cart/add"]:first > .ProductForm__AddToCart');
                atc.before(swatch);
                addedCode = true;
                break;
            case 'crawlapps_body_showtime':
                atc = $('form[action="/cart/add"]:first > .desc_blk > .variations');
                atc.after(swatch);
                addedCode = true;
                break;
        }
    }

    // Grid
    if($('.crawlapps_offers_gridview').length <= 0 ) {
        let atc = "";
        switch (addBodyClass) {
            case 'crawlapps_body_colors':
                atc = $('form[action="/cart/add"]:first > .expanded > #addToCart-product-template').parent();
                atc.after(grid);
                addedCode = true;
                break;
            case 'crawlapps_body_editions':
            case 'crawlapps_body_editionsv960':
                atc = $('form[action="/cart/add"]:first > .product-add-to-cart');
                atc.after(grid);
                addedCode = true;
                break;
            case 'crawlapps_body_launch':
            case 'crawlapps_body_launchv630':
                atc = $('form[action="/cart/add"]:first > .add-to-cart');
                atc.after(grid);
                addedCode = true;
                break;
            case 'crawlapps_body_startup':
            case 'crawlapps_body_startupv921':
                atc = $('form[action="/cart/add"]:first > .smart-payments');
                atc.after(grid);
                addedCode = true;
                break;
            case 'crawlapps_body_reach':
            case 'crawlapps_body_reachv440':
                atc = $('form[action="/cart/add"]:first > .product-form-scrollable .product-form-atc');
                atc.after(grid);
                addedCode = true;
                break;
            case 'crawlapps_body_atlantic':
            case 'crawlapps_body_atlanticv1421':
                atc = $('form[action="/cart/add"]:first');
                atc.append(grid);
                addedCode = true;
                break;
            case 'crawlapps_body_loft':
            case 'crawlapps_body_loft-14101':
                atc = $('form[action="/cart/add"]:first > #ProductSection-product-template .product-single .product-mobile .product-details .product-smart-wrapper');
                atc.after(grid);
                addedCode = true;
                break;
            case 'crawlapps_body_grid':
            case 'crawlapps_body_gridv460':
                atc = $('form[action="/cart/add"]:first > .product-add-to-cart');
                atc.after(grid);
                addedCode = true;
                break;
            case 'crawlapps_body_pacific':
            case 'crawlapps_body_pacificv430':
                atc = $('form[action="/cart/add"]:first > .product-submit');
                atc.after(grid);
                addedCode = true;
                break;
            case 'crawlapps_body_vogue':
            case 'crawlapps_body_voguev450':
                atc = $('form[action="/cart/add"]:first > div.product-add');
                atc.after(grid);
                addedCode = true;
                break;
            case 'crawlapps_body_kagami':
                atc = $('form[action="/cart/add"]:first > div.product__buy');
                atc.after(grid);
                addedCode = true;
                break;
            case 'crawlapps_body_triss-shopify-theme':
                atc = $('form[action="/cart/add"]:first > #AddToCart');
                atc.after(grid);
                addedCode = true;
                break;
            case 'crawlapps_body_symetry':
            case 'crawlapps_body_symetry-301':
                atc = $('form[action="/cart/add"]:first > .quantity-submit-row');
                atc.after(grid);
                addedCode = true;
                break;
            case 'crawlapps_body_foodly':
            case 'crawlapps_body_foodlythemev18':
                atc = $('form[action="/cart/add"]:first > div .js-add-to-card').parent();
                atc.after(grid);
                addedCode = true;
                break;
            case 'crawlapps_body_basel-premium-theme':
                atc = $('form[action="/cart/add"]:first #shopify_add_to_cart');
                atc.after(grid);
                addedCode = true;
                break;
            case 'crawlapps_body_testament':
                atc = $('form[action="/cart/add"]:first > .product-add');
                atc.after(grid);
                addedCode = true;
                break;
            case 'crawlapps_body_adeline-offical':
                atc = $('form[action="/cart/add"]:first > div #add-to-cart').parent();
                atc.after(grid);
                addedCode = true;
                break;
            case 'crawlapps_body_flow':
            case 'crawlapps_body_flow1':
                atc = $('form[action="/cart/add"]:first');
                atc.append(grid);
                addedCode = true;
                break;
            case 'crawlapps_body_minimal':
                atc = $('form[action="/cart/add"]:first');
                atc.append(grid);
                addedCode = true;
                break;
            case 'crawlapps_body_prestige':
                atc = $('form[action="/cart/add"]:first');
                atc.append(grid);
                addedCode = true;
                break;
            case 'crawlapps_body_showtime':
                atc = $('form[action="/cart/add"]:first > .desc_blk > .desc_blk_bot');
                atc.after(grid);
                addedCode = true;
                break;
        }
    }

    return addedCode;
}
