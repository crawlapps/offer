@php
    $space = " ";
    $html = "<p class='dropdown_headline'>".$ds->text->display_headline_text."</p>";
        $html .= "<select class='crawlapps_css_dropdown read_offers_data' name='properties[OfferType]' id='offerChange_dropdown'>";
            if($ds->text->is_buy_1_enabled)
            $html .= "<option value='1'>".$ds->text->selectText."</option>";

            foreach ($reulset as $key => $val){
                $html .= '<option data-buy="'.$val->buy.'" data-get="'.$val->get.'" data-offer="'.$val->offer_type.'" data-code="'.$val->code.'_T'.++$key.'" value="'.$val->code.'_T'.$key.'">';

                if($val->get == 0){
                    $html .=
                            $ds->text->before_qty.
                            $space.
                            $val->buy;
                }else{
                        if($val->offer_type == "free"){
                            $html .=
                                    $ds->text->before_qty.
                                    $space.
                                    $val->buy.
                                    $space.
                                    $ds->text->after_qty.
                                    $space.
                                    $ds->text->before_price.
                                    $space.
                                    $val->get.
                                    $space.
                                    $ds->text->after_price.
                                    $space.
                                    $ds->type['free']['text'];
                        }
                        elseif ($val->offer_type == "fixed"){
                            $html .=
                                    $ds->text->before_qty.
                                    $space.
                                    $val->buy.
                                    $space.
                                    $ds->text->after_qty.
                                    $space.
                                    $ds->text->before_price.
                                    $space.
                                    $ds->type['fixed']['symbol'].
                                    $val->get.
                                    $space.
                                    $ds->text->after_price.
                                    $space.
                                    $ds->type['fixed']['text'];
                        }
                        elseif ($val->offer_type == "percentage"){
                            $html .=
                                    $ds->text->before_qty.
                                    $space.
                                    $val->buy.
                                    $space.
                                    $ds->text->after_qty.
                                    $space.
                                    $ds->text->before_price.
                                    $space.
                                    $val->get.
                                    $ds->type['percentage']['symbol'].
                                    $space.
                                    $ds->text->after_price.
                                    $space.
                                    $ds->type['percentage']['text'];
                        }
                }
                $html .='</option>';
            }
            $html .= '</select>';
    echo $html;
@endphp

<style>
    .crawlapps_css_dropdown{
        background-color: {{ $ds->style->bg_color  }} !important;
        font-size: {{ $ds->style->font_size  }}px !important;
        color: {{ $ds->style->text_color  }} !important;
        border-width: {{ $ds->style->border_size  }}px !important;
        border-color: {{ $ds->style->border_color  }} !important;
        padding: {{ $ds->style->padding }} !important;
        border-radius: {{ $ds->style->border_radius }}px !important;
        font-family: {{ $ds->style->font_family }} !important;
    }
    .crawlapps_css_dropdown:focus{
        outline: none !important;
    }
    .dropdown_headline{
        color: {{ $ds->style->color_display_headline_text  }} !important;
        font-size: {{ $ds->style->font_size_display_headline_text  }}px !important;
        margin-bottom: 5px;
    }
    @php echo $gs->advanced_css @endphp
</style>
<script>


    var el = document.getElementById('offerChange_dropdown');
    el.onchange = clickerFn;
    function clickerFn(button) {
        button = button.target;
        let get = button.selectedOptions[0].getAttribute('data-get')
        let buy = button.selectedOptions[0].getAttribute('data-buy') ? button.selectedOptions[0].getAttribute('data-buy'): 1;
        let offer = button.selectedOptions[0].getAttribute('data-offer');
        var values = 0;
        let quantity = document.querySelector('[name="quantity"]');
        if(quantity == null){
            var div = document.querySelector('[action="/cart/add"]' );
            var newinput = document.createElement( 'input' );
            newinput.setAttribute('name','quantity');
            newinput.setAttribute('type','hidden');
            div.appendChild( newinput);
        }
        if(offer=="free"){
            values = parseInt(buy) + parseInt(get);
            document.querySelector('[name="quantity"]').setAttribute('value',parseInt(values));
        }else{
            values = buy;
            document.querySelector('[name="quantity"]').setAttribute('value',parseInt(values));
        }
    }
    $("#country option[value='Angola']").attr('selected','selected');
    @php echo $gs->advanced_js @endphp
</script>


<style>
    .crawlapps_body_venture .crawlapps_offers_dropdown{
        padding: 0 5px;
        flex: 1 0 auto;
    }
    .crawlapps_body_venture .crawlapps_offers_dropdown .dropdown_headline{
        /*font-size: 0.75em !important;*/
        font-weight: 700;
        /*color: #666 !important;*/
        text-transform: uppercase;
        margin-bottom: 13px;
    }
    .crawlapps_body_venture .crawlapps_offers_dropdown .crawlapps_css_dropdown{
        width: 100%;
    }
    .crawlapps_body_venture .crawlapps_offers_dropdown .crawlapps_css_dropdown{
        border: solid !important;
    }
    .crawlapps_body_venture .crawlapps_offers_dropdown select{
        margin-bottom: 10px;
    }
    .crawlapps_body_debut .dropdown_headline{
        margin-bottom: 5px;
        color: #3d4246 !important;
        /*font-size: 16px !important;*/
    }
    .crawlapps_body_debut .crawlapps_offers_dropdown{
        width: 50%;
        margin-bottom: 10px;
        padding: 0 5px;
    }
    .crawlapps_body_debut .crawlapps_offers_dropdown select{
        margin-bottom: 10px;
        width: 100%;
    }
    .crawlapps_body_minimal .crawlapps_offers_dropdown select{
        background-position: top 15px right 6px !important;
        /*background-position-x: 120% !important;
        background-position-y: 0% !important;*/
        margin-bottom: 17px;
    }
    .crawlapps_body_boundless .crawlapps_offers_dropdown select{
        margin-bottom: 10px;
    }
    .crawlapps_body_boundless .crawlapps_offers_dropdown .crawlapps_css_dropdown{
        border: {{ $ds->style->border_size  }}px solid !important;
        width: 100%;
    }
    .crawlapps_body_narrative .crawlapps_css_dropdown{
        width: 100%;
    }
    .crawlapps_body_narrative .product-form__item ~ .shopify-payment-btn{
        margin-top: 0 !important;
    }
    .crawlapps_body_simple .crawlapps_css_dropdown{
        border-style: solid !important;
    }
    .crawlapps_body_simple .crawlapps_css_dropdown.selector-wrapper {
        /*width: 50%;*/
    }
    .crawlapps_body_motion .crawlapps_offers_dropdown{
        margin-bottom: 20px;
    }
    .crawlapps_body_motion .crawlapps_offers_dropdown select{
        width: 100%;
    }
    .crawlapps_body_brooklyn .crawlapps_offers_dropdown select {
        font-family: "Old Standard TT",serif;
        font-style: normal;
        width: calc(100% - 19px);
        display: block;
        margin-left: 5px;
    }
    .crawlapps_body_booster .crawlapps_offers_dropdown {
        float: left;
        padding: 0px 0px;
        width: 100%;
        margin-bottom: 10px;
    }
    .crawlapps_body_booster .crawlapps_css_dropdown{
        width: 100%;
    }
    .crawlapps_body_parallax .purchase-details.smart-payment-button--false{
        flex-wrap: wrap;
    }
    .crawlapps_body_parallax .crawlapps_offers_dropdown{
        width: 100%;
    }
    .crawlapps_body_parallax .crawlapps_offers_dropdown .crawlapps_css_dropdown{
        margin-bottom: 0;
    }
    .crawlapps_body_turbo .crawlapps_offers_dropdown{
        width: 100%;
        margin-top:15px;
    }
    .crawlapps_body_showtime #content .pro_main_c .desc_blk .desc_blk_bot .price.smart_checkout_price_pos{
        float: left;
    }
    .crawlapps_body_showtime .crawlapps_offers_dropdown {
        padding: 0 35px 10px 35px;
        margin-bottom: 5px;

    }
    .crawlapps_body_showtime .crawlapps_css_dropdown{
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-shadow: none;
        color: #555;
        display: block;
        font-size: 14px;
        height: 34px;
        line-height: 1.42857;
        padding: 6px 12px;
        transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
        width: 100%;
    }
    .crawlapps_body_streamline .crawlapps_offers_dropdown{
        margin-bottom: 15px;
    }
    .crawlapps_body_streamline .crawlapps_css_dropdown{
        border-style: solid !important;
        width: 100%;
    }
    .crawlapps_body_minimart .crawlapps_offers_dropdown{
        margin-bottom: 15px;
    }
    .crawlapps_body_minimart .crawlapps_css_dropdown{
        width: 100%;
        background-image: url(//cdn.shopify.com/s/files/1/0273/2942/9597/t/38/assets/select-arrow.png?5327);
        background-repeat: no-repeat;
        background-position:right center;
        -webkit-appearance: none;
        appearance: none;
    }
    .crawlapps_body_icon .crawlapps_css_dropdown{
        width: 100%;
        -webkit-appearance: none;
        appearance: none;
        background-image: url(//cdn.shopify.com/s/files/1/0273/2942/9597/t/38/assets/select-arrow.png?5327);
        background-repeat: no-repeat;
        background-position: right center;
    }
    .crawlapps_body_empire .crawlapps_offers_dropdown .crawlapps_css_dropdown{
        width: 100%;
        -webkit-appearance: none;
        appearance: none;
        background-image: url(//cdn.shopify.com/s/files/1/0273/2942/9597/t/38/assets/select-arrow.png?5327);
        background-repeat: no-repeat;
        background-position: right center;
    }
    .crawlapps_body_ella .crawlapps_offers_dropdown p{
        margin-bottom: 10px;
    }
    .crawlapps_body_ella .crawlapps_offers_dropdown .crawlapps_css_dropdown{
        -webkit-appearance: none;
        appearance: none;
        min-width: 320px;
        margin-bottom: 15px;
        height: auto;
    }
    .crawlapps_body_ella .crawlapps_offers_dropdown{
        position: relative;
        min-width: 300px;
        display: inline-block;
    }
    .crawlapps_body_ella .crawlapps_offers_dropdown:after{
        content: ' ';
        display: block;
        position: absolute;
        bottom: 39%;
        right: 9px;
        margin-top: -3px;
        width: 0;
        height: 0;
        border-style: solid;
        border-width: 4px 4px 0;
        border-color: #333333 transparent transparent transparent;
    }
    .crawlapps_body_seiko .crawlapps_offers_dropdown{
        text-align: right;
        margin-bottom: 15px;
    }
    .crawlapps_body_shoptimized .crawlapps_offers_dropdown .crawlapps_css_dropdown{
        min-width: 170px;
    }
    .crawlapps_body_masonry .crawlapps_offers_dropdown{
        margin-bottom: 1.5em;
    }
    .crawlapps_body_hustler .crawlapps_offers_dropdown{
        margin-bottom: 15px;
        width: 100%;
    }
    .crawlapps_body_hustler .crawlapps_offers_dropdown .crawlapps_css_dropdown{
        width: 100%;
    }
    .crawlapps_body_district .crawlapps_offers_dropdown{
        width: calc(50% - 10px);
        margin-bottom: 15px;
    }
    .crawlapps_body_district .crawlapps_offers_dropdown .crawlapps_css_dropdown{
        width: 100%;
    }
    .crawlapps_body_pacificv430 .shopify-product-form, .crawlapps_body_pacific .shopify-product-form{
        text-align:center;
    }
    .crawlapps_body_pacificv430 .crawlapps_offers_dropdown, .crawlapps_body_pacific .crawlapps_offers_dropdown{
        display: inline-block;
        width: 33.33%;
    }
    .crawlapps_body_pacificv430 .dropdown_headline, .crawlapps_body_pacific .dropdown_headline{
        text-align:left;
    }
    .crawlapps_body_pacificv430 .crawlapps_css_dropdown, .crawlapps_body_pacific .crawlapps_css_dropdown{
        background-image:url(https://offer.crawlapps.com/images/small-down.png);
        background-repeat: no-repeat;
        background-position: center right 10px;
    }
    .crawlapps_body_superstorev232 .crawlapps_css_dropdown, .crawlapps_body_superstore .crawlapps_css_dropdown{
        margin-bottom: 10px;
        width: 100%;
        font-family: Slate,sans-serif;
        font-style: normal;
        font-weight: 200;
        font-size: 0.9375rem;
        z-index: 1;
        width: 100%;
        padding: 0.9375rem 0.875rem 0.9375rem;
        letter-spacing: 0em;
        color: #212121;
        background-color: #fff;
        border: 1px solid #212121;
        border-radius: 3px;
    }
    .crawlapps_body_turbo .dropdown_headline{
        margin-bottom: 0.5em;
    }
    .crawlapps_body_parallax .purchase-details{
        flex-wrap: wrap;
    }
    .crawlapps_body_retina .crawlapps_offers_dropdown{
        width:100%;
    }
    .crawlapps_body_retina .purchase-details{
        flex-wrap: wrap;
    }
    .crawlapps_body_kingdom330 .crawlapps_css_dropdown, .crawlapps_body_kingdom .crawlapps_css_dropdown{
        background-image:url(https://offer.crawlapps.com/images/small-down.png);
        background-repeat: no-repeat;
        background-position: center right 10px;
        width:100%;
        margin-bottom:25px;
    }
    .crawlapps_body_gridv460 .crawlapps_css_dropdown, .crawlapps_body_grid .crawlapps_css_dropdown{
        width:100%;
        max-width: 220px;
        background: #fff;
        padding: 10px;
        border-color: #e2e2e2;
        color: #888;
    }
    .crawlapps_body_loft-14101 .crawlapps_css_dropdown, .crawlapps_body_loft .crawlapps_css_dropdown{
        background-image:url(https://offer.crawlapps.com/images/small-down.png);
        background-repeat: no-repeat;
        background-position: center right 10px;
        width:100%;
        max-width:250px;
    }
    .crawlapps_body_atlanticv1421 .crawlapps_css_dropdown, .crawlapps_body_atlantic .crawlapps_css_dropdown{
        width:100%;
        font-family: "Avenir Next",sans-serif;
        font-style: normal;
        font-weight: 300;
        position: relative;
        display: inline-block;
        padding: 14px;
        font-size: 14px;
        line-height: 1.5;
        color: #052422;
        text-align: left;
        cursor: pointer;
        border: 1px solid #ebebeb;
        border-radius: 0;
        outline: 0;
        background: #fff;
    }
    .crawlapps_body_empirev511 .crawlapps_css_dropdown, .crawlapps_body_empire .crawlapps_css_dropdown{
        width:100%;
        z-index: 2;
        width: 100%;
        height: 100%;
        padding-right: 1.75rem;
        color: #4d4d4d;
        cursor: pointer;
        background-color: #fff;
        box-shadow: 0 1px 2px 0 rgba(0,0,0,0.15);
        -webkit-appearance: none;
        padding: 1.5rem 0.625rem 0.375rem;
    }
    .crawlapps_body_reachv440 .crawlapps_css_dropdown, .crawlapps_body_reach .crawlapps_css_dropdown{
        width: 100%;
        font-family: "Source Sans Pro",sans-serif;
        font-style: normal;
        font-weight: 300;
        height: 100%;
        padding: 0.875rem 2.5rem 0.875rem 1rem;
        color: #48392e;
        cursor: pointer;
        background: #f9f7f6;
        border: 0;
        -webkit-appearance: none;
    }
    .crawlapps_body_startupv921 .crawlapps_css_dropdown, .crawlapps_body_startup .crawlapps_css_dropdown{
        width:100%;
        position: relative;
        padding: 13px 45px 13px 15px;
        font-size: 14px;
        color: #1d1b44;
        cursor: pointer;
        background-color: #fff;
        border: 1px solid #f0eff2;
        border-radius: 5px;
    }
    .crawlapps_body_california .crawlapps_offers_dropdown{
        margin-top:15px;
    }
    .crawlapps_body_california .crawlapps_css_dropdown{
        width:100%;
    }
    .crawlapps_body_launchv630 .crawlapps_css_dropdown, .crawlapps_body_launch .crawlapps_css_dropdown{
        width:45%;
    }
    .crawlapps_body_launchv630 .dropdown_headline, .crawlapps_body_launch .dropdown_headline{
        padding:0;
    }
    .crawlapps_body_district375 .crawlapps_offers_dropdown, .crawlapps_body_district .crawlapps_offers_dropdown{
        flex: 0 0 100%;
        padding-left: 10px;
        margin-bottom: 15px;
    }
    .crawlapps_body_district375 .crawlapps_css_dropdown, .crawlapps_body_district .crawlapps_css_dropdown{
        width: 100%;
    }
    .crawlapps_body_editionsv960 .crawlapps_css_dropdown, .crawlapps_body_editions .crawlapps_css_dropdown{
        display: block;
        margin: 0 auto;
        font-family: Slate,sans-serif;
        font-style: normal;
        font-weight: 200;
        position: relative;
        padding: 9px 10px 11px;
        font-size: 13px;
        letter-spacing: 1px;
        color: #232c3f;
        text-align: left;
        background-color: rgba(35,44,63,0.1);
        border-radius: 3px;
        -moz-background-clip: padding;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
    }
    .crawlapps_body_colors .crawlapps_css_dropdown{
        width: 100%;
        margin-bottom: 20px;
    }
    .crawlapps_body_story .crawlapps_css_dropdown{
        width:100%;
    }
    .crawlapps_body_flow1 .crawlapps_offers_dropdown .crawlapps_css_dropdown, .crawlapps_body_flow .crawlapps_offers_dropdown .crawlapps_css_dropdown{
        margin-bottom: 20px;
    }
    .crawlapps_body_motionv502 .crawlapps_offers_dropdown .crawlapps_css_dropdown, .crawlapps_body_motion .crawlapps_offers_dropdown .crawlapps_css_dropdown{
        width: 100%;
        margin-bottom: 20px;
    }
    .crawlapps_body_voguev450 .crawlapps_css_dropdown, .crawlapps_body_vogue .crawlapps_css_dropdown{
        width: 100%;
        background-color: transparent;
        padding: 15px 24px;
        line-height: 1;
    }
    .crawlapps_body_envy .crawlapps_offers_dropdown{
        float: left;
        width: 100%;
        padding-left: 15px;
        padding-right: 15px;
    }
    .crawlapps_body_envy .crawlapps_offers_dropdown select{
        width:100%;
    }
    .crawlapps_body_adeline-offical .page-product .product-info .product-action .action-button{
        clear: both;
    }
    .crawlapps_body_adeline-offical .crawlapps_offers_dropdown{
        width: 60%;
        margin-bottom: 20px;
    }
    .crawlapps_body_basel-premium-theme .crawlapps_css_dropdown{
        width:100%;
        height: auto;
        margin-bottom: 20px;
    }
    .crawlapps_body_foodlythemev18 .crawlapps_css_dropdown, .crawlapps_body_foodly .crawlapps_css_dropdown{
        border-style: solid;
        max-width: 280px;
        background-image:url(https://offer.crawlapps.com/images/small-down.png);
        background-repeat: no-repeat;
        background-position: center right 10px;
        margin-bottom: 20px;
    }
    .crawlapps_body_triss-shopify-theme .crawlapps_offers_dropdown{
        float: left;
        width: 100%;
    }
    .crawlapps_body_triss-shopify-theme .crawlapps_css_dropdown{
        width: 100%;
        margin-bottom: 20px;
    }
    .crawlapps_body_kagami .crawlapps_css_dropdown{
        background-image:url(https://offer.crawlapps.com/images/small-down.png);
        background-repeat: no-repeat;
        background-position: center right 10px;
        margin-bottom: 20px;
    }
    .crawlapps_body_artisan .crawlapps_offers_dropdown{
        width: 100%;
        margin-bottom: 20px;
    }
    .crawlapps_body_artisan .dropdown_headline{
        margin-top: 0;
    }
    .crawlapps_body_artisan .crawlapps_css_dropdown{
        width:100%;
    }
    .crawlapps_body_streamlinev302 .crawlapps_css_dropdown, .crawlapps_body_streamline .crawlapps_css_dropdown{
        border-style: solid;
        width: 100%;
        margin-bottom: 20px;
    }
    .crawlapps_body_flex .crawlapps_offers_dropdown{
        width: 100%;
        margin-bottom: 10px;
    }
    .crawlapps_body_flex .crawlapps_css_dropdown{
        width: 98%;

        background-color: #fff;
        border-color: #dbdbdb;
        color: #363636;
        cursor: pointer;
        display: block;
        font-size: 1em;
        max-width: 100%;
        padding: 8px;
        outline: none;
    }
    .crawlapps_body_impulsev302 .crawlapps_css_dropdown, .crawlapps_body_impulse .crawlapps_css_dropdown{
        width: 100%;
        margin-bottom: 15px;
    }
    .crawlapps_body_broadcast160crossborder .crawlapps_css_dropdown, .crawlapps_body_broadcast .crawlapps_css_dropdown{
        width: 100%;
    }
    .crawlapps_body_split230 .crawlapps_css_dropdown, .crawlapps_body_split .crawlapps_css_dropdown{
        width: 100%;
        display: block !important;
        background: transparent !important;
        border-color: rgba(37,42,43,0.12) !important;
        font-size: 0.9375rem !important;
        line-height: 1;
        padding: 16px;
    }
    .crawlapps_body_prestige .crawlapps_offers_dropdown{
        margin-bottom: 30px;

    }

    .crawlapps_body_prestige .crawlapps_css_dropdown{
        position: relative;
        width: 100%;
        text-align: left;
        padding: 10px 28px 10px 14px;
        border: 1px solid #efefef;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .crawlapps_body_symetry-301 .crawlapps_offers_dropdown .crawlapps_css_dropdown, .crawlapps_body_symetry .crawlapps_offers_dropdown .crawlapps_css_dropdown{
        height: 30px;
        line-height: 30px;
        padding: 4px 14px 4px 4px;
        border: 1px solid #e2e2e2;
    }

    .crawlapps_body_express .crawlapps_css_dropdown{
        background-size: 1.2rem;
        font-size: 1.6rem;
        text-overflow: '';
        cursor: pointer;
        appearance: none;
        text-align-last: right;
        border-radius: 0;
        width: 100%;
        padding: .7em .25em .7em 0;
        background-color: #FFFBF6;
        border-top: none;
        border-left: none;
        border-right: none;
        border-bottom: 1px solid;
    }

    @media screen and (max-width: 440px){
        .crawlapps_body_district .crawlapps_offers_dropdown{
            width: 100%;
        }
    }
    @media (max-width: 480px){
        .crawlapps_body_showtime .crawlapps_offers_dropdown {
            float: none;
            margin: 0 auto;
            display: block;
            text-align: center;
        }
    }
    @media (min-width: 591px) and (max-width: 1008px){
        .crawlapps_body_brooklyn .crawlapps_offers_dropdown select{
            width: calc(100% - 10px);
            margin-left: 0;
        }
    }
    @media (max-width: 989px) {
        .crawlapps_body_debut .crawlapps_offers_dropdown{
            padding: 0;
            width: 100%;
        }
        .crawlapps_body_debut .crawlapps_offers_dropdown select{
            margin-bottom: 0;
        }
        .crawlapps_body_debut .product-form__controls-group ~ .product-form__controls-group--submit{
            margin-top: 0;
        }
    }
    @media (min-width: 799px){
        .crawlapps_body_turbo .crawlapps_offers_dropdown{
            width: calc(50% - 6px);
            display: inline-block;
            margin-top:0;
        }
        .crawlapps_body_turbo .crawlapps_css_dropdown{
            margin-bottom:0;
        }
    }
    @media (min-width: 769px){
        .crawlapps_body_minimal .crawlapps_css_dropdown{
            width: 50%;
        }
        .crawlapps_body_supply .crawlapps_css_dropdown{
            max-width: 40% !important;
        }
        .crawlapps_body_loft-14101 .crawlapps_css_dropdown, .crawlapps_body_loft .crawlapps_css_dropdown{
            max-width:300px;
        }
    }
    @media (max-width: 989px){
        .crawlapps_body_simple .crawlapps_css_dropdown {
            width: 100%;
        }
    }
    @media (max-width: 417px) {
        .crawlapps_body_brooklyn .crawlapps_offers_dropdown select {
            width: calc(100% - 10px);
            display: block;
            margin-left: 0;
        }
    }
    @media (max-width: 989px) and (min-width: 750px){
        .crawlapps_body_booster .crawlapps_offers_dropdown {
            width: 52%;
        }
    }
    @media (max-width: 740px){
        .crawlapps_body_icon .crawlapps_offers_dropdown{
            margin-top: 15px;
        }
    }
    @media (min-width: 405px) and (max-width: 590px){
        .crawlapps_body_brooklyn .crawlapps_offers_dropdown{
            padding-right: 10px;
        }
        .crawlapps_body_brooklyn .crawlapps_offers_dropdown select{
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 338px;
        }
    }
    @media (min-width: 721px){
        .crawlapps_body_pacificv430 .crawlapps_offers_dropdown,.crawlapps_body_pacific .crawlapps_offers_dropdown{
            min-width: 30%;
        }
    }
    @media screen and (max-width: 1020px){
        .crawlapps_body_launchv630 .crawlapps_css_dropdown, .crawlapps_body_launch .crawlapps_css_dropdown{
            width:100%;
        }
    }
    @media (max-width: 991px){
        .crawlapps_body_envy .crawlapps_offers_dropdown{
            padding-top: 20px;
            width:100%;
        }
    }
</style>
