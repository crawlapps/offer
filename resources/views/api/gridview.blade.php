<p class="dropdown_headline">{{$ds->text->display_headline_text}}</p>
<table class="crawlapps_css_gridview" id="offerChange_gridview">
    <thead class="tbl_gd_thead">
        <td class="tbl_gd_td">{{$ds->text->headerText1}}</td>
        <td class="tbl_gd_td">{{$ds->text->headerText2}}</td>
        @if($ds->button->is_add_btn_enabled)
        <td class="tbl_gd_td">{{$ds->text->headerText3}}</td>
        @endif
    </thead>
    <tbody class="tbl_gd_tbody">
        @php $space = " "; @endphp
        @foreach ($reulset as $key => $val)
        <tr class="tbl_gd_tr read_offers_data"  data-buy="{{$val->buy}}" data-get="{{$val->get}}" data-offer="{{$val->offer_type}}" data-code="{{$val->code}}">
            <td class="tbl_gd_td">
                {{ $ds->text->before_qty.$space.$val->buy.$space }}
            </td>
            <td class="tbl_gd_td">
                @php
                    if($val->get == 0){
                        echo "<font>---</font>";
                    }else{
                        if($val->offer_type == "free"){
                            echo $ds->text->after_qty.
                                $space.
                                $val->get.
                                $space.
                                $ds->text->after_price.
                                $space.
                                $ds->type[$val->offer_type]['text'];
                        }
                        elseif($val->offer_type == "fixed"){
                            echo $ds->text->after_qty.
                                $space.
                                $ds->type[$val->offer_type]['symbol'].
                                $val->get.
                                $space.
                                $ds->text->after_price.
                                $space.
                                $ds->type[$val->offer_type]['text'];
                        }
                        else if($val->offer_type == "percentage"){
                            echo $ds->text->after_qty.
                                $space.
                                $val->get.
                                $ds->type[$val->offer_type]['symbol'].
                                $space.
                                $ds->text->after_price.
                                $space.
                                $ds->type[$val->offer_type]['text'];
                            }
                    }
                @endphp
            </td>
            @if($ds->button->is_add_btn_enabled)
            <td class="tbl_gd_td">
                <button type="button" data-buy="{{$val->buy}}" data-get="{{$val->get}}" data-offer="{{$val->offer_type}}" value="{{$val->code}}_T{{++$key}}"  class="tbl_gd_btn">{{$ds->button->text}}</button>
            </td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>
<input type="hidden" name='properties[OfferType]'  value="" id="crawlapps_property_offer" />

<script>
    var el = document.getElementsByClassName('tbl_gd_btn');
    for (var i=0; i < el.length; i++) {
        el.item(i).onclick = clickerFn;
    }
    function clickerFn(button) {
        button = button.target;
        let input = document.getElementById('crawlapps_property_offer');
        input.setAttribute('value',button.value);
        let get = button.getAttribute('data-get')
        let buy = button.getAttribute('data-buy') ? button.getAttribute('data-buy'): 1;
        let offer = button.getAttribute('data-offer');
        var values = 0;
        let quantity = document.querySelector('[name="quantity"]');
        if(quantity == null){
            var div = document.querySelector('[action="/cart/add"]' );
            var newinput = document.createElement( 'input' );
                newinput.setAttribute('name','quantity');
                newinput.setAttribute('type','hidden');
            div.appendChild( newinput);
        }
        if(offer=="free"){
            values = parseInt(buy) + parseInt(get);
            $('[name="quantity"]').attr('value',values);
        }else{
            values = buy;
            $('[name="quantity"]').attr('value',values);
        }
            $('[name="add"]:first, #AddToCart:first, .product-form--atc-button:first, .add_to_cart_product_page:first, .product-add .add, #shopify_add_to_cart, .js-add-to-card, .quantity-submit-row > input[type="submit"], .product__add-to-cart, button.product-form-submit, button#AddToCartDesk, input.add-to-cart-button, button#addToCart-product-template,.add-to-cart, .product-form-submit-wrap input.button, .addtocart-button-active, .ProductForm__AddToCart ' ).click();
    }
    @php echo $gs->advanced_js @endphp
</script>

@php

    $border_size_half = "0px";
    $border_radius = $ds->style->border_radius."px";
    if($ds->style->border_radius) {
        $border_radius = $ds->style->border_radius + 2;
        $border_radius = $border_radius."px";
    }
@endphp


<style>
    .crawlapps_css_gridview {
        width: auto;
        border-collapse: separate;
        text-shadow: none !important;
        border-radius: {{$border_radius}} !important;
        font-family:{{$ds->style->font_family}} !important;
        font-size:{{$ds->style->font_size}}px !important;
        margin-top:{{$ds->style->border_size}}px !important;
    }
    .crawlapps_css_gridview th,
    .crawlapps_css_gridview td{
        padding: {{$ds->style->td_padding}} !important;
        border: {!! $ds->style->border_size !!}px solid {!! $ds->style->border_color !!} !important;
    }
    .crawlapps_css_gridview thead td{
        background:{{$ds->style->header_bg_color}} !important;
        color:{{$ds->style->header_color}} !important;
    }
    .crawlapps_css_gridview tbody td{
        background:{{$ds->style->body_bg_color}} !important;
        color:{{$ds->style->text_color}} !important;
    }
    .crawlapps_css_gridview thead tr th:first-child,
    .crawlapps_css_gridview thead tr td:first-child{
        border-top-left-radius: {{$ds->style->border_radius}}px;
    }
    .crawlapps_css_gridview thead tr th:last-child,
    .crawlapps_css_gridview thead tr td:last-child{
        border-top-right-radius: {{$ds->style->border_radius}}px;
    }
    .crawlapps_css_gridview tbody tr:last-child td:first-of-type{
        border-bottom-left-radius: {{$ds->style->border_radius}}px;
    }
    .crawlapps_css_gridview tbody tr:last-child td:last-of-type {
        border-bottom-right-radius: {{$ds->style->border_radius}}px;
    }



    .crawlapps_css_gridview tbody tr td:not(:last-of-type){
        border-right: {!! $border_size_half !!} solid {!! $ds->style->border_color !!} !important;
    }

    .crawlapps_css_gridview thead tr td:not(:last-of-type){
        border-right: {!! $border_size_half !!} solid {!! $ds->style->border_color !!} !important;
    }

    .crawlapps_css_gridview tr:not(:last-of-type) td{
        border-bottom: {!! $border_size_half !!} solid {!! $ds->style->border_color !!} !important;
    }

    .crawlapps_css_gridview thead tr td{
        border-bottom: {!! $border_size_half !!} solid {!! $ds->style->border_color !!} !important;
    }



    .crawlapps_css_gridview tbody td .tbl_gd_btn:hover{
        border-color: {{$ds->button->border_color_on_hover}} !important;
    }
    .crawlapps_css_gridview tbody td .tbl_gd_btn:focus{
        outline: none !important;
        border-color: {{$ds->button->border_color_on_hover}} !important;
    }
    .crawlapps_css_gridview tbody td .tbl_gd_btn{
        font-family:{{$ds->button->font_family}} !important;
        font-size:{{$ds->button->font_size}}px !important;
        border-color:{{$ds->button->border_color}} !important;
        border-width:{{$ds->button->border_size}}px !important;
        background-color:{{$ds->button->bg_color}} !important;
        padding:{{$ds->button->padding}} !important;
        color:{{$ds->button->text_color}} !important;
        border-radius:{{$ds->button->border_radius}}px !important;
        border-style:solid !important;
    }


    .dropdown_headline {
        color: {{ $ds->style->color_display_headline_text  }};
        font-size: {{ $ds->style->font_size_display_headline_text  }}px;
        margin-bottom: 5px;
    }

    @media (max-width: 767px) {
        .crawlapps_offers_gridview .dropdown_headline {
            text-align: center;
        }
        .crawlapps_css_gridview {
            margin-left: auto;
            margin-right: auto;
        }
    }

    @php echo $gs->advanced_css @endphp
</style>
<style>
    .crawlapps_body_venture .crawlapps_offers_gridview {
        width: 100%;
        padding: 0 5px;
    }
    .crawlapps_body_venture .crawlapps_offers_gridview table {
        margin-bottom: 10px;
    }
    .crawlapps_body_venture .shopify-payment-button__button--hidden{
        display: none;
    }
    .crawlapps_body_debut .crawlapps_offers_gridview{
        width: 100%;
        padding: 0 5px;
    }
    .crawlapps_body_debut .crawlapps_offers_gridview table{
        margin-bottom: 10px;
    }
    .crawlapps_body_debut .shopify-payment-button__button--hidden{
        display: none;
    }
    .crawlapps_body_minimal .crawlapps_offers_gridview{
        margin-top: -30px;
    }
    .crawlapps_body_minimal .crawlapps_offers_gridview table{
        margin-bottom: 0;
    }
    .crawlapps_body_boundless .crawlapps_offers_gridview table{
        margin-bottom: 10px;
    }
    .crawlapps_body_narrative .crawlapps_offers_gridview{
        margin-top: 10px;
    }
    .crawlapps_body_narrative .shopify-payment-button__button--hidden{
        display: none;
    }
    .crawlapps_body_boundless .shopify-payment-button__button--hidden{
        display: none;
    }
    .crawlapps_body_boundless .crawlapps_offers_gridview{
        margin-top: 10px;
    }
    .crawlapps_body_brooklyn .crawlapps_css_gridview{
        margin: 0 auto;
    }
    .crawlapps_body_brooklyn .crawlapps_css_gridview tr:first-child th:after,
    .crawlapps_body_brooklyn .crawlapps_css_gridview tr:first-child td:after{
        display: none;
    }
    .crawlapps_body_brooklyn .shopify-payment-button__button--hidden{
        display: none;
    }
    .crawlapps_body_brookly .shopify-payment-button__more-options{
        display: none;
    }
    .crawlapps_body_simple .shopify-payment-button__more-options{
        display: none;
    }
    .crawlapps_body_supply .shopify-payment-button__more-options{
        display: none;
    }
    .crawlapps_body_supply .crawlapps_offers_gridview{
        margin-top: 13px;
    }
    .crawlapps_body_motion .crawlapps_offers_gridview{
        margin-top: 10px;
        margin-bottom: 15px;
    }
    .crawlapps_body_motion .cartIcon{
        margin-bottom: 10px;
    }
    .crawlapps_body_responsive .crawlapps_offers_gridview{
        clear: both;
    }
    .crawlapps_body_booster .crawlapps_offers_gridview{
        width: 100%;
        float: left;
    }
    .crawlapps_body_parallax .purchase-details.smart-payment-button--false{
        flex-wrap: wrap;
    }
    .crawlapps_body_parallax .crawlapps_offers_gridview{
        margin-top: 10px;
    }
    .crawlapps_body_turbo .crawlapps_offers_gridview{
        width: 100%;
        margin-top: 10px;
    }
    .crawlapps_body_prestige .crawlapps_offers_gridview{
        width: 100%;
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .crawlapps_body_showtime .shopify-payment-button__more-options{
        display: none;
    }
    .crawlapps_body_streamline .crawlapps_css_gridview{
        background:transparent;
    }
    .crawlapps_body_streamline .crawlapps_offers_gridview{
        margin-top: 10px;
    }
    .crawlapps_body_shoptimized .crawlapps_offers_gridview{
        float: left;
        width: 100%;
        padding: 0 30px;
        margin-top: 15px;
    }
    .crawlapps_body_retina .crawlapps_offers_gridview{
        margin-top: 15px;
    }
    .crawlapps_body_pipeline .crawlapps_offers_gridview{
        margin-top: 15px;
    }
    .crawlapps_body_masonry .crawlapps_offers_gridview{
        margin-top: 15px;
    }
    .crawlapps_body_masonry .crawlapps_css_gridview tbody td .tbl_gd_btn{
        width: auto;
    }
    .crawlapps_body_minimart .crawlapps_offers_gridview{
        margin-bottom: 15px;
    }
    .crawlapps_body_icon .crawlapps_offers_gridview{
        width: 100%;
        float: left;
    }
    .crawlapps_body_hustler .crawlapps_offers_gridview{
        width: 100%;
        float: left;
    }
    .crawlapps_body_ella .crawlapps_offers_gridview{
        width: 100%;
        float: left;
    }
    .crawlapps_body_seiko .crawlapps_offers_gridview{
        width: 100%;
        margin-top: 15px;
    }
    .crawlapps_body_seiko .crawlapps_offers_gridview .dropdown_headline{
        text-align: right;
    }
    .crawlapps_body_seiko .crawlapps_offers_gridview .crawlapps_css_gridview{
        margin-left: auto;
        margin-right: 0;
    }
    .crawlapps_body_district .crawlapps_offers_gridview{
        margin-top: 10px;
    }
    .crawlapps_body_pacificv430 .shopify-product-form, .crawlapps_body_pacific .shopify-product-form{
        text-align:center;
    }
    .crawlapps_body_pacificv430 .crawlapps_offers_gridview, .crawlapps_body_pacific .crawlapps_offers_gridview{
        display: inline-block;
    }
    .crawlapps_body_pacificv430 .dropdown_headline, .crawlapps_body_pacific .dropdown_headline{
        text-align:left;
    }
    .crawlapps_body_parallax .purchase-details{
        flex-wrap: wrap;
    }
    .crawlapps_body_retina .purchase-details{
        flex-wrap: wrap;
    }
    .crawlapps_body_kingdom330 .crawlapps_offers_gridview, .crawlapps_body_kingdom .crawlapps_offers_gridview{
        margin-top:15px;
    }
    .crawlapps_body_gridv460 .crawlapps_css_gridview, .crawlapps_body_grid .crawlapps_css_gridview{
        border-collapse: collapse !important;
    }
    .crawlapps_body_gridv460 .dropdown_headline, .crawlapps_body_grid .dropdown_headline{
        margin-top: 0;
    }
    .crawlapps_body_gridv460 ._2ogcW-Q9I-rgsSkNbRiJzA, .crawlapps_body_grid ._2ogcW-Q9I-rgsSkNbRiJzA{
        display: none;
    }
    .crawlapps_body_loft-14101 ._2ogcW-Q9I-rgsSkNbRiJzA, .crawlapps_body_loft ._2ogcW-Q9I-rgsSkNbRiJzA{
        display: none;
    }
    .crawlapps_body_loft-14101 .crawlapps_offers_gridview, .crawlapps_body_loft .crawlapps_offers_gridview{
        margin-bottom: 20px;
    }
    .crawlapps_body_atlanticv1421 ._2ogcW-Q9I-rgsSkNbRiJzA, .crawlapps_body_atlantic ._2ogcW-Q9I-rgsSkNbRiJzA{
        display: none;
    }
    .crawlapps_body_atlanticv1421 .crawlapps_offers_gridview, .crawlapps_body_atlantic .crawlapps_offers_gridview{
        margin-top: 35px;
    }
    .crawlapps_body_reachv440 .crawlapps_offers_gridview, .crawlapps_body_reach .crawlapps_offers_gridview{
        width: 100%;
        float: left;
    }
    .crawlapps_body_california .crawlapps_offers_gridview{
        margin-top:15px;
    }
    .crawlapps_body_launchv630 ._2ogcW-Q9I-rgsSkNbRiJzA, .crawlapps_body_launch ._2ogcW-Q9I-rgsSkNbRiJzA{
        display: none;
    }
    .crawlapps_body_launchv630 .dropdown_headline, .crawlapps_body_launch .dropdown_headline{
        padding-bottom:0;
    }
    .crawlapps_body_district375 .crawlapps_offers_gridview, .crawlapps_body_district .crawlapps_offers_gridview{
        flex: 0 0 100%;
        padding-left: 10px;
        order: 3;
        margin-top:10px;
    }
    .crawlapps_body_editionsv960 .crawlapps_css_gridview, .crawlapps_body_editions .crawlapps_css_gridview{
        margin: 0 auto;
    }
    .crawlapps_body_editionsv960 .dropdown_headline, .crawlapps_body_editions .dropdown_headline{
        margin-top:0;
    }
    .crawlapps_body_motionv502 .crawlapps_offers_gridview, .crawlapps_body_motion .crawlapps_offers_gridview{
        margin-top: 10px;
    }
    .crawlapps_body_envy .crawlapps_offers_gridview{
        float: left;
        width: 100%;
        padding-left: 15px;
        padding-right: 15px;
    }
    .crawlapps_body_adeline-offical .page-product .product-info .product-action .action-button{
        clear: both;
    }
    .crawlapps_body_adeline-offical .crawlapps_offers_gridview{
        width: 100%;
        float: left;
        margin-top: 15px;
        margin-bottom:15px;
    }
    .crawlapps_body_testament .crawlapps_css_gridview{
        margin-left: auto;
        margin-right: auto;
    }
    .crawlapps_body_basel-premium-theme .crawlapps_offers_gridview{
        margin-top: 15px;
    }
    .crawlapps_body_foodlythemev18 .crawlapps_offers_gridview, .crawlapps_body_foodly .crawlapps_offers_gridview{
        margin-top: 15px;
    }
    .crawlapps_body_foodlythemev18 .crawlapps_css_gridview, .crawlapps_body_foodly .crawlapps_css_gridview{
        margin-left: auto;
        margin-right: auto;
    }
    .crawlapps_body_triss-shopify-theme .crawlapps_offers_gridview{
        width: 100%;
        float: left;
        margin-top: 15px;
    }
    .crawlapps_body_kagami .crawlapps_offers_gridview{
        margin-top: 15px;
    }
    .crawlapps_body_streamlinev302 .crawlapps_offers_gridview, .crawlapps_body_streamline .crawlapps_offers_gridview{
        margin-top: 15px;
    }
    .crawlapps_body_flex ._2ogcW-Q9I-rgsSkNbRiJzA{
        display: none;
    }
    .crawlapps_body_flex .crawlapps_offers_gridview{
        margin-top: 15px;
    }
    .crawlapps_body_impulsev302 .crawlapps_offers_gridview, .crawlapps_body_impulse .crawlapps_offers_gridview{
        margin-top: 15px;
    }
    .crawlapps_body_broadcast160crossborder ._2ogcW-Q9I-rgsSkNbRiJzA, .crawlapps_body_broadcast ._2ogcW-Q9I-rgsSkNbRiJzA{
        display: none;
    }

    .crawlapps_body_express .crawlapps_offers_gridview{
        margin-top: 25px;
    }
    .crawlapps_body_showtime .crawlapps_offers_gridview{
        margin-top: 20px;
    }
    .crawlapps_body_showtime .dropdown_headline{
        text-align: center;
    }
    .crawlapps_body_showtime .crawlapps_css_gridview{
        margin-left: auto;
        margin-right: auto;
    }
    @media (max-width: 989px){
        .crawlapps_body_supply .crawlapps_offers_gridview{
            margin-top: 10px;
        }
    }
    @media (max-width: 767px){
        .crawlapps_body_shoptimized .crawlapps_offers_gridview {
            margin-bottom: 15px;
        }
    }
</style>
