<p class="dropdown_headline">{{$ds->text->display_headline_text}}</p>
@php $space = " "; @endphp
@if($ds->text->is_buy_1_enabled)
<div class="crawlapps_css_swatch">
    <label for="buy1_radio">
        <button type="button" class="swatch_gd_btn" value="1">{{ $ds->text->selectText  }}
        </button>
    </label>
    <input type="radio" id="buy1_radio" style="display:none;" />
</div>
@endif
<ul class="crawlapps_css_swatch_ul"  style="list-style: none;padding: 0;margin: 0;">
@foreach ($reulset as $key => $val)
<li class="crawlapps_css_swatch_li">
    <label for="{{ $val->offer_type }}.'_radio'">
        <button type="button" class="swatch_gd_btn read_offers_data" data-buy="{{$val->buy}}" data-get="{{$val->get}}" data-offer="{{$val->offer_type}}" data-code="{{$val->code}}_T{{++$key}}" value="{{$val->code}}_T{{$key}}">
            @php
                if($val->get == 0){
                    echo $ds->text->before_qty.$space.$val->buy.$space;

                }else{
                    echo $ds->text->before_qty.$space.$val->buy.$space.$ds->text->after_qty;
                    if($val->offer_type == "free"){
                        echo $ds->text->before_price.
                            $space.
                            $val->get.
                            $space.
                            $ds->text->after_price.
                            $space.
                            $ds->type[$val->offer_type]['text'];
                    }
                    elseif($val->offer_type == "fixed"){
                        echo $ds->text->before_price.
                            $space.
                            $ds->type[$val->offer_type]['symbol'].
                            $val->get.
                            $space.
                            $ds->text->after_price.
                            $space.
                            $ds->type[$val->offer_type]['text'];
                    }
                    else if($val->offer_type == "percentage"){
                        echo $ds->text->before_price.
                            $space.
                            $val->get.
                            $ds->type[$val->offer_type]['symbol'].
                            $space.
                            $ds->text->after_price.
                            $space.
                            $ds->type[$val->offer_type]['text'];
                        }
                }

            @endphp
        </button>
    </label>
    <input type="radio" id="{{ $val->offer_type }}.'_radio'" style="display:none;" />
</li>
@endforeach
</ul>
<input type="hidden" name='properties[OfferType]'  value="" id="crawlapps_property_offer" />
<script>
    var el = document.getElementsByClassName('swatch_gd_btn');
    for (var i=0; i < el.length; i++) {
        el.item(i).onclick = clickerFn;
    }
    function clickerFn(button) {
        button = button.target;
        let input = document.getElementById('crawlapps_property_offer');
        input.setAttribute('value',button.value);

        let get = button.getAttribute('data-get');
        let buy = button.getAttribute('data-buy') ? button.getAttribute('data-buy'): 1;
        let offer = button.getAttribute('data-offer');
        var values = 0;
        let quantity = document.querySelector('[name="quantity"]');
        if(quantity == null){
            var div = document.querySelector('[action="/cart/add"]' );
            var newinput = document.createElement( 'input' );
            newinput.setAttribute('name','quantity');
            newinput.setAttribute('type','hidden');
            div.appendChild( newinput);
        }
        if(offer=="free"){

            values = parseInt(buy) + parseInt(get);
            document.querySelector('[name="quantity"]').setAttribute('value',values);
        }else{
            values = buy;
            document.querySelector('[name="quantity"]').setAttribute('value',values);
        }
    }

    @php echo $gs->advanced_js @endphp
</script>
<style>
    .crawlapps_css_swatch_li .swatch_gd_btn{
        font-family:{{$ds->style->font_family}} !important;
        font-size:{{$ds->style->font_size}}px !important;
        border-color:{{$ds->style->border_color}} !important;
        border-width:{{$ds->style->border_size}}px !important;
        background-color:{{$ds->style->bg_color}} !important;
        padding:{{$ds->style->padding}} !important;
        color:{{$ds->style->text_color}} !important;
        border-style:solid !important;
        border-radius:{{$ds->style->border_radius}}px !important;
    }

    .crawlapps_css_swatch_li .swatch_gd_btn:hover{
        border-color: {{$ds->style->border_color_on_hover}} !important;
    }

    .crawlapps_css_swatch_li .swatch_gd_btn:focus{
        outline: none;
        border-color: {{$ds->style->border_color_on_hover}} !important;
    }

    .crawlapps_css_swatch_li{
        display:{{$ds->style->display_type}} !important;
    }
    .dropdown_headline{
        color: {{ $ds->style->color_display_headline_text  }};
        font-size: {{ $ds->style->font_size_display_headline_text  }}px;
        margin-bottom: 5px;
    }
    @php echo $gs->advanced_css @endphp
</style>
<style>
    .crawlapps_body_venture .crawlapps_offers_swatch{
        width: 100%;
        padding: 0 5px;
    }
    .crawlapps_body_venture .crawlapps_offers_swatch ul{
        margin-bottom: 8px !important;
    }
    .crawlapps_body_debut .crawlapps_offers_swatch{
        width: 100%;
        padding: 0 5px;
        margin-bottom: 10px;
    }
    .crawlapps_body_debut .crawlapps_offers_swatch ul{
        margin-bottom: 10px;
    }
    .crawlapps_body_minimal .crawlapps_offers_swatch ul{
        margin-bottom: 17px;
    }
    .crawlapps_body_boundless .crawlapps_css_swatch_li{
        margin-bottom: 8px;
        margin-right: 5px;
    }
    .crawlapps_body_boundless .crawlapps_css_swatch_li:last-of-type{
        margin-bottom: 10px;
    }
    .crawlapps_body_minimal .crawlapps_css_swatch_li label{
        margin-bottom: 0;
    }
    .crawlapps_body_minimal .crawlapps_css_swatch_li:last-of-type{
        margin-bottom: 10px;
    }
    .crawlapps_body_narrative .crawlapps_css_swatch_li:last-of-type label{
        margin-bottom: 0;
    }
    .crawlapps_body_narrative .crawlapps_offers_dropdown ~ .btn{
        margin-top: 10px;
    }
    .crawlapps_body_venture ul{
        //display: flex;
        flex-wrap: wrap;
    }
    .crawlapps_body_venture ul li{
        margin-bottom: 5px;
        margin-right: 5px;
    }
    .crawlapps_body_brooklyn .crawlapps_css_swatch_li{
        margin-bottom: 5px;
    }
    .crawlapps_body_brooklyn .crawlapps_css_swatch_li:last-of-type{
        margin-bottom: 0;
    }
    .crawlapps_body_simple .crawlapps_css_swatch_ul .crawlapps_css_swatch_li label{
        margin-bottom: 5px;
    }
    .crawlapps_body_simple .crawlapps_css_swatch_ul{
        margin-bottom: -5px !important;
    }
    .crawlapps_body_responsive .crawlapps_offers_swatch{
        clear: both;
    }
    .crawlapps_body_booster .crawlapps_offers{
        width: 100%;
        float: left;
    }
    .crawlapps_body_booster .crawlapps_offers_swatch .crawlapps_css_swatch_li:not(:last-child){
        margin-right: 5px;
    }
    .crawlapps_body_booster .crawlapps_offers_swatch .crawlapps_css_swatch_li label {
        margin-bottom: 10px;
    }
    .crawlapps_body_parallax .purchase-details.smart-payment-button--false{
        flex-wrap: wrap;
    }
    .crawlapps_body_parallax .crawlapps_offers_swatch {
        width: 100%;
    }
    .crawlapps_body_showtime .crawlapps_offers_swatch{
        width: 100%;
        float: left;
    }
    .crawlapps_body_pipeline .crawlapps_offers_swatch .crawlapps_css_swatch_li{
        margin-bottom: 0.5rem;
        margin-right: 0.5rem;
    }
    .crawlapps_body_pipeline .crawlapps_offers_swatch .crawlapps_css_swatch_li label{
        margin: 0;
    }
    .crawlapps_body_minimart .crawlapps_offers_swatch{
        margin-bottom: 10px;
    }
    .crawlapps_body_icon .crawlapps_offers_swatch .crawlapps_css_swatch_li .swatch_gd_btn{
        margin-bottom: 0;
    }
    .crawlapps_body_empire .crawlapps_offers_swatch .crawlapps_css_swatch_li:not(:last-child){
        margin-right: 5px;
        margin-bottom: 10px;
    }
    .crawlapps_body_ella .crawlapps_offers_swatch p{
        margin-bottom: 10px;
    }
    .crawlapps_body_ella .crawlapps_offers_swatch .crawlapps_css_swatch_li{
        margin-right: 5px;
        margin-bottom: 10px;
    }
    .crawlapps_body_ella .crawlapps_offers_swatch .crawlapps_css_swatch_li label{
        margin-bottom: 0;
    }
    .crawlapps_body_seiko .crawlapps_offers_swatch{
        text-align: right;
        margin-bottom: 10px;
    }
    .crawlapps_body_seiko .crawlapps_offers_swatch .crawlapps_css_swatch_li:not(:last-child) label{
        margin-bottom: 10px;
        margin-right: 5px;
    }
    .crawlapps_body_hustler .crawlapps_offers_swatch{
        margin-bottom: 5px;
    }
    .crawlapps_body_hustler .crawlapps_offers_swatch .crawlapps_css_swatch_ul .crawlapps_css_swatch_li label{
        margin-bottom: 10px;
        margin-right: 5px;
    }
    .crawlapps_body_district .crawlapps_offers_swatch .crawlapps_css_swatch_ul .crawlapps_css_swatch_li{
        margin-right: 5px;
        margin-bottom: 10px;
    }
    .crawlapps_body_shoptimized .crawlapps_offers ~ .product--option.addtocart--button{
        margin-top: 0;
    }
    .crawlapps_body_shoptimized .crawlapps_offers ~ .product--option.addtocart--button .header {
        margin-top: 0;
    }
    .crawlapps_body_retina .crawlapps_offers ~ .purchase {
        margin-top: 0;
    }
    .crawlapps_body_pipeline .crawlapps_offers_swatch ~ .add-to-cart__wrapper {
        margin-top: 1.0em;
    }
    .crawlapps_body_story .dropdown_headline{
        margin-top:0;
    }
    .crawlapps_body_story .crawlapps_css_swatch_li{
        margin-bottom: 7px;
        margin-right: 2px;
    }
    .crawlapps_body_colors .row.expanded{
        margin-bottom:0;
    }
    .crawlapps_body_colors .crawlapps_css_swatch_ul{
        margin-bottom: 5px !important;
    }
    .crawlapps_body_colors .crawlapps_css_swatch_li{
        margin-bottom: 10px;
        margin-right: 5px;
    }
    .crawlapps_body_editionsv960 .dropdown_headline, .crawlapps_body_editions .dropdown_headline{
        margin-top:0;
    }
    .crawlapps_body_editionsv960 .dropdown_headline, .crawlapps_body_editions .dropdown_headline{
        margin-top:0;
    }
    .crawlapps_body_editionsv960 .crawlapps_css_swatch_ul, .crawlapps_body_editions .crawlapps_css_swatch_ul{
        margin-bottom: 10px !important;
    }
    .crawlapps_body_editionsv960 .crawlapps_css_swatch_li, .crawlapps_body_editions .crawlapps_css_swatch_li{
        margin-bottom: 10px;
        margin-right: 6px;
    }
    .crawlapps_body_district375 .crawlapps_offers_swatch, .crawlapps_body_district .crawlapps_offers_swatch{
        padding-left: 10px;
        flex: 0 0 100%;
    }
    .crawlapps_body_district375 .crawlapps_css_swatch_ul, .crawlapps_body_district .crawlapps_css_swatch_ul{
        margin-bottom: 10px !important;
    }
    .crawlapps_body_district375 .crawlapps_css_swatch_li, .crawlapps_body_district .crawlapps_css_swatch_li{
        margin-bottom: 10px;
        margin-right: 5px;
    }
    .crawlapps_body_launchv630 .dropdown_headline, .crawlapps_body_launch .dropdown_headline{
        padding:0;
    }
    .crawlapps_body_launchv630 .crawlapps_css_swatch_li, .crawlapps_body_launch .crawlapps_css_swatch_li{
        margin: 0;
        margin-bottom: 10px;
        margin-right: 5px;
    }
    .crawlapps_body_launchv630 .add-to-cart , .crawlapps_body_launch .add-to-cart{
        margin: 5px 0 0;
    }
    .crawlapps_body_california .crawlapps_offers_swatch{
        margin-top:10px;
    }
    .crawlapps_body_california .crawlapps_css_swatch_li label:after{
        display: none;
    }
    .crawlapps_body_california .crawlapps_css_swatch_li{
        margin-bottom: 5px;
    }
    .crawlapps_body_mobilia .crawlapps_css_swatch_ul{
        margin-bottom: 10px !important;
    }
    .crawlapps_body_mobilia .crawlapps_css_swatch_li{
        padding-bottom: 0;
        margin-bottom: 10px;
        margin-right: 3px;
    }
    .crawlapps_body_startupv921 .crawlapps_css_swatch_li, .crawlapps_body_startup .crawlapps_css_swatch_li{
        margin: 0;
        margin-right: 3px;
        margin-bottom: 10px;
    }
    .crawlapps_body_startupv921 .product-title-centered .product-options ~ .product-quantity, .crawlapps_body_startup .product-title-centered .product-options ~ .product-quantity{
        margin-top: 1em;
    }
    .crawlapps_body_reachv440 .crawlapps_offers_swatch, .crawlapps_body_reach .crawlapps_offers_swatch{
        margin-top: 1.5rem;
    }
    .crawlapps_body_reachv440 .dropdown_headline, .crawlapps_body_reach .dropdown_headline{
        margin-top:0;
    }
    .crawlapps_body_reachv440 .crawlapps_css_swatch_li, .crawlapps_body_reach .crawlapps_css_swatch_li{
        margin-bottom: 0.5rem;
        margin-right: 0.2rem;
    }
    .crawlapps_body_reachv440 .product-form-atc, .crawlapps_body_reach .product-form-atc{
        margin-top:1rem;
    }
    .crawlapps_body_empirev511 .product-form--atc, .crawlapps_body_empire .product-form--atc{
        margin-top: 0;
    }
    .crawlapps_body_empirev511 .crawlapps_css_swatch_li, .crawlapps_body_empire .crawlapps_css_swatch_li{
        margin-bottom: 1rem;
        margin-right: 0.5rem;
    }
    .crawlapps_body_atlanticv1421 .dropdown_headline, .crawlapps_body_atlantic .dropdown_headline{
        margin-top: 0;
    }
    .crawlapps_body_atlanticv1421 .crawlapps_css_swatch_li, .crawlapps_body_atlantic .crawlapps_css_swatch_li{
        margin-bottom: 5px;
    }
    .crawlapps_body_loft-14101 .crawlapps_offers_swatch, .crawlapps_body_loft .crawlapps_offers_swatch{
        margin:15px 0;
    }
    .crawlapps_body_gridv460 .crawlapps_css_swatch_li label, .crawlapps_body_grid .crawlapps_css_swatch_li label{
        margin:0;
        margin-bottom:5px;
    }
    .crawlapps_body_kingdom330 .crawlapps_css_swatch_ul, .crawlapps_body_kingdom .crawlapps_css_swatch_ul{
        margin-bottom: 10px !important;
    }
    .crawlapps_body_kingdom330 .crawlapps_css_swatch_li, .crawlapps_body_kingdom .crawlapps_css_swatch_li{
        margin-bottom:5px;
    }
    .crawlapps_body_retina .purchase-details{
        flex-wrap: wrap;
    }
    .crawlapps_body_retina .crawlapps_css_swatch_li{
        margin-right: 5px;
    }
    .crawlapps_body_retina .purchase-details__buttons{
        margin-top:0;
    }
    .crawlapps_body_parallax .purchase-details{
        flex-wrap: wrap;
    }
    .crawlapps_body_parallax .crawlapps_css_swatch_li{
        margin-right: 5px;
    }
    .crawlapps_body_parallax .purchase-details__buttons{
        margin-top:0;
    }
    .crawlapps_body_turbo .crawlapps_offers_swatch{
        margin-top:15px;
    }
    .crawlapps_body_turbo .crawlapps_css_swatch_li{
        margin-right:6px;
    }
    .crawlapps_body_turbo .crawlapps_css_swatch_li label{
        margin-bottom:0;
    }
    .crawlapps_body_superstorev232 .crawlapps_css_swatch_li, .crawlapps_body_superstore .crawlapps_css_swatch_li{
        margin-bottom:5px;
    }
    .crawlapps_body_pacificv430 .shopify-product-form, .crawlapps_body_pacific .shopify-product-form{
        text-align:center;
    }
    .crawlapps_body_pacificv430 .crawlapps_offers_swatch, .crawlapps_body_pacific .crawlapps_offers_swatch{
        display: inline-block;
    }
    .crawlapps_body_pacificv430 .dropdown_headline, .crawlapps_body_pacific .dropdown_headline{
        text-align:left;
    }
    .crawlapps_body_pacificv430 .crawlapps_css_swatch_li, .crawlapps_body_pacific .crawlapps_css_swatch_li{
        margin-bottom:5px;
    }
    .crawlapps_body_flow1 .crawlapps_css_swatch_ul, .crawlapps_body_flow .crawlapps_css_swatch_ul{
        margin-bottom: 10px !important;
    }
    .crawlapps_body_flow1 .crawlapps_css_swatch_li, .crawlapps_body_flow .crawlapps_css_swatch_li{
        margin-bottom: 10px;
        margin-right: 4px;
    }
    .crawlapps_body_motionv502 .crawlapps_css_swatch_ul, .crawlapps_body_motion .crawlapps_css_swatch_ul{
        margin-bottom: 10px !important;
    }
    .crawlapps_body_motionv502 .crawlapps_css_swatch_li, .crawlapps_body_motion .crawlapps_css_swatch_li{
        margin-bottom: 10px;
        margin-right: 4px;
    }
    .crawlapps_body_motionv502 .crawlapps_css_swatch_li label, .crawlapps_body_motion .crawlapps_css_swatch_li label{
        margin-bottom:0;
    }
    .crawlapps_body_voguev450 .crawlapps_css_swatch_li button, .crawlapps_body_vogue .crawlapps_css_swatch_li button{
        background-color: transparent;
        padding: 15px 24px;
        line-height: 1;
    }
    .crawlapps_body_voguev450 .crawlapps_css_swatch_li, .crawlapps_body_vogue .crawlapps_css_swatch_li{
        margin-bottom: 10px;
        margin-right: 4px;
    }
    .crawlapps_body_envy .crawlapps_offers_swatch{
        float: left;
        width: 100%;
        padding-left: 15px;
        padding-right: 15px;
    }
    .crawlapps_body_envy .crawlapps_css_swatch_li{
        margin-bottom: 10px;
        margin-right: 4px;
    }
    .crawlapps_body_envy .crawlapps_css_swatch_li label{
        margin-bottom:0;
    }
    .crawlapps_body_adeline-offical .page-product .product-info .product-action .action-button{
        clear: both;
    }
    .crawlapps_body_adeline-offical .crawlapps_css_swatch_ul{
        margin-bottom: 10px !important;
    }
    .crawlapps_body_adeline-offical .crawlapps_css_swatch_li{
        margin-bottom: 10px;
        margin-right: 4px;
    }
    .crawlapps_body_adeline-offical .crawlapps_css_swatch_li label{
        margin-bottom:0;
    }
    .crawlapps_body_testament .crawlapps_css_swatch_li{
        margin-bottom: 10px;
        margin-right: 4px;
    }
    .crawlapps_body_testament .crawlapps_css_swatch_li label{
        margin-bottom:0;
    }
    .crawlapps_body_testament .crawlapps_css_swatch_li .swatch_gd_btn{
        margin-bottom:0;
    }
    .crawlapps_body_basel-premium-theme .crawlapps_css_swatch_ul{
        margin-bottom: 10px !important;
    }
    .crawlapps_body_basel-premium-theme .crawlapps_css_swatch_li{
        margin-bottom: 5px;
        margin-right: 4px;
    }
    .crawlapps_body_foodlythemev18 .crawlapps_css_swatch_ul, .crawlapps_body_foodly .crawlapps_css_swatch_ul{
        margin-bottom: 10px !important;
    }
    .crawlapps_body_foodlythemev18 .crawlapps_css_swatch_li, .crawlapps_body_foodly .crawlapps_css_swatch_li{
        margin-bottom: 10px;
        margin-right: 4px;
    }
    .crawlapps_body_sunrise-theme .crawlapps_css_swatch_ul{
        margin-bottom: 10px !important;
    }
    .crawlapps_body_sunrise-theme .crawlapps_css_swatch_li{
        margin-bottom: 10px;
        margin-right: 4px;
    }
    .crawlapps_body_sunrise-theme .crawlapps_css_swatch_li .swatch_gd_btn{
        background-image: none;
        box-shadow: none;
    }
    .crawlapps_body_symetry-301 .crawlapps_css_swatch_li, .crawlapps_body_symetry .crawlapps_css_swatch_li{
        margin-bottom: 10px;
        margin-right: 4px;
        list-style: none;
    }
    .crawlapps_body_triss-shopify-theme .crawlapps_css_swatch_li{
        margin-bottom: 10px;
        margin-right: 4px;
    }
    .crawlapps_body_triss-shopify-theme .crawlapps_css_swatch_li .swatch_gd_btn{
        margin-bottom: 0;
    }
    .crawlapps_body_kagami .crawlapps_css_swatch_ul{
        margin-bottom: 10px !important;
    }
    .crawlapps_body_kagami .crawlapps_css_swatch_li{
        margin-bottom: 10px;
        margin-right: 4px;
    }
    .crawlapps_body_artisan .crawlapps_css_swatch_ul{
        margin-bottom: 10px !important;
    }
    .crawlapps_body_artisan .crawlapps_css_swatch_li{
        margin-bottom: 10px;
        margin-right: 4px;
    }
    .crawlapps_body_streamlinev302 .crawlapps_css_swatch_ul, .crawlapps_body_streamline.crawlapps_css_swatch_ul{
        margin-bottom: 10px !important;
    }
    .crawlapps_body_streamlinev302 .crawlapps_css_swatch_li, .crawlapps_body_streamline .crawlapps_css_swatch_li{
        margin-bottom: 10px;
        margin-right: 4px;
    }
    .crawlapps_body_streamlinev302 .crawlapps_css_swatch_li label, .crawlapps_body_streamline .crawlapps_css_swatch_li label{
        margin-bottom: 0;
    }
    .crawlapps_body_flex .crawlapps_css_swatch_li{
        margin-bottom: 10px;
        margin-right: 4px;
    }
    .crawlapps_body_flex .swatch_gd_btn.read_offers_data {
        cursor:pointer;
    }
    .crawlapps_body_impulsev302 .crawlapps_css_swatch_ul, .crawlapps_body_impulse .crawlapps_css_swatch_ul{
        margin-bottom: 10px !important;
    }
    .crawlapps_body_impulsev302 .crawlapps_css_swatch_li, .crawlapps_body_impulse .crawlapps_css_swatch_li{
        margin-bottom: 10px;
        margin-right: 4px;
    }
    .crawlapps_body_impulsev302 .crawlapps_css_swatch_li label, .crawlapps_body_impulse .crawlapps_css_swatch_li label{
        margin-bottom: 0;
    }
    .crawlapps_body_responsive .crawlapps_css_swatch_ul{
        margin-bottom: 10px !important;
    }
    .crawlapps_body_responsive .crawlapps_css_swatch_li{
        margin-bottom: 10px;
        margin-right: 4px;
        padding:0;
    }
    .crawlapps_body_responsive .crawlapps_css_swatch_li label{
        margin-bottom: 0;
    }
    .crawlapps_body_broadcast160crossborder .crawlapps_css_swatch_li, .crawlapps_body_broadcast .crawlapps_css_swatch_li{
        margin-bottom: 10px;
        margin-right: 4px;
        padding:0;
    }
    .crawlapps_body_split230 .crawlapps_css_swatch_li, .crawlapps_body_split .crawlapps_css_swatch_li{
        border: 0 !important;
        padding: 0 !important;
        width: auto !important;
        min-width: auto !important;
        height: auto !important;
        margin: 0 !important;
        margin-bottom: 10px !important;
        margin-right: 4px !important;
    }
    .crawlapps_body_split230 .crawlapps_css_swatch_li label:after, .crawlapps_body_split .crawlapps_css_swatch_li label:after{
        display: none !important;
    }

    .crawlapps_body_split230 .crawlapps_css_swatch_li button, .crawlapps_body_split .crawlapps_css_swatch_li button{
        border-style: solid !important;
        border-width: 1px;
        padding: 7px;
        font-weight: 300;
    }

    .crawlapps_body_masonry .crawlapps_css_swatch_li{
        list-style: none !important;
    }

    .crawlapps_body_prestige li {
        margin: 2px 0;
    }

    .crawlapps_body_express li{
        margin: 2px;
    }
    .crawlapps_body_showtime .crawlapps_offers_swatch{
        padding: 0 25px 15px 35px;
        float: none;
    }
    @media (max-width: 989px) {
        .crawlapps_body_debut .crawlapps_offers_swatch{
            padding: 0;
            width: 100%;
        }
        .crawlapps_body_debut .product-form__controls-group ~ .product-form__controls-group--submit{
            margin-top: 0;
        }
    }
    @media (max-width: 580px) {
        .crawlapps_body_showtime .crawlapps_offers_swatch {
            text-align: center;
        }
    }
    @media (max-width: 740px) {
        .crawlapps_body_icon .crawlapps_offers_swatch{
            margin-top: 15px;
            text-align: center;
        }
    }
    @media (max-width: 991px){
        .crawlapps_body_envy .crawlapps_offers_swatch{
            padding-top: 20px;
        }
    }
</style>
