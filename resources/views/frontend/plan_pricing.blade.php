@extends('layouts.app')
@section("content")
<div class="oa-main">
            <div class="d-flex align-items-start mb-4">
                <h1 class="mb-0">Plan Pricing</h1>
            </div>
            <div class="oa-row">
                <div class="w-50">
                    <div class="oa-box mb-20">
                        <div class="oa-box-body" style="padding-top: 2rem;">
                            <h1 class="mb-1">Pricing</h1>
                            <p class="text-gray-700 mb-20">1. 10-day free trial</p>
                            <p class="text-gray-700 mb-20">2. $20 per month</p>
                            <p class="text-gray-700 mb-20">3. All charges are billed in USD. Recurring charges, including monthly or usage-based charges, are billed every 30 days.</p>
                        </div>
                    </div>
                </div>
                <div class="w-50">
                    <div class="d-flex align-items-center justify-content-center h-100">
                        <img src="/images/pricing.png" />
                    </div>
                </div>
            </div>
        </div>
        <div class="oa-footer">
            <div class="text-center">
                <p class="text-gray-700 mb-20">You can read more about setting up Rulesets in our <a href="/faq" style="color:#00A9A2;">FAQ</a> page, otherwise you can reach out to our support team directly <a href="#" onclick="$('.mylivechat_buttonround').click()" style="color:#00A9A2;">here</a>.</p>
                <need-help></need-help>
            </div>
        </div>
@endsection
