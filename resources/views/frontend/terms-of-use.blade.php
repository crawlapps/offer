@extends('layouts.app')
@section('content')
<div class="oa-main">
    <h2 class="mb-1">TERMS AND CONDITIONS</h2>
    <div>
        <p>
            <b>Getrocketapps.com and Rocket Apps Terms of Service</b> <br>
            Getrocketapps.com <b>(“Site”)</b> and Rocket <b>(“Apps”)</b> are owned & operated by Centafic AB, a company based in Sweden. Throughout the site, the terms <b>“Centafic” , “Company” “we”, “us” and “our”</b> as well as any and all affiliates thereof, (www.getrocketapps.com), and Rocket Apps, shall each individually and collectively be referred to as <b>“Centafic.”</b>. Additionally, throughout this site and App, the terms getrocketapps.com, Rocket Apps as well as all other technologies whatsoever, products, applications, information, tolls and/or services related thereto shall be referred to each as a <b>“Service”</b> and together as the <b>“Services”</b>.
        </p>
        <br>
        <p>
            We may amend this document from time to time, which contains the terms and conditions, Privacy Policy, to represent, for example, improvements to our privacy practices or other organizational, legal or regulatory purposes. When we make substantial revisions to this Privacy Policy, we will inform you of such changes by publishing the amended policy on this website, and by other means where appropriate. You are unconditionally committing to the new policy or policies as reported by continuing to use this website or related <b>“Services”</b> after these updates are released.
        </p>
        <br>
        <p>
            Through visiting our website and/or buying something from us including a paid subscription you engage in our <b>"Service"</b> and agree to be bound by the following terms and conditions and any other documents posted by Centafic referred to hereinafter as the <b>"Agreement"</b> and/or the <b>"Terms & Conditions"</b> and/or the <b>"Terms of Sale”</b> including those relevant terms and conditions and policies listed herein and/or available via hyperlink. These Terms and Conditions apply to all users of the Site, including without limitation users who are browsers, vendors, customers, merchants and/or content contributors and any of the mentioned affiliates, partners and collaborators.
        </p>
        <br>
        <p>
            <b>
                Please carefully read these Terms and Conditions before you view or use our website. You agree to be bound by these Terms and Conditions by accessing or using any part of the Site. If you do not subscribe to all terms and conditions of this Agreement, you may not use or use any of the Products or Services on the Site or our Apps. If these Terms and Conditions are considered an offer, acceptance of such Terms of Conditions is expressly limited.
            </b>
        </p>
        <br>
        <p>
            <b>Services offered by Centafic</b> <br>
            Centafic may from time to time add or remove any app or service. Following are details of some of the services offered by Centafic.

            <div class="pl-10" style="padding-left: 30px;">
                <br>
                    <b>1. Rocket Discount</b> <br> 
                Rocket Discount allows you to offer discounts to your customers based on the quantity they order. You can apply discounts in percentage, as a fixed amount, or give an X number of units for free according to your set conditions. The offers you create can be displayed on your product page(s) in three different ways, Dropdown view, Swatch view, and Grid view. You can choose to use the display type(s) that fits your store and needs the best. On top of that, all display types can be fully customized to be displayed as you want them on the product page(s).
            </div>
        </p>
        <br>
        <p>
            Centafic produces applications that are aimed at improving the e-commerce experience available to. Centafic online store applications are built to give stores additional value-added features. Centafic can also create custom apps for your website, supply webmaster services and many other e-commerce services. The list of applications we sell is constantly changing, you can access a list of our latest apps here on our website www.getrocketapps.com. The specifics of each aspect of the Services (including technical details, support and pricing information) are posted on our website, as we may amend them from time to time, as well as in any applicable statement of work we may issue to you, in respect of the Services. Where you engage us to custom develop Services, the details of our engagement will be governed by any additional documents and agreements as part of that engagement. We make appropriate attempts to clarify the functionality of each Service on our website to you, but we cannot promise that a Service will look or work exactly the way it does on a demo page. If you have any concerns about any of our Services terms or details please contact us at the contact information below.
        </p>
        <br>
        <p>
            <b>Terms of Sale</b> <br>
            These Services are provided on a 1-store system, meaning you will need to buy Services on a single store basis. Of instance, if you have 2 stores and want to use 1 of our apps for both stores, you need to buy 2 subscriptions to that app. Once you complete the purchase of the Service the transaction will be completed, and your payment method will be paid on that Online Store platform following your predefined billing cycle. When making a payment, you confirm that you are entitled to make the payment using the method that you used.
        </p>
        <br>
        <p>
            All Centafic payments do not include relevant sales, goods and services, harmonized taxes or any other fees that may be paid by regulatory authorities. These taxes and/or fees are applied to the total amount that you are required to pay and billed to your payment method. It will be your sole responsibility to pay those taxes and/or fees.
        </p>
        <br>
        <p>
            You agree to indemnify and hold harmless Centafic and its officers, directors, subsidiaries, affiliates, employees, representatives, agents, licensors and their respective successors and assigns, against any chargeback costs  which we are required to pay, as a result of a payment dispute with you or in relation to a payment you have made to us for the Services.
        </p>
        <br>
        <p>
            In the event that you are billed directly by us, in most cases, you will be billed in advance of your payment period, typically monthly. In some cases, such as for overages, you may be billed in the month after the overages incurred. You must make all payments, without any setoff or deduction for any reason, within fifteen (15) days of the date of your invoice in a manner determined by us in our sole discretion.
        </p>
        <br>
        <p>
            All payments are made payable in <b>USD</b>.
        </p>
        <br>
        <p>
            <b>Trial period before purchase</b> <br>
            For certain Services we can provide a trial period before being eligible to purchase or subscribe. The length and precise details of the trial period will be posted to our websites and during the installation process of the Services. You won't be charged for the Services until the trial period is up.
        </p>
        <br>
        <p>
            You accept that while you may have trial-based access to the Services, you will be bound by the terms and conditions of this Agreement at no price. The continued use of the Services after the trial period is your agreement to be paid for those Services and to the terms of this Agreement.
        </p>
        <br>
        <p>
            <b>Intellectual Property</b> <br>
            Centafic and its original content, features, functionality, and design elements are and will remain the exclusive property of Centafic AB and its licensors. Our intellectual property may not be used in connection with any product or platform without the prior written consent of Centafic.
        </p>
        <br>
        <p>
            Subject to your compliance with the terms and conditions of this Agreement, we hereby grant to you a limited, non-exclusive, non-assignable, non-sub licensable, revocable right to install and use the Services for the purpose of operating your online store. This right terminates upon termination of this Agreement or any other agreements previously provided to you by us, as may be applicable. For certainty, if you uninstall all our apps, your Agreement is terminated automatically. Any and all rights not expressly granted to you are reserved by us, and this Agreement does not confer to you a proprietary interest in any Services.
        </p>

        <br>
        <p>
            <b>Modifications and continued use of Centafic</b><br>
            We reserve the right to modify, update, remove or disable access to any Services, without notice to you, and we will not be liable to you if we exercise those rights. When you purchase a particular Service, you are purchasing the right to use that Service as of the time you purchase it. You are not paying for the right to any updates, upgrades or future versions of the Services, though we may make such updates, upgrades or future versions available to you, in our sole discretion. We do not warrant, represent or undertake to provide any updates and in no event shall we be liable to you for our failure to do so.
        </p>

        <br>
        <p>
            <b>Third Party Services and Content</b><br>
            We are not a party to any relationship between you and any third party, including, but not limited to, you and your Online Store platform or you and your customers (your “Customers”), and as such, we have no responsibility to you as regards to your relationships with these third parties. You acknowledge and agree that you have no recourse against us for any acts or omissions of third parties, and your interaction with third parties is entirely at your own risk.
        </p>

        <br>
        <p>
            Your use of the Services may rely on services and products which are offered by third parties (“Third Party Services”). We have no responsibility to you for anything third parties do (or fail to do) and we provide no warranties or guarantees about third parties or Third Party Services. Your use of Third Party Services may be subject to specific terms and conditions which are set by those third parties.
        </p>
        <br>
        <p>
            We may make third parties’ content and materials (“Third Party Content”) available to you through our websites, such as reviews. Our making available of such Third Party Content does not constitute an endorsement or recommendation, and we are not responsible for any reliance you may place on Third Party Content. We make no warranties or representations as to any Third Party Content and we shall have no liability for it. Any opinions or statements made by third parties are those of such third parties, and do not necessarily state or reflect our views.
        </p>
        <br>
        <p>
            You agree that we will have no liability to you with respect to the acts, omissions, errors, representations, warranties, breaches or negligence for any damages or expenses resulting in any manner from your interactions with any: a) third parties; b) Third Party Services; or c) Third Party Content, and we are not obliged to become involved in any disputes you may have with any third parties. If you have a dispute with any third parties, you release Centafic and its Others from any direct, indirect, incidental, special, consequential, exemplary or other damages whatsoever, including, without limitation, any direct, indirect, incidental, special, consequential, exemplary or other damages, arising out of or in any way related to such disputes and/or our Services.
        </p>
        <br>
        <p>
            <b>Disclaimer</b><br>
            The Services and the materials on our website are provided on an as-is, as-available, basis and without warranties of any kind, expressed or implied. By accessing and using the Services and the materials on our website, you acknowledge and agree that such access and use is entirely at your own risk. We make no representation or warranties regarding the use or the results of the Services or the materials on our website (whether provided directly by us or through third parties or our affiliates), including, without limitation, that the Services or materials on our website will be accurate, complete, correct, timely or suitable, that the Services and the materials on our website are of merchantable quality or fit for a particular purpose, that the Services and the materials on our website will be available or uninterrupted at all times or at any particular time, or that the Services and the materials on our website will be free from errors, viruses or other harmful components. To the fullest extent permissible pursuant to applicable law, we disclaim all warranties, express or implied, including, but not limited to, warranties and conditions regarding the use of the Services and the materials on our website, including all implied warranties or conditions of merchantability, fitness for a particular purpose or non-infringement, whether express or implied, or arising from a course of dealing, usage or trade practice. We are not responsible for what others do with any materials or information you choose to share using the Services.
        </p>
        <br>
        <p>
            <b>Limitation of Liability</b><br>
            We are, with the limitations set out below, liable towards you for damages caused by our negligence, regardless of what legal ground you use for such claim.
        </p>
        <br>
        <p>
            Unless set out in the Agreement, we are not liable for damage caused by modifications or changes to the Service made according to your instructions or performed by anyone other than us (including but not limited to changes made by you or on your behalf).
        </p>
        <br>
        <p>
            We are not, under any circumstances, liable for your loss of profit, revenue, savings, or goodwill, loss due to operational, power or network interruptions, loss of data, your potential liability towards a third party or indirect or consequential damages of any kind.
        </p>
        <br>
        <p>
            Our total and aggregate liability under these Terms is, for each calendar year and regardless of the number of damages, limited to the fees paid by you during the 12 months period prior to the time when the damage(s) occurred. If you use the Service under a trial or otherwise free subscription, our aggregate liability, regardless of the number of damages, is limited to EUR 100. Our liability for Third Party Applications will never exceed such amount as we are entitled to reclaim from the provider(s) of such Third-Party Application.
        </p>
        <br>
        <p>
            We are not liable for damages unless you notify us in writing thereof no later than 90 days after you noticed or should have noticed, the actual damage or loss, however no later than six (6) months from when the damage occurred.
        </p>

        <br>
        <p>
            For the avoidance of doubt, you acknowledge and agree that any and all agreements between you and any other party is made on your own risk and that we are not responsible for any of your loss or damage in relation to such agreements. Neither the templates provided by us, nor any Content, are intended as legal advice and we recommend third party supervision before using the documents for any purpose. We undertake no responsibility with concern to the legal outcome when using the Platform or Content.
        </p>
        <br>
        <p>
            <b>Indemnity</b><br>
            You shall indemnify us with respect to all direct liability, losses, damages, costs or expenses howsoever caused, arising out of, or in connection with any breach of these Terms.
        </p>
        <br>
        <p>
            <b>Cancellation & Refund Policy</b><br>
            Please refer to our Cancellation and Refund Policy for more information of this Section.
        </p>

        <br>
        <p>
            <b>Privacy Policy</b><br>
            We are committed to protecting your privacy, in accordance with the terms and conditions of the Centafic Privacy Policy. By accessing and continuing to use the Services, including without limitation by registering or creating an account or profile with Centafic, and by providing personally identifiable information or personal data to Centafic through the Services, you are acknowledging that you have read our Privacy Policy (which sets out how we process personal data, and our legal basis for processing personal data) and that you agree and consent to us processing your personal data to provide the Services to you in accordance with this Agreement and our Privacy Policy. Please ensure that you have reviewed and understand our Privacy Policy before purchasing or subscribing for any Services from us or providing personal data to us.
        </p>
        <br>
        <p>
            You may be required, when you use certain features of the Services, to create an account with us, including a username and password. If we determine the username is in use by someone else or it is otherwise offensive, we may refuse to allow you, in our sole discretion, to use the chosen username. In addition, you are responsible for maintaining the confidentiality of your password and you are responsible for all uses of your username, whether or not you authorize such uses. You agree to notify us immediately of any actual or suspected loss, theft or unauthorized use of your username and password, or your account. We are not responsible for verifying your identity or the identity of anyone who uses your account, and we are not liable for any loss or damage as a result of your failure to protect your password, or as a result of unauthorized use of your username and/or password. You agree that any registration information you provide will be true and accurate, and that you will update it, as is necessary to keep it current. We reserve the right to automatically log you out of your account after such a period of inactivity as we determine is reasonable, in the circumstances.
        </p>
        <br>
        <p>
            We care about the security of our users. While we work hard to protect the security of your Uploaded Content, personal data, and account, we cannot guarantee that unauthorized third parties will not be able to defeat our security measures. Please notify us immediately in the event of any actual or suspected breach or unauthorized access or use of your account.
        </p>
        <br>
        <p>
            <b>Links</b><br>
            We do not monitor or review the content of other parties’ websites and services which are linked to from this website, nor do we control the availability and content of such websites and services. Opinions expressed or material appearing on such websites are not necessarily shared or endorsed by us and we should not be regarded as the publisher of such opinions or material, nor are we responsible for the accuracy of such opinions or material.
        </p>
        <br>
        <p>
            Please be aware that we are not responsible for the privacy practices, or content of third party websites and services. We encourage our users to be aware when they leave our website, and to read the terms of service and privacy statements of the websites that they may link to or access. You should evaluate the security and trustworthiness of any other website connected to our website or accessed through our website yourself, before disclosing any personal data to it. Centafic will not accept any responsibility for any loss or damage in whatever manner, howsoever caused, resulting from your disclosure to third parties of personal data.
        </p>
        <br>
        <p>
            <b>Age</b><br>
            Any use of the Services which is contrary to the terms of this Agreement may result in the immediate termination of this Agreement and your use of the Services, by us.
        </p>
        <br>
        <p>
            The Services offered by Centafic are directed towards and designed for the use of persons above the age of majority in their respective province, state, or country. Persons under the age of majority are not permitted to use the Services on their own, and Centafic will not approve applications of, or establish, or maintain accounts or memberships for any persons below their respective region’s age of majority.
        </p>
        <br>
        <p>
            If you are younger than 18, you may use the Services under the supervision of a parent or legal guardian. Otherwise, you must be 18 or older to use the Services and in no circumstances shall people under the age of majority in your state, province, or country, use the Services. Use of the Services by anyone under 13 years of age is strictly prohibited.
        </p>
        <br>
        <p>
            <b> Events out of our control </b> <br>
            We will not be liable or responsible for any failure to perform, or delay in performance of, any of our obligations under a contract that is caused by events outside our reasonable control <b>(“Force Majeure Event”)</b>.
        </p>
        <br>
        <p>
            A Force Majeure Event includes any act, event, non-happening, omission or accident beyond our reasonable control and includes in particular (without limitation) the following:
        </p>
        <br>
        <p>

            <ul style="list-style: lower-alpha;padding-left: 40px;">
                <li>
                    Strikes, lock-outs or other industrial action.
                </li>
            <li>
                Civil commotion, riot, invasion, terrorist attack or threat of terrorist attack, war (whether declared or not) or threat or preparation for war.
            </li>
            <li>
                Fire, explosion, storm, floor, earthquake, subsidence, epidemic or other
                Natural disaster
            </li>
            <li>
                Impossibility of the use of railways, shipping, aircraft, motor transport or other means of public or private transport.
            </li>
            <li>
                Impossibility of the use of public or private telecommunications networks.
            </li>
            <li>
                The acts, decrees, legislation, regulations or restrictions of any government.
            </li>
            </ul>
        </p>
        <br>
        <p>
            Our performance under any contract is deemed to be suspended for the period that the Force Majeure Event continues, and we will have an extension of time for performance for the duration of that period. We will use our reasonable endeavours to bring the Force Majeure Event to a close or to find a solution by which our obligations under the contract may be performed despite the Force Majeure Event.
        </p>
        <br>
        <p>
            <b>Governing Law</b><br>
            Contracts for the purchase of Product(s) through Centafic will be governed by Swedish law. Any dispute arising from, or related to, such contracts shall be subject to the non-exclusive jurisdiction of the courts of Stockholm, Sweden.
        </p>
        <br>
        <p>
            <b>Contact Us</b><br>
            Our contact information can be found on our Contact Us link on our website.
        </p>
    </div>

    <div class="oa-footer">
        <div class="text-center">
            <need-help></need-help>
        </div>
    </div>

</div>
@endsection
