@extends('layouts.app')
@section('content')
<div class="oa-main">
    <h2 class="mb-1">Privacy Policy</h2>
    <div>
        <p>
            Centafic AB. and its affiliates (collectively referred to as <b>“Centafic”, “we”, “our” and “us”</b>) respect your privacy. We offer various applications and tools which users use to improve their ecommerce website experience.
        </p>
        <br>
        <p>
            This Privacy Policy describes the types of Personal Data we collect through products and services (“Services”) and via our online presence, which includes our main website at www.getrocketapps.com, (“Site”) and our Rocket Apps (“Apps”). The Apps offered by Centafic are listed at www.getrocketapps.com. This policy also explains how we use Personal Data, with whom we share it, your rights and preferences, and how about our privacy practices you can contact us. This policy does not extend to websites, products or services of third parties, even if they are connected to our Services or Sites, and you should carefully consider the privacy policies of those third parties.
        </p>
        <br>

        <p>
            By using the Centafic Services, you agree to the collection, usage, retention and disclosure, as defined in and subject to the limitations set out in this Privacy Policy, of your Personal Data for the processing.
        </p>
        <br>

        <p>
            <b>PERSONAL INFORMATION WE COLLECT</b><br>
            When you install an App from us, we are automatically able to access certain types of information from your Shopify account. We collect the following types of personal information from you and/or your customers once you have installed the App or when you use our Site:

        <ul style="list-style: disc;padding-left: 40px;">
            <li>
                Information about you and others who may access the App on behalf of your store, such as your name, address, email address, phone number, and billing information
            </li>

            <li>
                Information about individuals who visit your store, such as their IP address, web browser details, and time zone.
            </li>

            <li>
                Cookies as prescribed in our Cookie Policy
            </li>

            <li>
                Information about the browsing activity of individuals that visit the App, including web browser details.
            </li>

            <li>
                Any information you submit on the Centafic website
            </li>
        </ul>
        </p>

        <br>
        <p>
            <b>HOW DO WE USE YOUR PERSONAL INFORMATION?</b><br>
            We use the personal information we collect from you and your customers in order to provide the Service and to operate the App. Additionally, we use this personal information to:
        <ul style="list-style: disc;padding-left: 40px;">
            <li>
                Communicate with you;
            </li>
            <li>
                Optimize or improve the App;
            </li>
            <li>
                Provide you with information or advertising relating to our products or services.
            </li>
        </ul>
        </p>
        <br>

        <p>
            <b>AGE</b><br>
            We don't knowingly collect or request personal information from anyone below the majority age. Please do not attempt to register for or use the Services or App or give us any personal information about yourself if you are under the age of majority. When we know that we have obtained personal information from a child under the age of majority, the information will be removed as soon as possible.  Please send us an e-mail at support@getrocketapps.com if you think a child under the age of majority might have sent us personal data.
        </p>
        <br>

        <p>
            <b>OTHER INFORMATION</b><br>
            By submitting personal information to us on our apps or using our site, you agree that we may collect and use such personal information in accordance with our privacy policy or as permitted by law. If you do not agree with these terms, do not provide any personal information to us. If you refuse or withdraw consent, or do not provide us with any necessary personal information, we may not be able to provide you with products or service.
        </p>
        <br>
        <p>
            <b>REMOVAL FROM OUR DATABASE</b><br>
            To request removal from the database, please submit a support request and we will be happy to remove your information promptly. If you request that we modify or remove all of your personal information, we will promptly do so, to the extent possible. Once your personal information has been removed you will not be able to use our products or services unless you re-register.
        </p>
        <br>

        <p>
            <b>IP ADDRESSES</b><br>
            We use IP addresses to analyze trends, administer the site and the apps and gather broad demographic information for aggregated used. IP addresses are not linked to personally identifiable information.
        </p>
        <br>

        <p>
            <b>SHARING YOUR PERSONAL INFORMATION</b><br>
            We may share your Personal Information to comply with applicable laws and regulations, to respond to a subpoena, search warrant or other lawful request for information we receive, or to otherwise protect our rights. We may also share your personal information to enhance our user experience and improve the apps with our affiliates.
        </p>
        <br>

        <p>
            <b>SECURITY</b><br>
            This web site has security measures in place designed to protect against loss, misuse and/or alteration of the information that you provide. Furthermore, we have taken reasonable precautions to prevent any unauthorized access to the contents of this web server.
        </p>
        <br>

        <p>
            <b>CONSENT</b><br>
            In some situations Centafic AB can seek your permission to process your Personal Data. You can indicate your approval in a number of ways, including checking a box (or relevant action) to indicate your permission when providing us with your Personal Data through our Services or a form, or registering or creating an account with us, as may be provided by Centafic AB and permitted by law; Remember that for certain countries or regions-specific consent laws also apply, depending on the jurisdiction you are living in.
        </p>
        <br>

        <p>
            <b>CHANGING OF PERSONAL INFORMATION</b><br>
            You may edit your personal information at any time by clicking on the “Settings” tab when you are signed in to Centafic AB. All information may be changed including: address, email, phone and more. The new information will be updated in our system and used in the collection of information when visiting Centafic AB.
        </p>
        <br>

        <p>
            <b>AMENDMENTS</b><br>
            We may amend this policy at any time. If we are going to use personally identifiable information collected through our sites or the app in a manner materially different from that stated at the time of collection we will notify users via email and/or by posting a notice on our site for thirty (30) days prior to such use. This Privacy Policy may not be amended by you except in a writing that specifically refers to this Privacy Policy and is physically signed by both parties.
        </p>
        <br>

        <p>
            <b>FOR USERS IN EUROPE AND OTHER PARTS</b><br>
            Centafic AB treats personal information in accordance with Regulation (EU) 2016/679 of the European Parliament (GDPR) and supplementary national legislation on data privacy. The entity responsible for the collection and processing of Personal Data for residents of the EEA, the UK and Switzerland is Centafic AB. To exercise your rights, the Data Protection Officer may be contacted on the information provided at the end of this policy.
        </p>
        <br>

        <p>
            Your Rights<br>
            If you are a European resident, you have the right to access personal information we hold about you and to ask that your personal information be corrected, updated, or deleted. If you would like to exercise this right, please contact us through the contact information below.
        </p>
        <br>

        <p>
            Additionally, if you are a European resident we note that we are processing your information in order to fulfill contracts we might have with you (for example if you make an order through the App), or otherwise to pursue our legitimate business interests listed above. Additionally, please note that your information will be transferred outside of Europe, including to Canada and the United States.
        </p>
        <br>

        <p>
            Rights according to data protection regulations
        </p>
        <br>

        <p>
            <b>GDPR gives individuals a number of rights as concerns Centafic AB:</b><br>
        <ul style="list-style: disc;padding-left: 40px;">
            <li>
                Right to information about how your personal data is stored and processed.
            </li>
            <li>
                Right of access by the data subject.
            </li>
            <li>
                Right to correct personal data.
            </li>
            <li>
                Right to have your personal data erased
            </li>
        </ul>
        </p>
        <br>

        <p>
            For assistance with any of the above, please contact us on the information provided at the end of this Privacy Policy
        </p>
        <br>
        <p>
            If you are a resident of the EEA and believe we process your information in scope of the General Data Protection Regulation (GDPR), you may direct your questions or complaints to the Office of the Data Protection Commissioner. <b>If you are a resident of the UK and since the UK is no longer a Member State of the EU (BREXIT)</b>, you may direct your questions or concerns to the UK Information Commissioner’s Office.
        </p>
        <br>

        <p>
            <b>Mexican residents.</b> Mexican residents may exercise data protection rights to access, correction, deletion, opposition or revocation under applicable law. You may be provided with further information about the steps to exercise your privacy rights, including identity verification, timing, the way to get in touch with the organization responding to your request for further communications about your request, and how your request may be honored. If you are a Mexican resident and a Customer of <b>Centafic AB</b>, please direct your requests directly to support@getrocketdiscount.com
        </p>
        <br>

        <p>
            <b>United States -</b> California residents. The personal information we collect about California consumers as well as the rights of California consumers are in accordance with the California Consumer Privacy Act (CCPA).
        </p>
        <br>

        <p>
            As Per CCPA, "Personal Information" is described as anything that identifies, relates to, defines or is capable of being connected, directly or indirectly, with a particular California household user, or could reasonably be linked to. For more information about the Personal Information we have obtained over the last 12 months, including the source types, please see the section above on Personal Data and Consent and Personal Data Collection. We collect this information for the business and commercial purposes outlined in the section above on Personal Data Usage. We share that information with the third-party categories listed therein. Centafic does not sell information of its users.
        </p>
        <br>

        <p>
            <b>DATA RETENTION</b><br>
            When you place an order through the App, we will maintain your Order Information for our records unless and until you ask us to delete this information.
        </p>
        <br>

        <p>
            <b>CHANGES TO THIS PRIVACY POLICY</b><br>
            We may update this privacy policy from time to time in order to reflect, for example, changes to our practices or for other operational, legal or regulatory reasons. The constant use of Centafic's websites and/or services following any changes to this Privacy Policy will, in accordance with the amended Privacy Policy, indicate your consent to the collection, use and disclosure of your Personal Data.
        </p>
        <br>

        <p>
            <b>CONTACT US</b><br>
            For more information about our privacy practices, if you have questions, or if you would like to make a complaint, please contact us by e-mail at support@getrocketdiscount.com.
        </p>

    </div>
    <div class="oa-footer">
        <div class="text-center">
            <need-help></need-help>
        </div>
    </div>


</div>
@endsection
