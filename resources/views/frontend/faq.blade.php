@extends('layouts.app')
@section('content')

    <div class="oa-main faq_page">
        <h2 class="mb-1">FAQs</h2>
        <p class="mb-20">Can't find what you need in our Support Center? Click  <a href="#" onclick="$('.mylivechat_buttonround').click()" style="color:#00A9A2;">here</a> to get in touch with a friendly member of our support team. </p>
        <div class="vertical-tabs">
            <div class="tab-sidebar">
                <ul id="myTab">
                    <li><a href="#important-note" class="nav-link active">Important Note</a></li>
                    <li><a href="#installation" class="nav-link">Installation</a></li>
                    <li><a href="#dashboard" class="nav-link">Dashboard</a></li>
                    <li><a href="#rulesets" class="nav-link">Rulesets</a></li>
                    <li><a href="#display-settings" class="nav-link">Display Settings</a></li>
                </ul>
            </div>
            <div class="tab-contnt">
                <div class="tab-content">
                    <div id="important-note" class="tab-pane show">
                        <div class="panel-heading">
                            <a class="setup-items">For optimal experience. Keep your theme name the same as provided by your theme developer. That way, we can detect the exact theme you are using and install custom CSS for your specific theme.</a>
                        </div>
                    </div>
                    <div id="installation" class="tab-pane">
                        <div class="panel-heading">
                            <a class="setup-items">Get Started with Rocket Discount</a>
                            <div class="panel-collapse">
                                <div class="oa-box-body">
                                    <p class="mb-2 font-size-lg">Not sure how to get started with Rocket Discount? Here is a step-by-step guide to help you on your way:</p>
                                    <p class="mb-2 font-size-lg">1. First, we need to install Rocket Discount to your theme. For that, we have three types of installation methods.</p>
                                    <ul style="margin-left:16px;">
                                        <li>
                                            <b>Auto Install (Most Popular)</b>
                                            <br>
                                            => Allow us to install all relevant code to your store automatically. This method works for most stores, but for some, it might not install it properly. We recommend that you do this in a duplicated version of your current theme, that is not yet published.
                                        </li>
                                        <li>
                                            <b>Manual Install (Advanced Users Only)</b>
                                            <br>
                                            => Manually install the code needed for Rocket Discount to function. If you are comfortable with installing code on your own, go with this. We recommend that you do this in a duplicated version of your current theme, that is not yet published.
                                        </li>
                                        <li>
                                            <b>Expert Install (Hassle–Free)</b>
                                            <br>
                                            => Hire our team of experts to install the app for you and ensure that it works as expected in your store. (Comes with a $25 USD flat fee)
                                        </li>
                                    </ul>
                                    <br>
                                    <p class="mb-2 font-size-lg">2. Now that the app is installed in your store, it is time for you to create your first Ruleset.</p>
                                    <p class="mb-2 font-size-lg">The Ruleset section is where you create your custom offers for your product(s). You can create as many Rulsets as you want; however, you cannot apply more than one Ruleset to the same product. Under the Ruleset section, you can do the following: </p>
                                    <ul style="margin-left:16px;">
                                        <li>
                                            <b>Give the Ruleset a name for your internal reference</b>
                                        </li>
                                        <li>
                                            <b>Select the product(s) you want to display the specific offer you are creating</b>
                                        </li>
                                        <li>
                                            <b>Choose how you want your offers to be displayed on your product page(s). There are three different display types:</b>
                                        </li>

                                    </ul>
                                    <br>
                                    <table style="width:100%;" class="discount-type-table">
                                        <tr>
                                            <td>
                                                <center style="min-height: 240px;">
                                                    <b>Dropdown view</b>
                                                    <img src="/images/types/dropdown.svg" style="margin-top:8px;" />
                                                </center>
                                            </td>
                                            <td>
                                                <center style="min-height: 240px;">
                                                    <b>Swatch view</b>
                                                    <img src="/images/types/swatch.svg" style="margin-top:8px;" />
                                                </center>
                                            </td>
                                            <td>
                                                <center style="min-height: 240px;">
                                                    <b>Grid view</b>
                                                    <img src="/images/types/grid.svg" style="margin-top:8px;" />
                                                </center>
                                            </td>
                                        </tr>
                                    </table>
                                    <br>
                                    <p class="mb-2 font-size-lg">You can choose to use the display type(s) that best fits your store or any specific product(s), which means that you can display different display types on various products in the same store by creating several Rulesets.</p>
                                    <p class="mb-2 font-size-lg ml-3">The last step in creating your Ruleset is to set up the conditions for tiered pricing. You can apply discounts in percentage, as a fixed amount, or give an X number of units for free according to your set conditions.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel-heading">
                        <a class="setup-items">Manual Installation Guide</a>
                        <div class="panel-collapse" style="display: block;">
                            <div class="oa-box-body">
                                <div class="oa-third-head">
                                    <a class="third-setup-items third-setup-items-1">1. Create snippet in your theme</a>
                                    <div class="third-panel-collapse">
                                        <div class="oa-box-body">
                                            <p class="mb-2 font-size-lg">Create the snippent in your theme, the snippet file name should be: <b>Snippets/crawlapps-offers.liquid</b> </p>
                                            <pre v-pre style="background:#aaaaaa61;padding:5px;border-radius:2px;">
&lt;script id="crawlapps_userguide_shop_data" type="application/json"&gt;
{!!
'{
    "shop": {
        "domain": "&#123;&#123; shop.domain &#125;&#125;",
        "permanent_domain": "&#123;&#123; shop.permanent_domain &#125;&#125;",
        "url": "&#123;&#123; shop.url &#125;&#125;",
        "secure_url": "&#123;&#123; shop.secure_url &#125;&#125;",
        "money_format": &#123;&#123; shop.money_format | json &#125;&#125;,
        "currency": &#123;&#123; shop.currency | json &#125;&#125;
    },
    "customer": {
        "id": &#123;&#123; customer.id | json &#125;&#125;,
        "tags": &#123;&#123; customer.tags | json &#125;&#125;
    },
    "cart": &#123;&#123; cart | json &#125;&#125;,
    "template": "&#123;&#123; template | split: "." | first &#125;&#125;",
    "product": &#123;&#123; product | json &#125;&#125;,
    "collection": &#123;&#123; collection.products | json &#125;&#125;
}'
!!}
&lt;/script&gt; </pre>

                                            <img src="{{ asset('images/manual-guide/snippet.png') }}" alt="Snippet">
                                        </div>
                                        <div class="oa-box-body">
                                            <p class="mb-2 font-size-lg">Now, include that file in <b>theme.liquid</b> inside the &lt;head&gt; section</p>
                                            <img src="{{ asset('images/manual-guide/theme.png') }}" alt="theme.liquid">
                                        </div>
                                    </div>
                                </div>

                                <div class="oa-third-head">
                                    <a class="third-setup-items third-setup-items-2">2. Add the HTML in cart page to show discounted value.</a>
                                    <div class="third-panel-collapse">
                                        <div class="oa-box-body"> <p class="mb-2 font-size-lg">
                                                Replace this content <b><span v-pre>&#123;&#123; item.price | money &#125;&#125;</span></b> with the below content in <b>cart-template.liquid</b> page
                                            </p>
                                            <pre v-pre style="background:#aaaaaa61;padding:5px;border-radius:2px;">
&lt;span class='crawlapps-cart-item-price' data-key='&#123;&#123;item.key&#125;&#125;'&gt;
  &lt;span class='crawlapps-cart-item-price' data-key='&#123;&#123;item.key&#125;&#125;'&gt;
      &#123;&#123; item.price | money &#125;&#125;
  &lt;/span&gt;
&lt;/span&gt;
</pre>
                                            <p></p></div>
                                        <div class="oa-box-body">
                                            <p class="mb-2 font-size-lg">
                                                Replace this content <b><span v-pre>&#123;&#123; item.line_price | money &#125;&#125;</span></b> with the below content in <b>cart-template.liquid</b> page
                                            <pre v-pre style="background:#aaaaaa61;padding:5px;border-radius:2px;">
&lt;span class='crawlapps-cart-item-line-price' data-key='&#123;&#123;item.key&#125;&#125;'&gt;
    &#123;&#123; item.line_price | money &#125;&#125;
&lt;/span&gt;
</pre>
                                            </p>

                                        </div>
                                        <div class="oa-box-body">
                                            <p class="mb-2 font-size-lg">
                                                Replace this content <b><span v-pre>&#123;&#123; item.line_price | money &#125;&#125;</span></b> with the below content in <b>cart-template.liquid</b> page
                                            <pre v-pre style="background:#aaaaaa61;padding:5px;border-radius:2px;">
&lt;span class='crawlapps-cart-item-line-price' data-key='&#123;&#123;item.key&#125;&#125;'&gt;
    &#123;&#123; item.line_price | money &#125;&#125;
&lt;/span&gt;
</pre>
                                            </p>

                                        </div>

                                        <div class="oa-box-body">
                                            <p class="mb-2 font-size-lg">
                                                Replace this content <b><span v-pre>&#123;&#123; cart.total_price | money &#125;&#125;</span></b> with the below content in <b>cart-template.liquid</b> page
                                            <pre v-pre style="background:#aaaaaa61;padding:5px;border-radius:2px;">
&lt;span class='crawlapps-cart-original-total'&gt;
    &#123;&#123; cart.total_price | money &#125;&#125;
&lt;/span&gt;
</pre>
                                            </p>
                                        </div>


                                    </div>
                                </div>

                                <div class="oa-third-head">
                                    <a class="third-setup-items third-setup-items-3">3. Set the discounts shortcodes in your product template</a>
                                    <div class="third-panel-collapse">
                                        <div class="oa-box-body"> <p class="mb-2 font-size-lg">
                                                Set the below shortcode anywhere inside the <b>&lt;form method="post" action="/cart/add"&gt;</b> element in product page.
                                            </p>
                                            <pre v-pre style="background:#aaaaaa61;padding:5px;border-radius:2px;">
<div> Dropdown: <b> &lt;div class="crawlapps_offers crawlapps_offers_dropdown"&gt;&lt;/div&gt; </b> </div>
<div> Swatch:   <b> &lt;div class="crawlapps_offers crawlapps_offers_swatch"&gt;&lt;/div&gt; </b></div>
<div> Grid:     <b> &lt;div class="crawlapps_offers crawlapps_offers_gridview"&gt;&lt;/div&gt; </b>
</div>
<div> Example: <a href="https://prnt.sc/pso9pw" target="_blank">Open</a></div> </pre>
                                            <p></p></div>

                                    </div>

                                </div>
                            </div></div></div>

                    </div>
                    <div id="dashboard" class="tab-pane">
                        <div class="panel-heading">
                            <a class="setup-items">What is "Orders", " Revenue", "AOV ", "AUC", "Extra Amount"?</a>
                            <div class="panel-collapse">
                                <div class="oa-box-body">
                                    <p class="mb-2 font-size-lg"><b>Orders:</b> Displays the number of orders you have received with the selected Rulesets, over a specific period. 
                                    </p>
                                    <p class="mb-2 font-size-lg"><b>Revenue:</b> Displays the total revenue you have made from "Orders."
                                    </p>
                                    <p class="mb-2 font-size-lg"><b>AOV:</b> Displays the average order value, which is calculated by dividing "Orders" with "Revenue”.
                                    </p>
                                    <p>
                                        <img src="/images/manual-guide/faq.svg" style="margin-top:8px;" />
                                    </p>
                                    <p class="mb-2 font-size-lg"><b>AUC:</b> Displays the average unit count, which is calculated by dividing the number of units your orders includes with "Orders".
                                    </p>
                                    <p class="mb-2 font-size-lg">
                                        Example: &nbsp;You get 100 orders that includes 200 units. The AUC would be 200/100= 2
                                    </p>
                                    <p class="mb-2 font-size-lg"><b>Extra Amount:</b> Displays the extra amount you have earned with the Rulesets you have created. The amount is calculated by taking the minimum number of units you can order and subtract it from the cost of the units the customer ordered.
                                    </p>

                                    <p class="mb-2 font-size-lg">
                                        Example 1: <br>
                                            <p> 1 units cost $95 </p>
                                            <p> 2 units cost $185 </p>
                                            <p> 3 units cost $275 </p>
                                    </p>
                                    <br>
                                    <p class="mb-2 font-size-lg">
                                        Example 2: <br>
                                    <p> 2 units cost $190 </p>
                                    <p> 4 units cost $360 </p>
                                    <p> 6 units cost $510 </p>
                                    </p>
                                    <br>
                                    <p class="mb-2 font-size-lg">
                                        If the customer purchases 6 units for $510 then 510-190= $320 will be added to “Extra Amount”
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="rulesets" class="tab-pane">
                        <div class="panel-heading">
                            <a class="setup-items">What are the three different "Display Types"?</a>
                            <div class="panel-collapse">
                                <div class="oa-box-body">
                                    <p class="mb-2 font-size-lg">Choose how you want your offers to be displayed on your product page(s). There are three different display types:</p>
                                    <table style="width:100%;" class="discount-type-table">
                                        <tr>
                                            <td>
                                                <center style="min-height: 240px;">
                                                    <b>Dropdown view</b>
                                                    <img src="/images/types/dropdown.svg" style="margin-top:8px;" />
                                                </center>
                                            </td>
                                            <td>
                                                <center style="min-height: 240px;">
                                                    <b>Swatch view</b>
                                                    <img src="/images/types/swatch.svg" style="margin-top:8px;" />
                                                </center>
                                            </td>
                                            <td>
                                                <center style="min-height: 240px;">
                                                    <b>Grid view</b>
                                                    <img src="/images/types/grid.svg" style="margin-top:8px;" />
                                                </center>
                                            </td>
                                        </tr>
                                    </table>
                                    <br>
                                    <p class="mb-2 font-size-lg">You can choose to use the display type(s) that best fits your store or any specific product(s), which means that you can display different display types on various products in the same store by creating several Rulesets.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel-heading">
                            <a class="setup-items">How does "Tier Pricing" work?</a>
                            <div class="panel-collapse">
                                <div class="oa-box-body">
                                    <p class="mb-2 font-size-lg">Rocket Discount allows you to offer discounts to your customers based on the quantity they order. You can apply discounts in percentage, as a fixed amount, or give an X number of units for free according to your set conditions.</p>
                                    <p class="mb-2 font-size-lg">Let us look at an example together. If your product price is $100 per unit, and you set the following conditions:</p>
                                    <ul style="margin-left:40px;margin-top:10px;">
                                        <li><b>% Off: Buy 1 Get 5% Off</b>
                                            <br>
<pre>
If one unit is ordered, the customer would receive a 5% discount and pay $95,
excluding any extra charges you might charge your customer,
such as taxes, shipping, etc.
</pre>
                                        </li>
                                        <li><b>Fixed Price: Buy 2 Get $20 Off</b>
                                            <br>
<pre>
If two units are ordered, the customer would receive a $20
discount on the total price and pay $180,
excluding any extra charges you might charge your customer,
such as taxes, shipping, etc.
</pre>
                                        </li>
                                        <li><b>Free: Buy 3 Get 1 Free</b>
                                            <br>
<pre>
If three units are ordered, the customer will receive four units and pay $300,
excluding any extra charges you might charge your customer,
such as taxes, shipping, etc.
</pre>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="display-settings" class="tab-pane">
                        <div class="panel-heading">
                            <a class="setup-items">How can "Display Settings" be used?</a>
                            <div class="panel-collapse">
                                <div class="oa-box-body">
                                    <p class="mb-2 font-size-lg">You can customize the three display types to be shown just as you want them on your product page. We have made sure that you have the option to customize anything you want on the display types. We have also included a live display preview that allows you to see how your customizations will look on the product page.</p>

                                </div>
                            </div>
                        </div>

                        <div class="panel-heading">
                            <a class="setup-items">How is "Font Family" used? </a>
                            <div class="panel-collapse">
                                <div class="oa-box-body">
                                    <p class="mb-2 font-size-lg">
                                        Font family works based on the font(s) installed to your theme. That means if you want to use a particular font for the offers you create, you would need first to have it installed to your theme and then add the font name under "Font Family" to function
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="oa-footer">
        <div class="text-center">
            <img src="images/footer-img.png" class="footer-img">
        </div>
        <div class="text-center">
            <need-help></need-help>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){
            $('.third-setup-items-1').click(function(){
                $(this).parents('.oa-third-head').find('.third-panel-collapse').toggle();
            });
            $('.third-setup-items-2').click(function(){
                $(this).parents('.oa-third-head').find('.third-panel-collapse').toggle();
            });
            $('.third-setup-items-3').click(function(){
                $(this).parents('.oa-third-head').find('.third-panel-collapse').toggle();
            });
            $('#myTab li a').click(function(){
                var id = $(this).attr('href');
                $(this).addClass('active');
                $(this).parents('#myTab li').siblings().find('a').removeClass('active');
                $(id).siblings().removeClass('show');
                $(id).addClass("show");
            });
            $('.panel-heading .setup-items').click(function(){
                $(this).parents('.panel-heading').siblings().find('.panel-collapse').slideUp();
                $(this).parents('.panel-heading').find('.panel-collapse').slideToggle();
            });
        });
    </script>

@endsection
