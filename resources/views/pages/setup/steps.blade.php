@extends('layouts.app')
@section("content")
    <div class="oa-main">
            <div class="oa-row">
                <div class="w-60 mb-xs-20">
                    <div class="d-flex align-items-center justify-content-center h-100">
                        <img src="/images/svg/Welcome.svg" style="height: 100%;width: 100%;"/>
                    </div>
                </div>
                <div class="w-40">
                    <h2 class="mb-3 text-center">Let's get you started. Your next steps are :</h2>
                    <div class="oa-box mb-20" style="min-height: auto;">
                        <div class="oa-box-body text-center">
                            <div class="cricle mb-1">1</div>
                            <p class="mb-0">Setup Rocket Discount in your theme</p>
                            <div class="d-flex justify-content-end"><a href="#" v-on:click="setupPopup" class="text-gray-700"><i class="ion ion-ios-arrow-round-forward h1"></i></a></div>
                        </div>
                    </div>
                    <div class="oa-box mb-20" style="min-height: auto;">
                        <div class="oa-box-body text-center">
                            <div class="cricle mb-1">2</div>
                            <p class="mb-0">Create your first Rocket Discount Ruleset. </p>
                            <div class="d-flex justify-content-end"><a href="{{ route('offers.create') }}" class="text-gray-700"><i class="ion ion-ios-arrow-round-forward h1"></i></a></div>
                        </div>
                    </div>
                    <div class="oa-box mb-20" style="min-height: auto;">
                        <div class="oa-box-body text-center">
                            <div class="cricle mb-1">3</div>
                            <p class="mb-0">Visit the Display Settings to customize how your discount will be displayed for your customers.</p>
                            <div class="d-flex justify-content-end"><a href="{{ route('settings.index') }}"  class="text-gray-700"><i class="ion ion-ios-arrow-round-forward h1"></i></a></div>
                        </div>
                    </div>
                </div>
                <div class="w-100">
                    <div class="oa-box mb-20" style="min-height: auto;">
                        <div class="oa-box-header">
                            <div class="d-flex align-items-center justify-content-between">
                                <h2 class="text-primary">Important Notes :</h2>
                            </div>
                        </div>
                        <div class="oa-box-body">
                            <ul class="mb-20" style="padding-left: 15px;">
                                <li class="font-size-lg">
                                    For optimal experience. Keep your theme name the same as provided by your theme developer. That way, we can detect the exact theme you are using and install custom CSS for your specific theme.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <div class="oa-footer">
        <div class="text-center">
            <need-help></need-help>
        </div>
    </div>
<div id="app-popup" class="app-popup d-none" style="display:none" v-show="showPopup">
    <div class="overlay"></div>
    <div class="popup-container border-radius">
        <div class="popup-head popup-head-new">
            <img src="https://cdn.shopify.com/s/files/1/0055/4093/7792/files/icon.png?v=1584376349"/>
            <div class="popup-heading-text">
                <h3 class="main-head">Get Started with Rocket Discount</h3>
                <p>Not sure how to get started with Rocket Discount? Here is a step-by-step guide to help you on your way:</p>
                <p>First, we need to install Rocket Discount to your theme. For that, we have three types of installation methods.</p>
            </div>
            <div class="close position-absolute">
                <a href="#" class="close-icon" v-on:click="setupPopup"><i class="ion ion-ios-close"></i></a>
            </div>
        </div>
        <div class="popup-body">

                <div class="popup-row">

                    <div class="popup-row-item">
                        <div class="install-option install-option-new border-radius">
                            <div class="fancy-heading">
                                <h4>Expert Install</h4> <font style="color:#fff;"> (Advanced Users Only) </font>

                            </div>
                            <div class="logo">
                                <img src="/images/svg/expert_install.svg" />
                            </div>
                            <div class="content">
                                <div class="tooltip-width-text">
                                Hire our team of experts to install the app for you and ensure that it works as expected in your store. (Comes with a $25 USD flat fee).
                                </div>
                            </div>
                            <div class="first-step-btn">
                                <button class="btn btn-block" v-on:click="contact2Popup" >Contact</button>
                            </div>
                        </div>
                    </div>

                    <div class="popup-row-item">
                        <div class="install-option install-option-new border-radius">
                            <div class="fancy-heading">
                                <h4>Auto Install</h4> <font style="color:#fff;"> (Most Popular) </font>
                            </div>
                            <div class="logo">
                                <img src="/images/svg/auto_install.svg" />
                            </div>
                            <div class="content">
                                <div class="tooltip-width-text">
                                    Allow us to install all relevant code to your store automatically. This method works for most stores, but for some, it might not install it properly. We recommend that you do this in a duplicated version of your current theme, that is not yet published.
                                </div>
                            </div>
                            <div class="first-step-btn">
                                <button v-on:click="show2Popup" class="btn btn-block">Start Now</button>
                            </div>
                        </div>
                    </div>

                    <div class="popup-row-item">
                        <div class="install-option install-option-new">
                            <div class="fancy-heading">
                                <h4>Manual Install</h4> <font style="color:#fff;"> (Hassle–Free) </font>

                            </div>
                            <div class="logo">
                                <img src="/images/svg/manual.svg" />
                            </div>
                            <div class="content">

                                <div class="tooltip-width-text">
                                    Manually install the code needed for Rocket Discount to function. If you are comfortable with installing code on your own, go with this. We recommend that you do this in a duplicated version of your current theme, that is not yet published.
                                </div>
                            </div>
                            <div class="first-step-btn">
                                <a href="{{route('faq') }}" class="btn btn-block">Installation Guide</a>
                            </div>
                        </div>
                    </div>

                </div>
        </div>
    </div>
</div>

<div class="automatic_install_section d-none" id="app-popup-install" style="display:none" v-show="showInstallPopup">
<div class="overlay"></div>
    <div class="automatic_install_popup">
    <div class="close position-absolute">
        <a href="#" id="close" v-on:click="show2Popup" class="cancel font-weight-bold d-block text-center text-body">×</a>
    </div>
    <div class="install_section-text">

        <setup-index-component
            props-url = "{{
                json_encode([
                        "index" => route("setup.index"),
                        "store" => route("setup.store"),
                ],JSON_HEX_APOS)
                }}"
            props-trans = "{{ json_encode([],JSON_HEX_APOS)}}"
            props-data = "{{ json_encode([
            'theme'=> $data,
            'main_theme'=> $main_theme,
            'is_new_theme'=> $is_new_theme,
            'cssthemeOption'=> collect(config('css_theme'))->sort()->all(),
             'css_theme'=> \ShopifyApp::shop()->css_theme
            ],JSON_HEX_APOS)}}"
        ></setup-index-component>
    </div>
    </div>
</div>

<div class="automatic_install_section d-none" id="contact-app-popup-install" style="display:none" v-show="contactInstallPopup">
    <div class="overlay"></div>
    <div class="automatic_install_popup">
        <div class="close position-absolute">
            <a href="#" id="close" v-on:click="contact2Popup" class="cancel font-weight-bold d-block text-center text-body">×</a>
        </div>
        <div class="install_section-text">
            <contact-index-component
                props-url = "{{
            json_encode([
                    "index" => route("setup.index"),
                    "store" => route("setup.store"),
            ],JSON_HEX_APOS)
            }}"
                props-trans = "{{ json_encode([],JSON_HEX_APOS)}}"
                props-data = "{{ json_encode([
        'theme'=> $data,
        'main_theme'=> $main_theme,
        'is_new_theme'=> $is_new_theme,
        ],JSON_HEX_APOS)}}"
            ></contact-index-component>
        </div>
    </div>
</div>

@endsection
