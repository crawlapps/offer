@extends('layouts.app')
@section("content")
    <uninstall-component
        props-url = "{{
                            json_encode([
                                    "index" => route("setup.index"),
                                    "uninstall" => route("setup.uninstall"),
                                    "store" => route("setup.store"),
                            ],JSON_HEX_APOS)
                            }}"
        props-trans = "{{ json_encode([],JSON_HEX_APOS)}}"
        props-data = "{{ json_encode([
            'theme'=> $data,
            'main_theme' => $main_theme,
            'is_new_theme' => $is_new_theme

            ],JSON_HEX_APOS)}}"
    ></uninstall-component>
@endsection
