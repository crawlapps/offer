@extends('layouts.app')
@section("content")
    <div class="oa-main">
        <h2 class="mb-1">Manual Installation Guide</h2>
        <p class="mb-20"> </p>
        <div class="vertical-tabs">
            <div class="tab-sidebar">
                <ul id="myTab">
                    <li><a href="#general" class="nav-link active">Snippet</a></li>
                    <li><a href="#ajax-cart" class="nav-link">Cart Page</a></li>
                    <li><a href="#discount-application" class="nav-link">Product Page</a></li>
                </ul>
            </div>
            <div class="tab-contnt">
                <div class="tab-content">
                    <div id="general" class="tab-pane show">
                        <div class="panel-heading">
                            <a class="setup-items">Create snippet in your theme</a>
                            <div class="panel-collapse">
                                <div class="oa-box-body">
                                    <p class="mb-2 font-size-lg">Create the snippent in your theme, the snippet file name should be: <b>Snippets/crawlapps-offers.liquid</b> </p>
                                    <pre v-pre style="background:#aaaaaa61;padding:5px;border-radius:2px;">
&lt;script id="crawlapps_userguide_shop_data" type="application/json"&gt;
{!!
'{
    "shop": {
        "domain": "&#123;&#123; shop.domain &#125;&#125;",
        "permanent_domain": "&#123;&#123; shop.permanent_domain &#125;&#125;",
        "url": "&#123;&#123; shop.url &#125;&#125;",
        "secure_url": "&#123;&#123; shop.secure_url &#125;&#125;",
        "money_format": &#123;&#123; shop.money_format | json &#125;&#125;,
        "currency": &#123;&#123; shop.currency | json &#125;&#125;
    },
    "customer": {
        "id": &#123;&#123; customer.id | json &#125;&#125;,
        "tags": &#123;&#123; customer.tags | json &#125;&#125;
    },
    "cart": &#123;&#123; cart | json &#125;&#125;,
    "template": "&#123;&#123; template | split: "." | first &#125;&#125;",
    "product": &#123;&#123; product | json &#125;&#125;,
    "collection": &#123;&#123; collection.products | json &#125;&#125;
}'
!!}
&lt;/script&gt; </pre>

                                    <img src="{{ asset('images/manual-guide/snippet.png') }}" alt="Snippet">
                                </div>

                                <div class="oa-box-body">
                                    <p class="mb-2 font-size-lg">Now, include that file in <b>theme.liquid</b> inside the &lt;head&gt; section</p>
                                    <img src="{{ asset('images/manual-guide/theme.png') }}" alt="theme.liquid">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="ajax-cart" class="tab-pane">
                        <div class="panel-heading">
                            <a class="setup-items">Add the HTML in cart page to show discounted value.</a>
                            <div class="panel-collapse">
                                <div class="oa-box-body">
                                    <p class="mb-2 font-size-lg">
                                        Replace this content <b><span v-pre>&#123;&#123; item.price | money &#125;&#125;</span></b> with the below content in <b>cart-template.liquid</b> page
<pre v-pre style="background:#aaaaaa61;padding:5px;border-radius:2px;">
&lt;span class='crawlapps-cart-item-price' data-key='&#123;&#123;item.key&#125;&#125;'&gt;
  &lt;span class='crawlapps-cart-item-price' data-key='&#123;&#123;item.key&#125;&#125;'&gt;
      &#123;&#123; item.price | money &#125;&#125;
  &lt;/span&gt;
&lt;/span&gt;
</pre>
                                    </p>
                                </div>

                                <div class="oa-box-body">
                                    <p class="mb-2 font-size-lg">
                                        Replace this content <b><span v-pre>&#123;&#123; item.original_line_price | money &#125;&#125;</span></b> with the below content in <b>cart-template.liquid</b> page
                                    <pre v-pre style="background:#aaaaaa61;padding:5px;border-radius:2px;">
&lt;span class='crawlapps-cart-item-line-price' data-key='&#123;&#123;item.key&#125;&#125;'&gt;
    &#123;&#123; item.original_line_price | money &#125;&#125;
&lt;/span&gt;
</pre>
                                    </p>

                                </div>

                                <div class="oa-box-body">
                                    <p class="mb-2 font-size-lg">
                                        Replace this content <b><span v-pre>&#123;&#123; item.line_price | money &#125;&#125;</span></b> with the below content in <b>cart-template.liquid</b> page
                                    <pre v-pre style="background:#aaaaaa61;padding:5px;border-radius:2px;">
&lt;span class='crawlapps-cart-item-line-price' data-key='&#123;&#123;item.key&#125;&#125;'&gt;
    &#123;&#123; item.line_price | money &#125;&#125;
&lt;/span&gt;
</pre>
                                    </p>

                                </div>

                                <div class="oa-box-body">
                                    <p class="mb-2 font-size-lg">
                                        Replace this content <b><span v-pre>&#123;&#123; cart.total_price | money &#125;&#125;</span></b> with the below content in <b>cart-template.liquid</b> page
                                    <pre v-pre style="background:#aaaaaa61;padding:5px;border-radius:2px;">
&lt;span class='crawlapps-cart-original-total'&gt;
    &#123;&#123; cart.total_price | money &#125;&#125;
&lt;/span&gt;
</pre>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="discount-application" class="tab-pane">
                        <div class="panel-heading">
                            <a class="setup-items">Set the discounts shortcodes in your product template.</a>
                            <div class="panel-collapse">
                                <div class="oa-box-body">
                                    <p class="mb-2 font-size-lg">
                                        Set the below shortcode anywhere inside the <b>&lt;form method="post" action="/cart/add"&gt;</b> element in product page.
                                    <pre v-pre style="background:#aaaaaa61;padding:5px;border-radius:2px;">
<div> Dropdown: <b> &lt;div class="crawlapps_offers crawlapps_offers_dropdown"&gt;&lt;/div&gt; </b> </div>
<div> Swatch:   <b> &lt;div class="crawlapps_offers crawlapps_offers_swatch"&gt;&lt;/div&gt; </b></div>
<div> Grid:     <b> &lt;div class="crawlapps_offers crawlapps_offers_gridview"&gt;&lt;/div&gt; </b>
</div>
<div> Example: <a href="https://prnt.sc/pso9pw" target="_blank">Open</a></div> </pre></p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="oa-footer">
        <div class="text-center">
            <need-help></need-help>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){
            $('#myTab li a').click(function(){
                var id = $(this).attr('href');
                $(this).addClass('active');
                $(this).parents('#myTab li').siblings().find('a').removeClass('active');
                $(id).siblings().removeClass('show');
                $(id).addClass("show");
            });
            $('.panel-heading .setup-items').click(function(){
                $(this).parents('.panel-heading').siblings().find('.panel-collapse').slideUp();
                $(this).parents('.panel-heading').find('.panel-collapse').slideToggle();
            });
        });
    </script>
@endsection
