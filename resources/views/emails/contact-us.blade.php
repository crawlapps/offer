@extends('emails.layout.master')
@section('email-content')
    <tr>
        <td align='center'>
            <center>
                <table border='0' cellpadding='30' cellspacing='0'
                       style='margin-left: auto;margin-right: auto;width:600px;text-align:center;' width='600'>
                    <tr>
                        <td align='left' style='background: #ffffff; border: 1px solid #dce1e5;' valign='top' width=''>
                            <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                <tr>
                                    <td align='center' valign='top'>
                                        <h2>
                                            Hello There,
                                        </h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td align='center' valign='top'>
                                        <p style='margin: 1em 0;'>
                                            This email is just to let you know we have received your support request. Your request has been assigned the following ID: {{ $data['assigned_id'] }}.
<br>
                                            A support rep has been assigned and will contact you shortly. During normal business hours most requests receive a reply within 24 hours. In the meantime, for more immediate help please check out our support center.
<br>
                                            To add additional comments or to provide further information that may assist our team in responding to your request, reply to this email.
                                        </p>

                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" valign='top'>
                                        <p style='margin: 1em 0;'>
                                            Thank you, <br>
                                            Rocket Discount.
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
@stop
