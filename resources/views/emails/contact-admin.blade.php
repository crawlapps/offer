@extends('emails.layout.master')
@section('email-content')
    <tr>
        <td align='center'>
            <center>
                <table border='0' cellpadding='30' cellspacing='0'
                       style='margin-left: auto;margin-right: auto;width:600px;text-align:center;' width='600'>
                    <tr>
                        <td align='left' style='background: #ffffff; border: 1px solid #dce1e5;' valign='top' width=''>
                            <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                <tr>
                                    <td align='center' valign='top'>
                                        <h2>
                                            Hello Admin/Support,
                                        </h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td align='center' valign='top'>
                                        <p style='margin: 1em 0;'>
                                            Theme: {{ explode("|", $data['theme'])[1] }}
                                            <br>
                                            Theme Id: {{ explode("|", $data['theme'])[0] }}
                                            <br>
                                            Assigned Id: {{ $data['assigned_id'] }}
                                        </p>
                                        <p style='margin: 1em 0;'>
                                            {!! $data['description'] !!}
                                        </p>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" valign='top'>
                                        <p style='margin: 1em 0;'>
                                            Thank you, <br>
                                            Rocket Discount.
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
@stop
