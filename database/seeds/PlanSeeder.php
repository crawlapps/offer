<?php

use Illuminate\Database\Seeder;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        \DB::table('plans')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1');

        \DB::table('plans')->insert(
            [
                [
                    'id' => 1,
                    'type' => 1,
                    'name' => "Basic",
                    'price' => 20,
                    'capped_amount' => 0.00,
                    'terms' => "",
                    'trial_days' => 10,
                    'test' => 1,
                    'on_install' => 1
                ],
            ]
        );
    }
}
