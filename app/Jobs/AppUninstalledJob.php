<?php

namespace App\Jobs;

class AppUninstalledJob extends \OhMyBrew\ShopifyApp\Jobs\AppUninstalledJob
{

    public function handle()
    {

        \Log::Info("=============== Shop Uninstalled ==================");
        if (!$this->shop) {
            return false;
        }

        $this->shop->theme_id = null;
        $this->shop->money_format = null;
        $this->shop->currency= null;
        $this->shop->save();

        $this->cancelCharge();
        $this->cleanShop();
        $this->softDeleteShop();

        return true;
    }
}
