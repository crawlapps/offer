<?php
function adminNav(){
    $menus = [];
    $shop = \ShopifyApp::shop();
    $storeFirstSaleDate = \App\Models\Webhook::where('shop_id',$shop->id)->orderBy('id','asc')->first();

    if(!$storeFirstSaleDate) {
        $menus[] = [
            'title'  => 'Instruction',
            'link'   => route('setup.steps'),
            'active' => (Request::is('setup/steps'))?'active':'',
        ];
    }

    $menus_all = [
        /*
        |--------------------------------------------------------------------------
        | Admin Navigation Menu
        |--------------------------------------------------------------------------
        |
        | This array is for Navigation menus of the backend.  Just add/edit or
        | remove the elements from this array which will automatically change the
        | navigation.
        |
        */
        // SIDEBAR LAYOUT - MENU
        [
            'title'  => 'Dashboard',
            'link'   => route('home'),
            'active' => (Request::path() == '/')?'active':'',
        ],
        [
            'title'  => 'Ruleset',
            'link'   => route('offers.index'),
            'active' => (Request::is('offers*'))?'active':'',
        ],
        [
            'title'  => 'Display Settings',
            'link'   => route('settings.index'),
            'active' => (Request::is('settings*'))?'active':'',
        ],
        [
            'title'  => 'Help',
            'link'   => '#',
            'active' => (Request::is('setup/uninstall/view*') || Request::is('faq') )?'active':'',
            'child' => [
                [
                    'title'  => 'Plan & Pricing',
                    'link'   => route('plan_pricing'),
                    'active' => (Request::is('plan-pricing'))?'active':'',
                    'target' => '',
                ],
                [
                    'title'  => 'Install - Unistall',
                    'link'   => route('setup.uninstall.view'),
                    'active' => (Request::is('setup/uninstall/view'))?'active':'',
                    'target' => '',
                ],
                [
                    'title'  => 'FAQ',
                    'link'   => route('faq'),
                    'active' => (Request::is('faq'))?'active':'',
                    'target' => '',
                ]
            ]
        ],
    ];

    $menus_all = array_merge($menus,$menus_all);
    return $menus_all;
}
