<?php

namespace App\Http\Controllers\Setup;

use App\Models\Webhook;
use App\Notifications\ContactAdminNotification;
use App\Notifications\ContactUsNotification;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use OhMyBrew\ShopifyApp\ShopifyApp;

class SetupController extends Controller
{
    public $view = 'pages.setup.';

    public function steps(){

        $shop = \ShopifyApp::shop();

        $is_new_theme = false;
        if($shop->theme_id) {
            $main_theme = $shop->theme_id;
        } else {
            $main_theme = $shop->api()->rest('GET', 'admin/themes.json',['role' => 'main']);
            $main_theme = $main_theme->body->themes[0]->id;
            $is_new_theme = true;
        }

        $data = $this->index();
        $orders = Webhook::where('shop_id',$shop->id)->where('topic','orders/create')->count();

        return view($this->view.'steps',compact('data','orders','main_theme', 'is_new_theme'));
    }

    public function index() {
        $shop = \ShopifyApp::shop();
        $themes = $shop->api()->rest('GET', 'admin/themes.json', ['fields' => 'id,name']);
        $theme = $data = [];
        foreach($themes->body->themes as $val){
            $theme['label'] = $val->name;
            $theme['value'] = (string)$val->id;
            $data[] = $theme;
        }
        return $data;
    }

    public function store(Request $request) {

        if($request->filled('call_type')) {
            $shop = \ShopifyApp::shop();
            $user = new User();

            $user->email = $shop->email_id;
            $request->merge(['assigned_id' => time()]);
            \Notification::send($user,new ContactUsNotification($request->all()));
            $user->email = config('mail.from.address');
            \Notification::send($user,new ContactAdminNotification($request->all()));

            return \Response::json([
                'data' => 'Mail Sucessfully Send',
            ], 200);
        }


        $template = "sections/cart-template.liquid";
        $shop = \ShopifyApp::shop();
        $asset = $shop->api()->rest('GET', 'admin/themes/'.$request->theme.'/assets.json',["asset[key]" => $template]);

        if($asset->errors) {
            $template = "snippets/ajax-cart-template.liquid";
            $asset = $shop->api()->rest('GET', 'admin/themes/'.$request->theme.'/assets.json',["asset[key]" => $template]);
        }

        if($asset->errors) {
            $template = "templates/cart.liquid";
            $asset = $shop->api()->rest('GET', 'admin/themes/'.$request->theme.'/assets.json',["asset[key]" => $template]);
        }

        if(@$asset->body->asset){
            $asset = $asset->body->asset->value;

            if(strpos($asset ,"uninstall-crawlappscartitemprice")){
                $asset = str_replace('uninstall-crawlappscartitemprice',"crawlapps-cart-item-price ",$asset);
            }
            elseif(!strpos($asset ,"<span class='crawlapps-cart-item-price'")){
                $asset = str_replace('{{ item.price | money }}',"<span class='crawlapps-cart-item-price ' data-key='{{item.key}}'>{{ item.price | money }}</span>",$asset);

                $asset = str_replace('{{ item.original_price | money }}',"<span class='crawlapps-cart-item-price ' data-key='{{item.key}}'>{{ item.original_price | money }}</span>",$asset);
            }

            if(strpos($asset ,"uninstall-crawlappscartitemlineprice")){
                $asset = str_replace('uninstall-crawlappscartitemlineprice',"crawlapps-cart-item-line-price ",$asset);
            }
            elseif(!strpos($asset ,"<span class='crawlapps-cart-item-line-price'")){
                $asset = str_replace('{{ item.line_price | money }}',"<span class='crawlapps-cart-item-line-price' data-key='{{item.key}}'>{{ item.line_price | money }}</span>",$asset);
                $asset= str_replace('{{ item.original_line_price | money }}',"<span class='crawlapps-cart-item-line-price' data-key='{{item.key}}'>{{ item.original_line_price | money }}</span>",$asset);
            }

            if(!strpos($asset ,"value=\"{{ item.quantity }}\" data-key='{{item.key}}'")){
                $asset = str_replace("value=\"{{ item.quantity }}\"","value=\"{{ item.quantity }}\" data-key='{{item.key}}'",$asset);
            }

            if(strpos($asset ,"uninstall-crawlappscartoriginaltotal")){
                $asset = str_replace('uninstall-crawlappscartoriginaltotal',"crawlapps-cart-original-total ",$asset);
            }
            elseif(!strpos($asset ,"<span class='crawlapps-cart-original-total'")){
                $asset = str_replace('{{ cart.total_price | money }}',"<span class='crawlapps-cart-original-total'>{{ cart.total_price | money }}</span>",$asset);
            }

            $parameter['asset']['key'] = $template;
            $parameter['asset']['value'] = $asset;
            $asset = $shop->api()->rest('PUT', 'admin/themes/'.$request->theme.'/assets.json',$parameter);

            $this->snippet('add',$request);
            $this->updateThemeLiquid('add',$request);
            if(@$asset->body->asset) {

                $shop->css_theme = $request->css_theme;
                $shop->theme_id = $request->theme;
                $shop->save();

                return \Response::json([
                    'is_new_theme' => false,
                    'theme_id' => $shop->theme_id,
                    'data' => 'Auto Install Completed',
                ], 200);
            }

        }
    }

    public function updateThemeLiquid($type,$request){
        $shop = \ShopifyApp::shop();
        if($type == 'add'){

            $asset = $shop->api()->rest('GET', 'admin/themes/'.$request->theme.'/assets.json',["asset[key]" => 'layout/theme.liquid']);
            if(@$asset->body->asset) {
                $asset = $asset->body->asset->value;
                if(!strpos($asset ,"{% include 'crawlapps-offers' %}</head>")) {
                    $asset = str_replace('</head>',"{% include 'crawlapps-offers' %}</head> ",$asset);
                }

                $parameter['asset']['key'] = 'layout/theme.liquid';
                $parameter['asset']['value'] = $asset;
                $asset = $shop->api()->rest('PUT', 'admin/themes/'.$request->theme.'/assets.json',$parameter);
            }
        }
    }

    public function snippet($type,$request){

        $shop = \ShopifyApp::shop();
        if($type == 'add') {
            $value = <<<EOF
        <script id="crawlapps_offer_shop_data" type="application/json">
            {
                "shop": {
                    "domain": "{{ shop.domain }}",
                    "permanent_domain": "{{ shop.permanent_domain }}",
                    "url": "{{ shop.url }}",
                    "secure_url": "{{ shop.secure_url }}",
                    "money_format": {{ shop.money_format | json }},
                    "currency": {{ shop.currency | json }}
                },
                "customer": {
                    "id": {{ customer.id | json }},
                    "tags": {{ customer.tags | json }}
                },
                "cart": {{ cart | json }},
                "template": "{{ template | split: "." | first }}",
                "product": {{ product | json }},
                "collection": {{ collection.products | json }}
            }
        </script>
EOF;
        }
        $parameter['asset']['key'] = 'snippets/crawlapps-offers.liquid';
        $parameter['asset']['value'] = $value;
        $asset = $shop->api()->rest('PUT', 'admin/themes/'.$request->theme.'/assets.json',$parameter);
    }

    public function unInstallView(Request $request){
        $shop = \ShopifyApp::shop();

        $is_new_theme = true;
        $main_theme = null;
        if ($shop->theme_id) {
            $main_theme = $shop->theme_id;
            $is_new_theme = false;
        }else{
            $main_theme = $shop->api()->rest('GET', 'admin/themes.json',['role' => 'main']);
            $main_theme = $main_theme->body->themes[0]->id;
        }

        $data = $this->index();
        return view($this->view.'uninstall',compact('data','main_theme', 'is_new_theme'));
    }

    public function unInstall(Request $request){
        $shop = \ShopifyApp::shop();

        $template = "sections/cart-template.liquid";
        $asset = $shop->api()->rest('GET', 'admin/themes/'.$request->theme.'/assets.json',["asset[key]" => $template]);

        if($asset->errors) {
            $template = "snippets/ajax-cart-template.liquid";
            $asset = $shop->api()->rest('GET', 'admin/themes/'.$request->theme.'/assets.json',["asset[key]" => $template]);
        }

        if($asset->errors) {
            $template = "templates/cart.liquid";
            $asset = $shop->api()->rest('GET', 'admin/themes/'.$request->theme.'/assets.json',["asset[key]" => $template]);
        }

        if(@$asset->body->asset) {

            $asset = $asset->body->asset->value;
            if(strpos($asset ,"crawlapps-cart-item-price")) {
                $asset = str_replace('crawlapps-cart-item-price',"uninstall-crawlappscartitemprice ",$asset);
            }

            if(strpos($asset ,"crawlapps-cart-item-line-price")) {
                $asset = str_replace('crawlapps-cart-item-line-price',"uninstall-crawlappscartitemlineprice ",$asset);
            }

            if(strpos($asset ,"crawlapps-cart-original-total")) {
                $asset = str_replace('crawlapps-cart-original-total',"uninstall-crawlappscartoriginaltotal ",$asset);
            }

            $parameter['asset']['key'] = $template;
            $parameter['asset']['value'] = $asset;

            $asset = $shop->api()->rest('PUT', 'admin/themes/'.$request->theme.'/assets.json',$parameter);
            if(@$asset->body->asset){
                $shop->theme_id = null;
                $shop->save();

                $main_theme = $shop->api()->rest('GET', 'admin/themes.json',['role' => 'main']);
                $main_theme = $main_theme->body->themes[0]->id;

                $parameter = [];
                $parameter['asset']['key'] = 'snippets/crawlapps-offers.liquid';
                $asset = $shop->api()->rest('DELETE', 'admin/themes/'.$request->theme.'/assets.json',$parameter);

                return \Response::json([
                    'theme_id' => $main_theme,
                    'data' => 'Successfully Uninstalled',
                ], 200);
            }

        }

    }
}
