<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommonController extends Controller
{
    public function faq(){
        return view('frontend.faq');
    }

    public function instruction(){
        return view('frontend.instruction');
    }

    public function manualInstall() {
        return view('pages.manual-installation-guide.index');
    }

    public function termsandcondition() {
        return view('frontend.terms-of-use');
    }

    public function privacyPolicy() {
        return view('frontend.privacy-policy');
    }

    public function planPricing() {
        return view('frontend.plan_pricing');
    }
}
