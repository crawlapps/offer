/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/@babel/runtime/regenerator/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/@babel/runtime/regenerator/index.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! regenerator-runtime */ "./node_modules/regenerator-runtime/runtime.js");


/***/ }),

/***/ "./node_modules/regenerator-runtime/runtime.js":
/*!*****************************************************!*\
  !*** ./node_modules/regenerator-runtime/runtime.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var runtime = (function (exports) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  exports.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  IteratorPrototype[iteratorSymbol] = function () {
    return this;
  };

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
  GeneratorFunctionPrototype.constructor = GeneratorFunction;
  GeneratorFunctionPrototype[toStringTagSymbol] =
    GeneratorFunction.displayName = "GeneratorFunction";

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      prototype[method] = function(arg) {
        return this._invoke(method, arg);
      };
    });
  }

  exports.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  exports.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      if (!(toStringTagSymbol in genFun)) {
        genFun[toStringTagSymbol] = "GeneratorFunction";
      }
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  exports.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return Promise.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return Promise.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new Promise(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  AsyncIterator.prototype[asyncIteratorSymbol] = function () {
    return this;
  };
  exports.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  exports.async = function(innerFn, outerFn, self, tryLocsList) {
    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList)
    );

    return exports.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        // Note: ["return"] must be used for ES3 parsing compatibility.
        if (delegate.iterator["return"]) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  Gp[toStringTagSymbol] = "Generator";

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  Gp[iteratorSymbol] = function() {
    return this;
  };

  Gp.toString = function() {
    return "[object Generator]";
  };

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  exports.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  exports.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };

  // Regardless of whether this script is executing as a CommonJS module
  // or not, return the runtime object so that we can declare the variable
  // regeneratorRuntime in the outer scope, which allows this module to be
  // injected easily by `bin/regenerator --include-runtime script.js`.
  return exports;

}(
  // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
   true ? module.exports : undefined
));

try {
  regeneratorRuntime = runtime;
} catch (accidentalStrictMode) {
  // This module should not be running in strict mode, so the above
  // assignment should always work unless something is misconfigured. Just
  // in case runtime.js accidentally runs in strict mode, we can escape
  // strict mode using a global Function call. This could conceivably fail
  // if a Content Security Policy forbids using Function, but in that case
  // the proper solution is to fix the accidental strict mode problem. If
  // you've misconfigured your bundler to force strict mode and applied a
  // CSP to forbid Function, and you're not willing to fix either of those
  // problems, please detail your unique predicament in a GitHub issue.
  Function("r", "regeneratorRuntime = r")(runtime);
}


/***/ }),

/***/ "./resources/js/crawlapps-cli-offer.js":
/*!*********************************************!*\
  !*** ./resources/js/crawlapps-cli-offer.js ***!
  \*********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var host = "https://offer.test";
var apiEndPoint = host + '/api';
var shopifyDomain = Shopify.shop;
var frontendDomain = window.location.origin;
var shopifyTheme = convertToSlug(Shopify.theme.name);
var jqueryLoaded = 0;

if (!window.jQuery) {
  var script = document.createElement('script');
  script.type = "text/javascript";
  script.src = "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js";
  document.getElementsByTagName('head')[0].appendChild(script);
  jqueryLoaded = 1;
}

Window.crawlapps = {
  shop_data: JSON.parse(document.getElementById('crawlapps_offer_shop_data').innerHTML),
  init: function init() {
    this.initScript();
  },
  addShortcode: function addShortcode() {
    if (allThemes()) return true;
    var themes = ['crawlapps_body_narrative', 'crawlapps_body_boundless', 'crawlapps_body_prestige', 'crawlapps_body_ella'];
    var theme2parentlvl = ['crawlapps_body_shoptimized', 'crawlapps_body_masonry', 'crawlapps_body_district'];
    var noAddBtnName = ['crawlapps_body_prestige'];
    var themeMotion = ['crawlapps_body_motion'];
    var gridAtLastAppend = ['crawlapps_body_narrative', 'crawlapps_body_boundless', 'crawlapps_body_prestige', 'crawlapps_body_district'];
    var addToCart = $('[name="add"]:first, #AddToCart:first, .product-form--atc-button:first, .add_to_cart_product_page:first').parent();

    if (theme2parentlvl.indexOf(addBodyClass) >= 0) {
      addToCart = $('[name="add"]:first, #AddToCart:first, .product-form--atc-button:first, .add_to_cart_product_page:first').parent().parent();
    }

    if (themeMotion.indexOf(addBodyClass) >= 0) addToCart = $('[name="add"]:first, #AddToCart:first, .product-form--atc-button:first, .add_to_cart_product_page:first');

    if ($('.crawlapps_offers_dropdown').length <= 0) {
      if (themes.indexOf(noAddBtnName) >= 0) {
        addToCart = $('form[action="/cart/add"]:first .ProductForm__AddToCart:first, #AddToCart:first, .product-form--atc-button:first, .add_to_cart_product_page:first');
      } else if (addBodyClass == "crawlapps_body_minimart") {
        addToCart = $('form[action="/cart/add"]:first #product-variants');
        addToCart.after('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_dropdown"></div>');
      } else if (addBodyClass == "crawlapps_body_hustler") {
        addToCart = $('form[action="/cart/add"]:first .bar_adjustment');
        addToCart.before('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_dropdown"></div>');
      } else if (addBodyClass == "crawlapps_body_debut") {
        if ($('form[action="/cart/add"]:first .single-option-selector').length > 0) {
          addToCart = $('form[action="/cart/add"]:first .selector-wrapper:last');
          addToCart.after('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_dropdown"></div>');
        } else {
          addToCart = $('form[action="/cart/add"]:first .product-form__controls-group--submit:first');
          addToCart.prepend('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_dropdown"></div>');
        }
      } else if (addBodyClass == "crawlapps_body_venture") {
        addToCart = $('form[action="/cart/add"]:first .product-form__item--quantity:first');
        addToCart.before('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_dropdown"></div>');
      } else if (addBodyClass == "crawlapps_body_simple") {
        if ($('form[action="/cart/add"]:first .single-option-selector').length > 1) {
          addToCart.before('<div data-code="crawlapps_offers" class="selector-wrapper crawlapps_offers crawlapps_offers_dropdown"></div>');
        } else {
          addToCart.before('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_dropdown"></div>');
        }
      } else if (addBodyClass == "crawlapps_body_ella") {
        addToCart = $('[name="add"]:first');
        addToCart.before('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_dropdown"></div>');
      } else if (addBodyClass == "crawlapps_body_narrative") {
        addToCart = $('[name="add"]:first');
        addToCart.before('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_dropdown"></div>');
      } else if (addBodyClass == "crawlapps_body_boundless") {
        addToCart = $('[name="add"]:first');
        addToCart.before('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_dropdown"></div>');
      } else {
        addToCart.before('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_dropdown"></div>');
      }
    }

    if ($('.crawlapps_offers_swatch').length <= 0) {
      if (themes.indexOf(addBodyClass) >= 0) {
        if (themes.indexOf(noAddBtnName) >= 0) {
          addToCart = $('form[action="/cart/add"]:first .ProductForm__AddToCart:first, #AddToCart:first, .product-form--atc-button:first, .add_to_cart_product_page:first');
        } else {
          addToCart = $('[name="add"]:first, #AddToCart:first, .product-form--atc-button:first, .add_to_cart_product_page:first');
        }

        addToCart.before('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_swatch"></div>');
      } else if (addBodyClass == "crawlapps_body_minimart") {
        addToCart = $('form[action="/cart/add"]:first #product-variants');
        addToCart.after('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_swatch"></div>');
      } else if (addBodyClass == "crawlapps_body_hustler") {
        addToCart = $('form[action="/cart/add"]:first .bar_adjustment');
        addToCart.before('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_swatch"></div>');
      } else if (addBodyClass == "crawlapps_body_debut") {
        addToCart = $('form[action="/cart/add"]:first .product-form__controls-group--submit');
        addToCart.prepend('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_swatch"></div>');
      } else {
        addToCart.before('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_swatch"></div>');
      }
    }

    addToCart = $('[name="add"]:first, #AddToCart:first, .product-form--atc-button:first, .add_to_cart_product_page:first').parent();

    if ($('.crawlapps_offers_gridview').length <= 0) {
      if (gridAtLastAppend.indexOf(addBodyClass) >= 0) {
        $('form[action="/cart/add"]:first, #AddToCart:first, .product-form--atc-button:first, .add_to_cart_product_page:first').append('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_gridview" ></div>');
      } else {
        addToCart.after('<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_gridview" ></div>');
      }
    }
  },
  getOffers: function getOffers() {
    var self = Window.crawlapps;
    var aPIEndPoint = "".concat(apiEndPoint, "/").concat(shopifyDomain, "/offers?body_class=").concat(addBodyClass, "&product_id=") + Window.crawlapps.shop_data.product.id;
    $.ajax({
      method: "GET",
      url: aPIEndPoint,
      contentType: 'application/json;',
      success: function success(response, _success, header) {
        var type = header.getResponseHeader('x_discount_type');
        var body_class = header.getResponseHeader('body_class');
        console.log(body_class);
        document.body.classList.add(body_class);

        if (typeof type == "string") {
          self.addShortcode();
        }

        if (type == "swatch") {
          $(".crawlapps_offers_swatch").html(response);
        } else if (type == "dropdown") {
          $(".crawlapps_offers_dropdown").html(response);
          var value = $("select.crawlapps_css_dropdown").children("option:selected").val();
          $("#offerChange_dropdown").trigger('change');
        } else {
          $(".crawlapps_offers_gridview").html(response);
        }
      }
    });
  },
  initScript: function initScript() {
    if (Window.crawlapps.shop_data.template == 'product') {
      Window.crawlapps.getOffers();
      Window.crawlapps.addToCartButtonClick();
    }

    if (Window.crawlapps.shop_data.template == 'cart') {
      Window.crawlapps.cartPage();
    } else {
      if (localStorage.getItem("crawlapps_cart_discount") != null) {
        $("input[name='checkout'], input[value='Checkout'], button[name='checkout'], [href$='checkout'], button[value='Checkout'], input[name='goto_pp'], button[name='goto_pp'], input[name='goto_gc'], button[name='goto_gc'], .action button.btn-2").removeAttr('name').attr('name', 'checkout_crawlapps').addClass('checkout_crawlapps');
      }
    }

    setTimeout(function () {
      console.log("initScript");
      Window.crawlapps.checkoutButtonClick();
    }, 3000);
  },
  cartPage: function () {
    var _cartPage = _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      var self, aPIEndPoint;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              $('body').on('click', '.crawlapps_discount_button', function (e) {
                e.preventDefault();
                var code = $('.crawlapps_discount_code').val();

                if (code == '') {
                  $('.crawlapps_discount_code').addClass('discount_error');
                } else {
                  localStorage.setItem('discount_code', code);
                  $('.crawlapps_discount_code').removeClass('discount_error');
                }
              });
              self = this;
              aPIEndPoint = "".concat(frontendDomain, "/cart.json");
              $.ajax({
                method: "GET",
                url: aPIEndPoint,
                contentType: 'application/json;',
                success: function success(response, _success2, header) {
                  console.log(response);
                  var formdata = {
                    discount_code: localStorage.getItem('discount_code'),
                    data: response
                  };
                  var url = "".concat(apiEndPoint, "/").concat(shopifyDomain, "/offers/cart");
                  $.post(url, formdata).done(function (response) {
                    var data = response.data;

                    if (_typeof(data.discounts) == "object" && _typeof(data.discounts.items) == "object") {
                      self.crawlappsShowCartDiscounts(data.discounts);
                    }
                  }).fail(function (xhr, status, error) {
                    console.log(xhr, status, error);
                  });
                },
                error: function error(XMLHttpRequest, textStatus, errorThrown) {
                  console.log(errorThrown);
                }
              });

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    function cartPage() {
      return _cartPage.apply(this, arguments);
    }

    return cartPage;
  }(),
  addToCartButtonClick: function () {
    var _addToCartButtonClick = _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
      var base;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              base = Window.crawlapps;
              $(document).on('click', "input[name='add'], input[value='Add To Cart'], button[name='add'], [href$='add'], button[value='Add To Cart']", function (e) {
                setTimeout(function () {
                  base.cartPage();
                }, 1000);
              });

            case 2:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    function addToCartButtonClick() {
      return _addToCartButtonClick.apply(this, arguments);
    }

    return addToCartButtonClick;
  }(),
  crawlappsShowCartDiscounts: function crawlappsShowCartDiscounts(discounts) {
    var flag = 0;
    $(".crawlapps_discount_hide").show();
    console.log('sssssss_2---in');
    discounts.cart.items.forEach(function (item) {
      if (item.cart_discount_away_msg_html) {
        $(".crawlapps-reminder[data-key='" + item.key + "']").html(item.cart_discount_away_msg_html);
      }

      if (item.discounted_price < item.original_price) {
        $(".cart__qty-input[data-key='" + item.key + "']").val(item.__quantity); //$(".cart__qty-input[data-key='" + item.key + "']").attr( 'readonly', 'readonly' );

        flag = 1;

        if (item.__code == "DISCOUNTSINGLE") {
          $(".crawlapps-cart-item-price[data-key='" + item.key + "']").html("<span class='original_price'>" + item.original_price_format + "</span>");
        } else {
          //console.log('---------------1----------');
          $(".crawlapps-cart-item-price[data-key='" + item.key + "']").html("<span class='original_price' style='text-decoration:line-through;'>" + item.original_price_format + "</span><br/>" + "<span class='discounted_price'>" + item.discounted_price_format + "</span>");
        } //console.log('---------------2----------');


        $(".crawlapps-cart-item-line-price[data-key='" + item.key + "']").html("<span class='original_price' style='text-decoration:line-through;'>" + item.original_line_price_format + "</span><br/>" + "<span class='discounted_price'>" + item.discounted_line_price_format + "</span>");
      } else {
        $(".crawlapps-cart-item-price[data-key='" + item.key + "']").html("<span class='original_price'>" + item.original_price_format + "</span>");
        $(".crawlapps-cart-item-line-price[data-key='" + item.key + "']").html("<span class='original_price'>" + item.original_line_price_format + "</span>");
      }
    });

    if (flag == 1) {
      $(".crawlapps-cart-original-total").html(discounts.original_total_price).css("text-decoration", "line-through");

      if (discounts.final_with_discounted_price == null) {
        $("<span class='crawlapps-cart-total'>" + discounts.discounted_price_total + "</span>").insertAfter('.crawlapps-cart-original-total');
      } else {
        $("<span class='crawlapps-cart-total'>" + discounts.final_with_discounted_price + "</span>").insertAfter('.crawlapps-cart-original-total');
      }

      if ($(".crawlapps-discount-bar").length > 0) {
        $(".crawlapps-discount-bar").html(discounts.cart_discount_msg_html);
      } else {
        $('form[action="/cart"]').prepend(discounts.cart_discount_msg_html);
      }
    }

    if (discounts.discount_code && discounts.discount_error == 1) {
      $(".crawlapps-cart-original-total").html(discounts.original_price_total);
      $(".crawlapps_discount_hide").after("<span class='crawlapps_summary'>Discount code does not match</span>");
      localStorage.removeItem('discount_code');
    } else if (discounts.discount_code && $('.discount_code_box').is(":visible")) {
      $(".crawlapps_discount_hide").after("<span class='crawlapps-summary-line-discount-code'><span class='discount-tag'>" + discounts.discount_code + "<span class='close-tag'></span></span><span class='crawlapps_with_discount'>" + " -" + discounts.with_discount + "</span></span><span class='after_discount_price'><span class='final-total'>Total</span>" + discounts.final_with_discounted_price + "</span>");

      if (flag == 1) {
        $(".crawlapps-cart-original-total").html(discounts.discounted_price_total).css("text-decoration", "line-through");
      } else {
        $(".crawlapps-cart-original-total").html(discounts.original_price_total).css("text-decoration", "line-through");
      }

      $(".crawlapps-cart-total").remove();
    } else {
      $(".crawlapps-cart-original-total").html(discounts.original_price_total);
    }

    console.log("-------end");
    Window.crawlapps.cart = discounts;
    localStorage.removeItem("crawlapps_cart_discount");
    localStorage.setItem('crawlapps_cart_discount', JSON.stringify(discounts));
  },
  checkoutButtonClick: function checkoutButtonClick() {
    console.log("in");
    $('a.checkout_crawlapps, button[name="checkout"], button.add_to_cart').click(function (e) {
      e.preventDefault();
      e.stopPropagation();
      e.stopImmediatePropagation();
      checkoutIN();
    });
    $(document).on('click', ".checkout_crawlapps, input[name='checkout'], [name='checkout_crawlapps'], input[name='checkout_crawlapps'] , input[value='CHECKOUT'], input[value='Checkout'], button[name='checkout'], [href$='checkout'], [href$='/checkout'], [href='/checkout'], [href='checkout'], button[value='Checkout'], input[name='goto_pp'], button[name='goto_pp'], input[name='goto_gc'], button[name='goto_gc'], #dropdown-cart .actions .btn, .checkout-button, .ajax-cart__button.button--add-to-cart", function (e) {
      e.preventDefault();
      e.stopPropagation();
      e.stopImmediatePropagation();
      checkoutIN();
    });

    function checkoutIN() {
      console.log("checkoutButtonClick");
      var formdata = {
        discount_code: localStorage.getItem('discount_code'),
        data: JSON.parse(localStorage.getItem('crawlapps_cart_discount'))
      };
      var url = "".concat(apiEndPoint, "/").concat(shopifyDomain, "/offers/create-draft-order");
      $.post(url, formdata).done(function (response) {
        var data = response.data;

        if (typeof data.url == "string") {
          window.open(data.url, "_self", "", true);
        }

        return false;
      }).fail(function (xhr, status, error) {
        console.log(xhr, status, error);
      });
      return false;
    }

    ;
  },
  reloadCartPage: function reloadCartPage() {
    document.location.reload();
  }
};
var addBodyClass = "";
var body = document.body;
console.log("Class Name: crawlapps_body_" + shopifyTheme);
addBodyClass = "crawlapps_body_" + shopifyTheme;
body.classList.add(addBodyClass);

if (jqueryLoaded) {
  setTimeout(function () {
    $(document).ready(function () {
      Window.crawlapps.init();
    });
  }, 2000);
} else {
  $(document).ready(function () {
    Window.crawlapps.init();
  });
}

function convertToSlug(Text) {
  return Text.toLowerCase().replace(/[^\w ]+/g, '').replace(/ +/g, '-');
}

function allThemes() {
  var addedCode = false;
  var dropdown = '<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_dropdown"></div>';
  var swatch = '<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_swatch"></div>';
  var grid = '<div data-code="crawlapps_offers" class="crawlapps_offers crawlapps_offers_gridview" ></div>'; // Dropdown

  if ($('.crawlapps_offers_dropdown').length <= 0) {
    var atc = "";

    switch (addBodyClass) {
      case 'crawlapps_body_colors':
        atc = $('form[action="/cart/add"]:first > .expanded > #addToCart-product-template').parent();
        atc.before(dropdown);
        addedCode = true;
        break;

      case 'crawlapps_body_editions':
      case 'crawlapps_body_editionsv960':
        atc = $('form[action="/cart/add"]:first > .product-options');
        atc.append(dropdown);
        addedCode = true;
        break;

      case 'crawlapps_body_launch':
      case 'crawlapps_body_launchv630':
        atc = $('form[action="/cart/add"]:first > .add-to-cart');
        atc.before(dropdown);
        addedCode = true;
        break;

      case 'crawlapps_body_startup':
      case 'crawlapps_body_startupv921':
        atc = $('form[action="/cart/add"]:first > .smart-payments');
        atc.before(dropdown);
        addedCode = true;
        break;

      case 'crawlapps_body_reach':
      case 'crawlapps_body_reachv440':
        atc = $('form[action="/cart/add"]:first > .product-form-scrollable .product-form-atc');
        atc.before(dropdown);
        addedCode = true;
        break;

      case 'crawlapps_body_atlantic':
      case 'crawlapps_body_atlanticv1421':
        atc = $('form[action="/cart/add"]:first > .addtocart-button-active');
        atc.before(dropdown);
        addedCode = true;
        break;

      case 'crawlapps_body_loft':
      case 'crawlapps_body_loft-14101':
        atc = $('form[action="/cart/add"]:first > #ProductSection-product-template .product-single .product-mobile .product-details .product-smart-wrapper');
        atc.before(dropdown);
        addedCode = true;
        break;

      case 'crawlapps_body_grid':
      case 'crawlapps_body_gridv460':
        atc = $('form[action="/cart/add"]:first > .product-add-to-cart');
        atc.before(dropdown);
        addedCode = true;
        break;

      case 'crawlapps_body_pacific':
      case 'crawlapps_body_pacificv430':
        atc = $('form[action="/cart/add"]:first > .product-submit');
        atc.before(dropdown);
        addedCode = true;
        break;

      case 'crawlapps_body_vogue':
      case 'crawlapps_body_voguev450':
        atc = $('form[action="/cart/add"]:first > div.product-add');
        atc.before(dropdown);
        addedCode = true;
        break;

      case 'crawlapps_body_kagami':
        atc = $('form[action="/cart/add"]:first > div.product__buy');
        atc.before(dropdown);
        addedCode = true;
        break;

      case 'crawlapps_body_triss-shopify-theme':
        atc = $('form[action="/cart/add"]:first > #AddToCart');
        atc.before(dropdown);
        addedCode = true;
        break;

      case 'crawlapps_body_symetry':
      case 'crawlapps_body_symetry-301':
        atc = $('form[action="/cart/add"]:first > .quantity-submit-row');
        atc.before(dropdown);
        addedCode = true;
        break;

      case 'crawlapps_body_foodly':
      case 'crawlapps_body_foodlythemev18':
        atc = $('form[action="/cart/add"]:first > div .js-add-to-card').parent();
        atc.before(dropdown);
        addedCode = true;
        break;

      case 'crawlapps_body_basel-premium-theme':
        atc = $('form[action="/cart/add"]:first #shopify_quantity');
        atc.before(dropdown);
        addedCode = true;
        break;

      case 'crawlapps_body_testament':
        atc = $('form[action="/cart/add"]:first > .product-add');
        atc.before(dropdown);
        addedCode = true;
        break;

      case 'crawlapps_body_adeline-offical':
        atc = $('form[action="/cart/add"]:first > div #add-to-cart').parent();
        atc.before(dropdown);
        addedCode = true;
        break;

      case 'crawlapps_body_flow':
      case 'crawlapps_body_flow1':
        atc = $('form[action="/cart/add"]:first > #AddToCart');
        atc.before(dropdown);
        addedCode = true;
        break;

      case 'crawlapps_body_minimal':
        atc = $('form[action="/cart/add"]:first > #AddToCart');
        atc.before(dropdown);
        addedCode = true;
        break;

      case 'crawlapps_body_prestige':
        atc = $('form[action="/cart/add"]:first > .ProductForm__AddToCart');
        atc.before(dropdown);
        addedCode = true;
        break;

      case 'crawlapps_body_showtime':
        atc = $('form[action="/cart/add"]:first > .desc_blk > .variations');
        atc.after(dropdown);
        addedCode = true;
        break;
    }
  } // Swatch


  if ($('.crawlapps_offers_swatch').length <= 0) {
    var _atc = "";

    switch (addBodyClass) {
      case 'crawlapps_body_colors':
        _atc = $('form[action="/cart/add"]:first > .expanded > #addToCart-product-template').parent();

        _atc.before(swatch);

        addedCode = true;
        break;

      case 'crawlapps_body_editions':
      case 'crawlapps_body_editionsv960':
        _atc = $('form[action="/cart/add"]:first > .product-options');

        _atc.append(swatch);

        addedCode = true;
        break;

      case 'crawlapps_body_launch':
      case 'crawlapps_body_launchv630':
        _atc = $('form[action="/cart/add"]:first > .add-to-cart');

        _atc.before(swatch);

        addedCode = true;
        break;

      case 'crawlapps_body_startup':
      case 'crawlapps_body_startupv921':
        _atc = $('form[action="/cart/add"]:first > .smart-payments');

        _atc.before(swatch);

        addedCode = true;
        break;

      case 'crawlapps_body_reach':
      case 'crawlapps_body_reachv440':
        _atc = $('form[action="/cart/add"]:first > .product-form-scrollable .product-form-atc');

        _atc.before(swatch);

        addedCode = true;
        break;

      case 'crawlapps_body_atlantic':
      case 'crawlapps_body_atlanticv1421':
        _atc = $('form[action="/cart/add"]:first > .addtocart-button-active');

        _atc.before(swatch);

        addedCode = true;
        break;

      case 'crawlapps_body_loft':
      case 'crawlapps_body_loft-14101':
        _atc = $('form[action="/cart/add"]:first > #ProductSection-product-template .product-single .product-mobile .product-details .product-smart-wrapper');

        _atc.before(swatch);

        addedCode = true;
        break;

      case 'crawlapps_body_grid':
      case 'crawlapps_body_gridv460':
        _atc = $('form[action="/cart/add"]:first > .product-add-to-cart');

        _atc.before(swatch);

        addedCode = true;
        break;

      case 'crawlapps_body_pacific':
      case 'crawlapps_body_pacificv430':
        _atc = $('form[action="/cart/add"]:first > .product-submit');

        _atc.before(swatch);

        addedCode = true;
        break;

      case 'crawlapps_body_vogue':
      case 'crawlapps_body_voguev450':
        _atc = $('form[action="/cart/add"]:first > div.product-add');

        _atc.before(swatch);

        addedCode = true;
        break;

      case 'crawlapps_body_kagami':
        _atc = $('form[action="/cart/add"]:first > div.product__buy');

        _atc.before(swatch);

        addedCode = true;
        break;

      case 'crawlapps_body_triss-shopify-theme':
        _atc = $('form[action="/cart/add"]:first > #AddToCart');

        _atc.before(swatch);

        addedCode = true;
        break;

      case 'crawlapps_body_symetry':
      case 'crawlapps_body_symetry-301':
        _atc = $('form[action="/cart/add"]:first > .quantity-submit-row');

        _atc.before(swatch);

        addedCode = true;
        break;

      case 'crawlapps_body_foodly':
      case 'crawlapps_body_foodlythemev18':
        _atc = $('form[action="/cart/add"]:first > div .js-add-to-card').parent();

        _atc.before(swatch);

        addedCode = true;
        break;

      case 'crawlapps_body_basel-premium-theme':
        _atc = $('form[action="/cart/add"]:first #shopify_quantity');

        _atc.before(swatch);

        addedCode = true;
        break;

      case 'crawlapps_body_testament':
        _atc = $('form[action="/cart/add"]:first > .product-add');

        _atc.before(swatch);

        addedCode = true;
        break;

      case 'crawlapps_body_adeline-offical':
        _atc = $('form[action="/cart/add"]:first > div #add-to-cart').parent();

        _atc.before(swatch);

        addedCode = true;
        break;

      case 'crawlapps_body_flow':
      case 'crawlapps_body_flow1':
        _atc = $('form[action="/cart/add"]:first > #AddToCart');

        _atc.before(swatch);

        addedCode = true;
        break;

      case 'crawlapps_body_minimal':
        _atc = $('form[action="/cart/add"]:first > #AddToCart');

        _atc.before(swatch);

        addedCode = true;
        break;

      case 'crawlapps_body_prestige':
        _atc = $('form[action="/cart/add"]:first > .ProductForm__AddToCart');

        _atc.before(swatch);

        addedCode = true;
        break;

      case 'crawlapps_body_showtime':
        _atc = $('form[action="/cart/add"]:first > .desc_blk > .variations');

        _atc.after(swatch);

        addedCode = true;
        break;
    }
  } // Grid


  if ($('.crawlapps_offers_gridview').length <= 0) {
    var _atc2 = "";

    switch (addBodyClass) {
      case 'crawlapps_body_colors':
        _atc2 = $('form[action="/cart/add"]:first > .expanded > #addToCart-product-template').parent();

        _atc2.after(grid);

        addedCode = true;
        break;

      case 'crawlapps_body_editions':
      case 'crawlapps_body_editionsv960':
        _atc2 = $('form[action="/cart/add"]:first > .product-add-to-cart');

        _atc2.after(grid);

        addedCode = true;
        break;

      case 'crawlapps_body_launch':
      case 'crawlapps_body_launchv630':
        _atc2 = $('form[action="/cart/add"]:first > .add-to-cart');

        _atc2.after(grid);

        addedCode = true;
        break;

      case 'crawlapps_body_startup':
      case 'crawlapps_body_startupv921':
        _atc2 = $('form[action="/cart/add"]:first > .smart-payments');

        _atc2.after(grid);

        addedCode = true;
        break;

      case 'crawlapps_body_reach':
      case 'crawlapps_body_reachv440':
        _atc2 = $('form[action="/cart/add"]:first > .product-form-scrollable .product-form-atc');

        _atc2.after(grid);

        addedCode = true;
        break;

      case 'crawlapps_body_atlantic':
      case 'crawlapps_body_atlanticv1421':
        _atc2 = $('form[action="/cart/add"]:first');

        _atc2.append(grid);

        addedCode = true;
        break;

      case 'crawlapps_body_loft':
      case 'crawlapps_body_loft-14101':
        _atc2 = $('form[action="/cart/add"]:first > #ProductSection-product-template .product-single .product-mobile .product-details .product-smart-wrapper');

        _atc2.after(grid);

        addedCode = true;
        break;

      case 'crawlapps_body_grid':
      case 'crawlapps_body_gridv460':
        _atc2 = $('form[action="/cart/add"]:first > .product-add-to-cart');

        _atc2.after(grid);

        addedCode = true;
        break;

      case 'crawlapps_body_pacific':
      case 'crawlapps_body_pacificv430':
        _atc2 = $('form[action="/cart/add"]:first > .product-submit');

        _atc2.after(grid);

        addedCode = true;
        break;

      case 'crawlapps_body_vogue':
      case 'crawlapps_body_voguev450':
        _atc2 = $('form[action="/cart/add"]:first > div.product-add');

        _atc2.after(grid);

        addedCode = true;
        break;

      case 'crawlapps_body_kagami':
        _atc2 = $('form[action="/cart/add"]:first > div.product__buy');

        _atc2.after(grid);

        addedCode = true;
        break;

      case 'crawlapps_body_triss-shopify-theme':
        _atc2 = $('form[action="/cart/add"]:first > #AddToCart');

        _atc2.after(grid);

        addedCode = true;
        break;

      case 'crawlapps_body_symetry':
      case 'crawlapps_body_symetry-301':
        _atc2 = $('form[action="/cart/add"]:first > .quantity-submit-row');

        _atc2.after(grid);

        addedCode = true;
        break;

      case 'crawlapps_body_foodly':
      case 'crawlapps_body_foodlythemev18':
        _atc2 = $('form[action="/cart/add"]:first > div .js-add-to-card').parent();

        _atc2.after(grid);

        addedCode = true;
        break;

      case 'crawlapps_body_basel-premium-theme':
        _atc2 = $('form[action="/cart/add"]:first #shopify_add_to_cart');

        _atc2.after(grid);

        addedCode = true;
        break;

      case 'crawlapps_body_testament':
        _atc2 = $('form[action="/cart/add"]:first > .product-add');

        _atc2.after(grid);

        addedCode = true;
        break;

      case 'crawlapps_body_adeline-offical':
        _atc2 = $('form[action="/cart/add"]:first > div #add-to-cart').parent();

        _atc2.after(grid);

        addedCode = true;
        break;

      case 'crawlapps_body_flow':
      case 'crawlapps_body_flow1':
        _atc2 = $('form[action="/cart/add"]:first');

        _atc2.append(grid);

        addedCode = true;
        break;

      case 'crawlapps_body_minimal':
        _atc2 = $('form[action="/cart/add"]:first');

        _atc2.append(grid);

        addedCode = true;
        break;

      case 'crawlapps_body_prestige':
        _atc2 = $('form[action="/cart/add"]:first');

        _atc2.append(grid);

        addedCode = true;
        break;

      case 'crawlapps_body_showtime':
        _atc2 = $('form[action="/cart/add"]:first > .desc_blk > .desc_blk_bot');

        _atc2.after(grid);

        addedCode = true;
        break;
    }
  }

  return addedCode;
}

/***/ }),

/***/ 1:
/*!***************************************************!*\
  !*** multi ./resources/js/crawlapps-cli-offer.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/vishalgohil/projects/ofc/offer/resources/js/crawlapps-cli-offer.js */"./resources/js/crawlapps-cli-offer.js");


/***/ })

/******/ });